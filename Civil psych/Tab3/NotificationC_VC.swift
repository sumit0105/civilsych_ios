//
//  NotificationC_VC.swift
//  Civil psych
//
//  Created by Ankur  on 27/02/21.
//

import UIKit

class NotificationC_VC: UIViewController {
    
    var imgArr = [#imageLiteral(resourceName: "profile-Image"),#imageLiteral(resourceName: "i13"),#imageLiteral(resourceName: "profile-Image"),#imageLiteral(resourceName: "i13"),#imageLiteral(resourceName: "profile-Image"),#imageLiteral(resourceName: "i13"),#imageLiteral(resourceName: "profile-Image")]
    let imgArr1 = [#imageLiteral(resourceName: "profile-Image"),#imageLiteral(resourceName: "i13"),#imageLiteral(resourceName: "profile-Image"),#imageLiteral(resourceName: "i13"),#imageLiteral(resourceName: "profile-Image"),#imageLiteral(resourceName: "i13"),#imageLiteral(resourceName: "profile-Image")]
    
    @IBOutlet weak var table:UITableView!

    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
    }
    
    func setup(){
        self.table.register(UINib(nibName: "NotificationC_Cell", bundle: nil), forCellReuseIdentifier: "NotificationC_Cell")
        table.delegate = self
        table.dataSource = self
    }
}

extension NotificationC_VC :UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return imgArr.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = table.dequeueReusableCell(withIdentifier: "NotificationC_Cell", for: indexPath) as! NotificationC_Cell
        cell.img_user.image = imgArr[indexPath.row]
        cell.btnViewStatus.tag = indexPath.row
        cell.btnViewStatus.addTarget(self, action: #selector(btn_ViewStatus(_:)), for: .touchUpInside)
        cell.btnView.isHidden = true
       
        if indexPath.row % 2 != 0 {
            cell.backgroundColor = #colorLiteral(red: 0.9046487808, green: 0.9457306266, blue: 0.9795823693, alpha: 1)
            let txt1 = customizeFont(string: "Richard Albert ", font: UIFont(name: AppFonts.Poppins_SemiBold.rawValue, size: 13)!, color: .black)
            
            let txt2 = customizeFont(string: "Completed his PAQ.", font: UIFont(name: AppFonts.Poppins_Regular.rawValue, size: 13)!, color: .lightGray)
        
            txt1.append(txt2)
            cell.lblComment.attributedText = txt1
            cell.btnView.isHidden = true
        }else{
            cell.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            let txt1 = customizeFont(string: "You have a 1-1 call with ", font: UIFont(name: AppFonts.Poppins_Regular.rawValue, size: 13)!, color: .lightGray)
            
            let txt2 = customizeFont(string: "Mike Doe ", font: UIFont(name: AppFonts.Poppins_SemiBold.rawValue, size: 13)!, color: .black)
            
            let txt3 = customizeFont(string: "Today at 9:30PM.", font: UIFont(name: AppFonts.Poppins_Regular.rawValue, size: 12)!, color: .lightGray)
        
            txt1.append(txt2)
            txt2.append(txt3)
            cell.lblComment.attributedText = txt1
            cell.btnView.isHidden = false
        }
            
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
    @IBAction func btn_ViewStatus(_ sender: UIButton){
        let vc = ViewProgressC_VC.instantiate(fromAppStoryboard: .HomeC)
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
}

