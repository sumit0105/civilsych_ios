//
//  InitialAssesmentC_VC.swift
//  Civil psych
//
//  Created by Ankur  on 26/02/21.
//

import UIKit

class InitialAssesmentC_VC: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    

    @IBAction func btn_Back(_ sender:UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btn_ChatNow(_ sender:UIButton) {
        let vc = ChatC_VC.instantiate(fromAppStoryboard: .HomeC)
        vc.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(vc, animated: true)
    }
}
