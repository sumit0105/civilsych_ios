//
//  SearchC_VC.swift
//  Civil psych
//
//  Created by Ankur  on 08/03/21.
//

import UIKit

class SearchC_VC:UIViewController {
    
    @IBOutlet weak var table: UITableView!
    @IBOutlet weak var searchTxt: UITextField!
    var List : [HomeListC_Modal] = []
    var filteredData: [HomeListC_Modal] = []
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setUp()
        
    }
    
    func setUp()  {
        self.table.register(UINib(nibName: "HomeC_Cell", bundle: nil), forCellReuseIdentifier: "HomeC_Cell")
        table.delegate = self
        table.dataSource = self
        List.append(HomeListC_Modal(employee_Name: "Nick Andorson", ssn_Number: "SSN No. #12355432", imgUser: #imageLiteral(resourceName: "profile-Image")))
        List.append(HomeListC_Modal(employee_Name: "Mark", ssn_Number: "SSN No. #12355432", imgUser: #imageLiteral(resourceName: "i13")))
        List.append(HomeListC_Modal(employee_Name: "Nick Andorson", ssn_Number: "SSN No. #12355432", imgUser: #imageLiteral(resourceName: "profile-Image")))
        List.append(HomeListC_Modal(employee_Name: "Mark", ssn_Number: "SSN No. #12355432", imgUser: #imageLiteral(resourceName: "i13")))
        List.append(HomeListC_Modal(employee_Name: "Nick Andorson", ssn_Number: "SSN No. #12355432", imgUser: #imageLiteral(resourceName: "profile-Image")))
        List.append(HomeListC_Modal(employee_Name: "Mark", ssn_Number: "SSN No. #12355432", imgUser: #imageLiteral(resourceName: "i13")))
        List.append(HomeListC_Modal(employee_Name: "Nick Andorson", ssn_Number: "SSN No. #12355432", imgUser: #imageLiteral(resourceName: "profile-Image")))
        List.append(HomeListC_Modal(employee_Name: "Mark", ssn_Number: "SSN No. #12355432", imgUser: #imageLiteral(resourceName: "i13")))
        
        self.filteredData = self.List
    }
    
    @IBAction func btn_searchAction(_ sender:UIButton) {
        self.navigationController?.popViewController(animated: false)
    }

    
}

extension SearchC_VC: UITableViewDelegate,UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return filteredData.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = table.dequeueReusableCell(withIdentifier: "HomeC_Cell") as! HomeC_Cell
        //            let dict = List[indexPath.row]
        let dict = filteredData[indexPath.row]
        cell.lbl_employeeName.text = dict.employee_Name
        cell.lbl_SSN.text = dict.ssn_Number
        cell.imgEmployee.image = dict.imgUser
        
        cell.btn_ViewProfile.tag = indexPath.row
        cell.btn_ViewProgress.tag = indexPath.row
        cell.btn_ViewProfile.addTarget(self, action: #selector(btn_ViewProfileAction(_:)), for: .touchUpInside)
        cell.btn_ViewProgress.addTarget(self, action: #selector(btn_ViewProgressAction(_:)), for: .touchUpInside)
        
        if indexPath.row % 2 == 0 {
            cell.backgroundColor = .white
        }else{
            cell.backgroundColor = #colorLiteral(red: 0.9084902406, green: 0.9498965144, blue: 0.9871194959, alpha: 1)
        }
        
        return cell
        
    }
    @IBAction func btn_ViewProfileAction(_ sender:UIButton) {
        let vc = ViewProfileC_VC.instantiate(fromAppStoryboard: .HomeC)
        vc.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func btn_ViewProgressAction(_ sender:UIButton) {
        let vc = ViewProgressC_VC.instantiate(fromAppStoryboard: .HomeC)
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
}
extension SearchC_VC: UITextFieldDelegate {
    @IBAction func changeText(_ sender:UITextField) {
        if sender.text?.count == 0 {
            self.filteredData = self.List
        }else{
            filteredData = List.filter({ (text) -> Bool in
                let tmp:NSString = text.employee_Name! as NSString
                let range = tmp.range(of: sender.text!, options: NSString.CompareOptions.caseInsensitive)
                return range.location != NSNotFound
            })
        }
        table.reloadData()
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        print("textFieldDidBeginEditing")
    }
    
}
