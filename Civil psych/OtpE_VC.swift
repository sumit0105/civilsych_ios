//
//  OtpE_VC.swift
//  Civil psych
//
//  Created by Ankur  on 20/02/21.
//

import UIKit

class OtpE_VC: UIViewController {
    
    // MARK:- IBOutlets
    //===================
    
    @IBOutlet weak var otpView: VPMOTPView!

    
    // MARK:- Variables
    //===================
    typealias selectBtn = (String,Int) -> Void
    var select : selectBtn? = nil
    
    var enteredOtp: String = ""
    var otp: String = ""

    override func viewDidLoad() {
        super.viewDidLoad()

        self.OtpViewLoad()
    }
    
    @IBAction func backBtnAction(_ sender: UIButton) {
        self.dismiss(animated: false, completion: {
            self.select!("s",0)
        })
        
    }
    @IBAction func btn_Next(_ sender: UIButton) {
        self.dismiss(animated: false, completion: {
            self.select!("s",1)
        })
    }
    
    @IBAction func backResendAction(_ sender: UIButton) {
        AlertControllerOnr(title: alertTitle.alert_alert, message: messageString.mResendOtp)
    }
}

// MARK:- Otp Methods
//===================
extension OtpE_VC: VPMOTPViewDelegate {
    
    func shouldBecomeFirstResponderForOTP(otpFieldIndex index: Int) -> Bool {
        print(index)
        return true
    }
    
    func hasEnteredAllOTP(hasEntered: Bool) -> Bool {
        print("Has entered all OTP? \(hasEntered)")
        
        return enteredOtp == "\(otp)"
    }
    
    
    
    func enteredOTP(otpString: String) {
        enteredOtp = otpString
        print("OTPString: \(otpString)")
    }
    
    func OtpViewLoad(){
        otpView.otpFieldsCount = 4
        //        otpView.otpFieldDefaultBorderColor = #colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1)
        //        otpView.backgroundColor = #colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1)
        
        otpView.otpFieldEnteredBorderColor = UIColor.green
        otpView.otpFieldErrorBorderColor = UIColor.red
        otpView.otpFieldBorderWidth = 0.5
        otpView.otpFieldDefaultBorderColor = #colorLiteral(red: 0.862745098, green: 0.862745098, blue: 0.862745098, alpha: 1)
        otpView.delegate = self
        otpView.shouldAllowIntermediateEditing = false
        otpView.otpFieldSize = 40
        otpView.otpFieldInputType = .numeric
        otpView.otpFieldDisplayType = .square
        otpView.otpFieldDefaultBackgroundColor = #colorLiteral(red: 0.862745098, green: 0.862745098, blue: 0.862745098, alpha: 1)
        //      otpView.otpFieldEnteredBorderColor = #colorLiteral(red: 0.862745098, green: 0.862745098, blue: 0.862745098, alpha: 1)
        
        
        
        // Create the UI
        otpView.initializeUI()
    }
}
