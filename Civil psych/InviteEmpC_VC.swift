//
//  InviteEmpC_VC.swift
//  Civil psych
//
//  Created by Ankur  on 10/02/21.
//

import UIKit

class InviteEmpC_VC: UIViewController, UITextFieldDelegate {
    
    @IBOutlet weak var txt_empName: UITextField!
    @IBOutlet weak var txt_phoneNum: UITextField!


    override func viewDidLoad() {
        super.viewDidLoad()

        txt_phoneNum.delegate = self
    }
    
    @IBAction func btn_Invite(_ sender:UIButton) {
        if self.txt_empName.text?.count == 0   {
            AlertControllerOnr(title: alertTitle.alert_alert, message: messageString.C_EmpName)
            return
        }
        
        if self.txt_phoneNum.text?.count == 0   {
            AlertControllerOnr(title: alertTitle.alert_alert, message: messageString.C_EmpPhone)
            return
        }
        
        self.dismiss(animated: true, completion: {
//            self.select!("s",1)
        })
    }
    
    @IBAction func btn_Back(_ sender:UIButton) {
        self.dismiss(animated: true, completion: {
//            self.select!("s",0)
        })
}
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == txt_phoneNum {
            guard let text = textField.text else { return false }
            let newString = (text as NSString).replacingCharacters(in: range, with: string)
//            textField.text = format(with: "(XXX) XXX-XXXX", phone: newString)
            textField.text = format(with: "(+X)XXX-XXXXXXX", phone: newString)
            return false
        }
        return true
    }

}
