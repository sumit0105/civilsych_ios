//
//  VerifyEmailPhoneC_VC.swift
//  Civil psych
//
//  Created by Ankur  on 02/02/21.
//

import UIKit

class VerifyEmailPhoneC_VC: UIViewController {
    
    // MARK:- IBOutlets
    //===================
    
    @IBOutlet weak var otpView: VPMOTPView!
    @IBOutlet weak var otpView1: VPMOTPView!
    
    // MARK:- Variables
    //===================
    
    var enteredOtp: String = ""
    var otp: String = ""
    
    var signupData:SignupC_Modal = SignupC_Modal(fullName: "", email: "", phone: "")
    
    override func viewDidLoad() {
        super.viewDidLoad()
        OtpViewLoad(view: otpView)
        OtpViewLoad(view: otpView1)
    }
    
    @IBAction func btn_Submit(_ sender:UIButton) {
        let vc = CreateProfileC_VC.instantiate(fromAppStoryboard: .LoginClinician)
        vc.signupData = signupData
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func btnResendEmail(_ sender: UIButton) {
        AlertControllerOnr(title: alertTitle.alert_alert, message: messageString.mResendOtpEmail)
    }
    
    @IBAction func btnResendPhone(_ sender: UIButton) {
        AlertControllerOnr(title: alertTitle.alert_alert, message: messageString.mResendOtpPhone)
    }
}

// MARK:- Otp Methods
//===================
extension VerifyEmailPhoneC_VC: VPMOTPViewDelegate {
    
    func shouldBecomeFirstResponderForOTP(otpFieldIndex index: Int) -> Bool {
        print(index)
        return true
    }
    
    func hasEnteredAllOTP(hasEntered: Bool) -> Bool {
        print("Has entered all OTP? \(hasEntered)")
        
        return enteredOtp == "\(otp)"
    }
    
    
    
    func enteredOTP(otpString: String) {
        enteredOtp = otpString
        print("OTPString: \(otpString)")
    }
    
    func OtpViewLoad(view:VPMOTPView){
        otpView.otpFieldsCount = 4
        //        otpView.otpFieldDefaultBorderColor = #colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1)
        //        otpView.backgroundColor = #colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1)
        
        view.otpFieldEnteredBorderColor = UIColor.green
        view.otpFieldErrorBorderColor = UIColor.red
        view.otpFieldBorderWidth = 0.5
        view.otpFieldDefaultBorderColor = #colorLiteral(red: 0.862745098, green: 0.862745098, blue: 0.862745098, alpha: 1)
        view.delegate = self
        view.shouldAllowIntermediateEditing = false
        view.otpFieldSize = 40
        view.otpFieldInputType = .numeric
        view.otpFieldDisplayType = .square
        view.otpFieldDefaultBackgroundColor = #colorLiteral(red: 0.862745098, green: 0.862745098, blue: 0.862745098, alpha: 1)
        //      otpView.otpFieldEnteredBorderColor = #colorLiteral(red: 0.862745098, green: 0.862745098, blue: 0.862745098, alpha: 1)
        
        
        
        // Create the UI
        view.initializeUI()
    }
}
