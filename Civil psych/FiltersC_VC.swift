//
//  FiltersC_VC.swift
//  Civil psych
//
//  Created by Ankur  on 09/02/21.
//

import UIKit

class FiltersC_VC: UIViewController {
    
    @IBOutlet weak var table: UITableView!
    @IBOutlet weak var tableHeight: NSLayoutConstraint!
    
    let arrSortBy = ["Recent Signup","Recent Activity"]
    let arrSections = ["Sort By","Location","D.O.T. Segment","Assessment Stage"]
    let arrDotSegment = ["FAA (Airline)","FRA (Railroad)","FMCSA (Trucking)","RITA/PHMSA (Pipeline)","USCG (Coast Guard"]
//1    let arrAssessmentStage = ["Profile","D.O.T. Clearinghouse Registration","Pre- Assessment questioner (PAQ)","Psychometric Questionnaire (ADE)","Initial Assessment","Return to Duty Requirements","Follow Up Assessment","Post RTD Requirements"]
    
    let arrAssessmentStage = ["Profile","D.O.T. Clearinghouse Registration","Pre- Assessment questioner (PAQ)","Psychometric Questionnaire (ADE)","Initial Assessment","Return to Duty Requirements","Follow Up Assessment","RTD Aftercare Requirements"]
    
    var selectedIndex1 = IndexPath(row: -1, section: 0)
    var selectedIndex2 = IndexPath(row: -1, section: 0)
    var selectedIndex3 = IndexPath(row: -1, section: 0)
    
    var tableViewHeight: CGFloat {
          table.layoutIfNeeded()
          return table.contentSize.height
      }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUp()
        updateHeight(table: table)
    }
    
    func setUp()  {
        self.table.register(UINib(nibName: "FilterC_SortByCell", bundle: nil), forCellReuseIdentifier: "FilterC_SortByCell")
        self.table.register(UINib(nibName: "FilterC_LocationCell", bundle: nil), forCellReuseIdentifier: "FilterC_LocationCell")
        self.table.register(UINib(nibName: "FilterC_SegmentCell", bundle: nil), forCellReuseIdentifier: "FilterC_SegmentCell")
        table.delegate = self
        table.dataSource = self
    }
    
    @IBAction func btn_Back(_ sender:UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func btn_Apply(_ sender:UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func btn_ClearAll(_ sender:UIButton) {
        selectedIndex1 = IndexPath(row: -1, section: 0)
        selectedIndex2 = IndexPath(row: -1, section: 0)
        selectedIndex3 = IndexPath(row: -1, section: 0)
        table.reloadData()
    }


}

extension FiltersC_VC: UITableViewDelegate,UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return arrSections.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return arrSortBy.count
        }else if section == 1 {
            return 1
        }else if section == 2 {
            return arrDotSegment.count
        }else{
            return arrAssessmentStage.count
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let headerView = UIView.init(frame: CGRect.init(x: 0, y: 0, width: tableView.frame.width, height: 30))
        headerView.backgroundColor = #colorLiteral(red: 0.07450980392, green: 0.368627451, blue: 0.7176470588, alpha: 1)
        let label = UILabel()
        label.frame = CGRect.init(x: 20, y: 0, width: headerView.frame.width, height: headerView.frame.height)
        
        label.text = arrSections[section]
        label.textColor = UIColor.white
        label.font = UIFont(name: AppFonts.Poppins_SemiBold.rawValue, size: 15.0)
        
        headerView.addSubview(label)
        
        return headerView
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            let cell = table.dequeueReusableCell(withIdentifier: "FilterC_SortByCell") as! FilterC_SortByCell
            cell.lbl_txt.text = arrSortBy[indexPath.row]
            cell.imgSelect.image = #imageLiteral(resourceName: "btn_RoundBlank")
            if selectedIndex1 == indexPath {
                cell.imgSelect.image = #imageLiteral(resourceName: "btn_RoundFill")
            }
            
            return cell
        }else if indexPath.section == 1{
            let cell = table.dequeueReusableCell(withIdentifier: "FilterC_LocationCell") as! FilterC_LocationCell
            
            return cell
            
        }else if indexPath.section == 2{
            let cell = table.dequeueReusableCell(withIdentifier: "FilterC_SegmentCell") as! FilterC_SegmentCell
            cell.lbl_txt.text = arrDotSegment[indexPath.row]
            cell.imgSelect.image = #imageLiteral(resourceName: "uncheck")
            if selectedIndex2 == indexPath {
                cell.imgSelect.image = #imageLiteral(resourceName: "checked")
            }
          
            return cell
            
        }else{
            let cell = table.dequeueReusableCell(withIdentifier: "FilterC_SegmentCell") as! FilterC_SegmentCell
            cell.lbl_txt.text = arrAssessmentStage[indexPath.row]
            cell.imgSelect.image = #imageLiteral(resourceName: "uncheck")
            if selectedIndex3 == indexPath {
                cell.imgSelect.image = #imageLiteral(resourceName: "checked")
            }
            
            return cell
            
        }
        
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 0{
            selectedIndex1 = indexPath
            table.reloadData()
        }else if indexPath.section == 2 {
            selectedIndex2 = indexPath
            table.reloadData()
        }else if indexPath.section == 3 {
            selectedIndex3 = indexPath
            table.reloadData()
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 30
    }
    //    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    //        if indexPath.section == 0 {
    //            return 35
    //        }else{
    //            return 40
    //        }
    //    }
    
    func updateHeight(table:UITableView)  {
        let f = tableViewHeight
        tableHeight.constant = f
        self.table.isScrollEnabled = false
        DispatchQueue.main.async {
            let ff2 = self.tableViewHeight
            self.tableHeight.constant = ff2
        }
    }
}
