//
//  AssessmentC_Cell.swift
//  Civil psych
//
//  Created by Ankur  on 27/02/21.
//

import UIKit

class AssessmentC_Cell: UITableViewCell {
    
    @IBOutlet weak var btnAccept:UIButton!
    @IBOutlet weak var btnDecline:UIButton!
    @IBOutlet weak var lblComment:UILabel!
    @IBOutlet weak var img_user:UIImageView!
    @IBOutlet weak var lbldateTime:UILabel!
     let identifier = "AssessmentC_Cell"

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
