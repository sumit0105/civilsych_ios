//
//  ChatCell.swift
//  Civil psych
//
//  Created by Ankur  on 28/02/21.
//

import UIKit

class ChatCell: UITableViewCell {
    
    @IBOutlet weak var lblLastMessage:UILabel!
    @IBOutlet weak var img_user:UIImageView!
    @IBOutlet weak var lbldateTime:UILabel!
    @IBOutlet weak var lbluserName:UILabel!
    @IBOutlet weak var lblcountMessage:UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
