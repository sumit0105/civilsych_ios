//
//  DOT_E_Cell.swift
//  Civil psych
//
//  Created by Ankur  on 04/03/21.
//

import UIKit

class DOT_E_Cell: UITableViewCell {
    
    @IBOutlet weak var btn_submit: UIButton!
    @IBOutlet weak var lbl_heading: UILabel!
    @IBOutlet weak var lbl_whatTodo: UILabel!
    @IBOutlet weak var lbl_howItWorks: UILabel!


    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
