//
//  PAQ_E_Cell.swift
//  Civil psych
//
//  Created by Ankur  on 03/03/21.
//

import UIKit
import Foundation

class PAQ_E_Cell: UITableViewCell {
    
    @IBOutlet weak var lbl_quesCount: UILabel!
    @IBOutlet weak var lbl_ques: UILabel!
    @IBOutlet weak var txt: UITextView!
    @IBOutlet weak var viewText: UIView!
    @IBOutlet weak var mainView: UIView!

    override func awakeFromNib() {
        super.awakeFromNib()
        txt.delegate = self
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}

extension PAQ_E_Cell : UITextViewDelegate{
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if txt.text == "Type here.." {
            txt.text = ""
        }
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if text == "\n" {
            txt.resignFirstResponder()
        }
        return true
    }

    func textViewDidEndEditing(_ textView: UITextView) {
        if txt.text == ""{
            txt.text = "Type here.."
        }
    }
}
