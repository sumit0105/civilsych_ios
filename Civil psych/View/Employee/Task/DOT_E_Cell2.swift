//
//  DOT_E_Cell2.swift
//  Civil psych
//
//  Created by Ankur  on 04/03/21.
//

import UIKit

class DOT_E_Cell2: UITableViewCell {
    
    @IBOutlet weak var btn_approval: UIButton!
    @IBOutlet weak var lbl_heading: UILabel!
    @IBOutlet weak var lbl_whatTodo: UILabel!
    @IBOutlet weak var lbl_howItWorks: UILabel!


    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
