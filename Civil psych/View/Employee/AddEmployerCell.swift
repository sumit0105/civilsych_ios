//
//  AddEmployerCell.swift
//  Civil psych
//
//  Created by Ankur  on 22/02/21.
//

import UIKit

protocol AddEmployerDelegate: AnyObject {
    func AddEmployerDelegate(index : IndexPath, passengerModel : AddEmployerModal?)
}
class AddEmployerCell: UITableViewCell {
    
    
    @IBOutlet weak var viewQuesMark:UIView!
    @IBOutlet weak var btn_QuestionMark:UIButton!
    @IBOutlet weak var txt_employerName: UITextField!
    @IBOutlet weak var txt_employerAddress: UITextView!
    @IBOutlet weak var txt_employerHrName: UITextField!
    @IBOutlet weak var txt_employerPhone: UITextField!
    @IBOutlet weak var txt_Address: UITextView!
    @IBOutlet weak var viewPinCode:UIView!
    @IBOutlet weak var btn_infoDer:UIButton!
   
    private var passengerModel : AddEmployerModal?
    var indexPath : IndexPath!
    weak var delegate : AddEmployerDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        txt_Address.delegate = self
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}

extension AddEmployerCell : UITextViewDelegate{
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if txt_Address.text == "Enter Address" {
            txt_Address.text = ""
        }
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if text == "\n" {
            txt_Address.resignFirstResponder()
        }
        return true
    }

    func textViewDidEndEditing(_ textView: UITextView) {
        if txt_Address.text == ""{
            txt_Address.text = "Enter Address"
        }
    }
}
