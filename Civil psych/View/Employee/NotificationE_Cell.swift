//
//  NotificationE_Cell.swift
//  Civil psych
//
//  Created by Ankur  on 01/03/21.
//

import UIKit

class NotificationE_Cell: UITableViewCell {
    
    @IBOutlet weak var btnConfirmPay:UIButton!
    @IBOutlet weak var btnView:UIView!
    @IBOutlet weak var lblComment:UILabel!
    @IBOutlet weak var img_user:UIImageView!
    @IBOutlet weak var lbldateTime:UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
