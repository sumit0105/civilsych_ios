//
//  HomeE_Cell.swift
//  Civil psych
//
//  Created by Ankur  on 03/03/21.
//

import UIKit

class HomeE_Cell: UITableViewCell {
    
    @IBOutlet weak var lbl_ClinicianName: UILabel!
    @IBOutlet weak var lbl_distance: UILabel!
    @IBOutlet weak var lbl_paymentType: UILabel!
    @IBOutlet weak var lbl_price: UILabel!
    @IBOutlet weak var img_user: UIImageView!
    @IBOutlet weak var img_Select: UIImageView!
   
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
