//
//  ReferencesCell.swift
//  Civil psych
//
//  Created by Ankur  on 22/02/21.
//

import UIKit
import DropDown

class ReferencesCell: UITableViewCell {
    
    @IBOutlet weak var txt_Address: UITextView!
    @IBOutlet weak var txtName: UITextField!
    @IBOutlet weak var txt_Phone: UITextField!
    @IBOutlet weak var txt_email: UITextField!
    @IBOutlet weak var txt_relationship: UITextField!
    
    //MARK: - DropDown's
    
    let drop_relationship = DropDown()
        
    lazy var dropDowns: [DropDown] = {
        return [
            self.drop_relationship
        ]
    }()

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        txt_Address.delegate = self
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func btn_relationship(_ sender: UIButton){
        
        self.drop_relationship.anchorView = sender
        self.drop_relationship.bottomOffset = CGPoint(x: 0, y: sender.bounds.height + 0)
        self.drop_relationship.textColor = .black
        self.drop_relationship.separatorColor = .clear
        self.drop_relationship.selectionBackgroundColor = .clear
        self.drop_relationship.backgroundColor = #colorLiteral(red: 0.8541120291, green: 0.9235828519, blue: 0.9914466739, alpha: 1)
        self.drop_relationship.dataSource.removeAll()
        self.drop_relationship.dataSource.append(contentsOf: ["Spouse","Partner","Family Member","Other"])
        self.drop_relationship.selectionAction = { [unowned self] (index, item) in
            self.txt_relationship.text = item
        }
        self.drop_relationship.show()
        
    }
    
}

extension ReferencesCell : UITextViewDelegate{
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if txt_Address.text == "Enter Address" {
            txt_Address.text = ""
        }
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if text == "\n" {
            txt_Address.resignFirstResponder()
        }
        return true
    }

    func textViewDidEndEditing(_ textView: UITextView) {
        if txt_Address.text == ""{
            txt_Address.text = "Enter Address"
        }
    }
}
