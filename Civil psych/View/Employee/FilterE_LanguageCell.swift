//
//  FilterE_LanguageCell.swift
//  Civil psych
//
//  Created by Ankur  on 01/03/21.
//

import UIKit

class FilterE_LanguageCell: UITableViewCell {
    
    @IBOutlet weak var lbl_txt: UILabel!
    @IBOutlet weak var imgSelect: UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
