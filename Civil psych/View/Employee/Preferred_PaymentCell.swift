//
//  Preferred_PaymentCell.swift
//  Civil psych
//
//  Created by Ankur on 19/07/21.
//

import UIKit

class Preferred_PaymentCell: UITableViewCell {
    
    @IBOutlet weak var lbl_date: UILabel!
    @IBOutlet weak var lbl_Installment: UILabel!
    @IBOutlet weak var txt_amount: UITextField!
    


    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
