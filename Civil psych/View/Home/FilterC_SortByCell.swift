//
//  FilterC_SortByCell.swift
//  Civil psych
//
//  Created by Ankur  on 09/02/21.
//

import UIKit

class FilterC_SortByCell: UITableViewCell {

    @IBOutlet weak var lbl_txt: UILabel!
    @IBOutlet weak var imgSelect: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
