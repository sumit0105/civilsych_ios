//
//  HomeC_Cell.swift
//  Civil psych
//
//  Created by Ankur  on 08/02/21.
//

import UIKit

class HomeC_Cell: UITableViewCell {
    
    @IBOutlet weak var viewBack: UIView!
    @IBOutlet weak var lbl_employeeName: UILabel!
    @IBOutlet weak var lbl_SSN: UILabel!
    @IBOutlet weak var imgEmployee: UIImageView!
    @IBOutlet weak var btn_ViewProgress: UIButton!
    @IBOutlet weak var btn_ViewProfile: UIButton!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
