//
//  ViewProgressC_Cell.swift
//  Civil psych
//
//  Created by Ankur  on 11/02/21.
//

import UIKit

class ViewProgressC_Cell: UITableViewCell {
    
    @IBOutlet weak var lbl_options: UILabel!
    @IBOutlet weak var lbl_status: UILabel!
    @IBOutlet weak var lbl_markStatus: UILabel!
    @IBOutlet weak var img_markStatus: UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
