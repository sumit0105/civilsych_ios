//
//  ReturnToDutyC_Cell.swift
//  Civil psych
//
//  Created by Ankur  on 01/03/21.
//

import UIKit

class ReturnToDutyC_Cell: UICollectionViewCell {
    
    @IBOutlet weak var lbl_head1: UILabel!
    @IBOutlet weak var lbl_head2: UILabel!
    @IBOutlet weak var lbl_head3: UILabel!
    @IBOutlet weak var viewSuggestedSource: UIView!
    @IBOutlet weak var viewDriverUpdated: UIView!
    @IBOutlet weak var viewDropDown: UIView!
    @IBOutlet weak var txt: UITextView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        txt.delegate = self
    }

}
extension ReturnToDutyC_Cell : UITextViewDelegate{
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if txt.text == "Type here.." {
            txt.text = ""
        }
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if text == "\n" {
            txt.resignFirstResponder()
        }
        return true
    }

    func textViewDidEndEditing(_ textView: UITextView) {
        if txt.text == ""{
            txt.text = "Type here.."
        }
    }
}
