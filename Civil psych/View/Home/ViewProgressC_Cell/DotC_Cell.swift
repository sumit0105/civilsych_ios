//
//  DotC_Cell.swift
//  Civil psych
//
//  Created by Ankur  on 12/02/21.
//

import UIKit

class DotC_Cell: UITableViewCell {
    
    @IBOutlet weak var lbl_options: UILabel!
    @IBOutlet weak var lbl_status: UILabel!
    @IBOutlet weak var img_markStatus: UIImageView!
    @IBOutlet weak var btn_drop: UIButton!
    @IBOutlet weak var view1: UIView!
    @IBOutlet weak var view1Height: NSLayoutConstraint!
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    
}
