//
//  PAQ_C_Cell.swift
//  Civil psych
//
//  Created by Ankur  on 26/02/21.
//

import UIKit

class PAQ_C_Cell: UITableViewCell {
    
    @IBOutlet weak var lbl_quesCount: UILabel!
    @IBOutlet weak var lbl_ques: UILabel!
    @IBOutlet weak var lbl_ans: UILabel!
    @IBOutlet weak var btn_addComment: UIButton!
    
    @IBOutlet weak var txt_Comment: UITextView!
    @IBOutlet weak var commentViewHeight: NSLayoutConstraint!
    @IBOutlet weak var ViewHeight: NSLayoutConstraint!
    @IBOutlet weak var viewComment: UIView!
    @IBOutlet weak var containerView: UIView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        txt_Comment.isScrollEnabled = false
        txt_Comment.delegate = self
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}

extension PAQ_C_Cell : UITextViewDelegate{
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if txt_Comment.text == "Type here.." {
            txt_Comment.text = ""
        }
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if text == "\n" {
            txt_Comment.resignFirstResponder()
        }
        return true
    }

    func textViewDidEndEditing(_ textView: UITextView) {
        if txt_Comment.text == ""{
            txt_Comment.text = "Type here.."
        }
    }
}
