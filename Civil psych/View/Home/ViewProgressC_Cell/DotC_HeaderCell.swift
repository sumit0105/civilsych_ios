//
//  DotC_HeaderCell.swift
//  Civil psych
//
//  Created by Ankur  on 12/02/21.
//

import UIKit
import LUExpandableTableView

final class DotC_HeaderCell: LUExpandableTableViewSectionHeader {
    
    @IBOutlet weak var lbl_options: UILabel!
    @IBOutlet weak var lbl_status: UILabel!
    @IBOutlet weak var imgSelect: UIImageView!
    @IBOutlet weak var btn_drop: UIButton!
    
    override var isExpanded: Bool {
        didSet {
            if lbl_options.text == "Profile" {
            }
            
        }
    }

    // MARK: - Base Class Overrides
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
    // MARK: - IBActions
    
    @IBAction func btn_drop(_ sender: UIButton) {
        // Send the message to his delegate that shold expand or collapse
        delegate?.expandableSectionHeader(self, shouldExpandOrCollapseAtSection: section)
        delegate?.expandableSectionHeader(self, wasSelectedAtSection: section)
    }
    
}
