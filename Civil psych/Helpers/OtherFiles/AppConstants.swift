//
//  AppConstants.swift
//  LUV U
//
//  Created by Ankur  on 14/01/21.
//  Copyright © 2021 Ankur. All rights reserved.
//

import Foundation

class AppConstants {
    
    
}

class alertTitle {
    static let alert_success = "Success"
    static let alert_error = "Error"
    static let alert_warning = "Warning"
    static let alert_alert = "Alert"
    static let alert_message = "Message"
}
class btnTitle {
    static let btnOk = "OK"
    static let btnCancel = "Cancel"
    static let btnFollow = "FOLLOW"
    static let btnUnfollow = "UNFOLLOW"
}

class messageString {
    // Employer
    static let E_empEmailPhone = "Please enter email/phone"
    static let E_empPass = "Please enter password"
    static let E_empConfirmPass = "Please enter confirm password"
    static let E_empFullName = "Please enter full name"
    static let E_empEmail = "Please enter email"
    static let E_empPhone = "Please enter phone"
    static let E_empAccept = "Please accept terms and conditions"
    static let E_empClinicName = "Please enter clinic name"
    
    // Clinician
    
    static let C_reqMsg = "Please enter request message"
    static let C_comments = "Please enter your comments"
    
    static let mNameValidation = "Please enter name."
    static let mEmailValidation = "Please enter email address."
    static let mEmailRegistered = "Please enter registered email address."
    static let mValidEmail = "Please enter valid email."
    static let mInvalidEmail = "Invalid email address"
    static let mGenderValidation = "Please select gender."
    static let mPasswordValidation = "Please enter password."
    static let mCurrentPassword = "Please enter current password."
    static let mNewPassword = "Please enter new password."
    static let mConfirmPasswordValidation = "Please enter confirm password."
    static let mPaaswordmatch = "Password does not match with the confirm password."
    static let mPasPasswordswordMessage = "Password should be a combination of one upper case character, one lowercase character, one special character, one number, and should be at least 8 characters long"
    static let mAddProfilePhoto = "Please add profile photo."
    static let mResendOtp = "Your verification code sent successfully on registered email/phone."
    static let mResendOtpEmail = "Your verification code sent successfully on registered email."
    static let mResendOtpPhone = "Your verification code sent successfully on registered phone."
    static let mOtp = "Please enter otp"
    static let mInitial = "Enter your initials"
    static let mName = "Enter your name"
    
    // Clinician
    
    static let C_EmpName = "Please Enter employee name"
    static let C_EmpPhone = "Please Enter phone number"
}


