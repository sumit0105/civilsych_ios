//
//  ApiKeys.swift
//  Caviar
//
//  Created by YATIN  KALRA on 20/08/20.
//  Copyright © 2020 Ankur. All rights reserved.
//

import Foundation
import UIKit

enum ApiKey {
    static var code: String { return "CODE" }
    
    
    static var mobile : String { return "mobile" }
    static var email : String { return "email" }
    static var user_id : String { return "user_id" }
}
//MARK:- Api Code
//=======================
enum ApiCode {
    
    static var success: Int { return 200 } // Success
    static var unauthorizedRequest: Int { return 206 } // Unauthorized request
    static var headerMissing: Int { return 207 } // Header is missing
    static var phoneNumberAlreadyExist: Int { return 208 } // Phone number alredy exists
    static var requiredParametersMissing: Int { return 418 } // Required Parameter Missing or Invalid
    static var fileUploadFailed: Int { return 421 } // File Upload Failed
    static var pleaseTryAgain: Int { return 500 } // Please try again
    static var tokenExpired: Int { return 401 } // Token expired refresh token needed to be generated
    
}

//MARK:- Api Response
//=======================
enum ApiResponse {

    static var response: Bool { return true } // response
   
}

extension UIViewController {
    func  customizeColor(string: String, color: UIColor) -> NSMutableAttributedString {
        return NSMutableAttributedString(string: string, attributes:
            [NSAttributedString.Key.foregroundColor : color ])
    }
    func  customizeFont(string: String, font: UIFont) -> NSMutableAttributedString {
        return NSMutableAttributedString(string: string, attributes:
            [NSAttributedString.Key.font : font ])
    }
    func  customizeFont(string: String, font: UIFont,color: UIColor) -> NSMutableAttributedString {
        return NSMutableAttributedString(string: string, attributes:
            [NSAttributedString.Key.font : font ,NSAttributedString.Key.foregroundColor : color ])
    }
    func shadowText(string: String) -> NSMutableAttributedString {
        
        let shadow = NSShadow()
        shadow.shadowBlurRadius = 3
        shadow.shadowOffset = CGSize(width: 3, height: 3)
        shadow.shadowColor = UIColor.gray
        
        return NSMutableAttributedString(string: string, attributes:
            [NSAttributedString.Key.shadow : shadow ])
    }
    
    func strokeWidth(string: String) -> NSMutableAttributedString{
        return NSMutableAttributedString(string: string, attributes: [NSAttributedString.Key.strokeWidth: 2])
    }
    
    func background(string: String)-> NSMutableAttributedString {
        return NSMutableAttributedString(string: string, attributes:
            [NSAttributedString.Key.backgroundColor : UIColor.lightGray ])
    }
    
    func underline(string: String) -> NSMutableAttributedString{
        return NSMutableAttributedString(string: string, attributes:
            [NSAttributedString.Key.underlineStyle : NSUnderlineStyle.thick.rawValue ])
    }
}
