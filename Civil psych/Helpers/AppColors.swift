//
//  AppColors.swift
//  DannApp
//
//  Created by Aakash Srivastav on 20/04/17.
//  Copyright © 2017 Appinventiv. All rights reserved.
//

import Foundation
import UIKit


enum AppColors {
    
    static var themeColor: UIColor                  { return #colorLiteral(red: 0.3176470588, green: 0.5803921569, blue: 0.4588235294, alpha: 1) } // rgb 81 148 117
    static var whiteColor: UIColor                  { return #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1) } // rgb 255 255 255
    static var textfield_Border: UIColor            { return #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1) } // rgb 153 153 153
    static var textfield_Text: UIColor              { return #colorLiteral(red: 0.3333333333, green: 0.3333333333, blue: 0.3333333333, alpha: 1) } //
    static var textfield_Title: UIColor             { return #colorLiteral(red: 0.9843137255, green: 0.4235294118, blue: 0.4274509804, alpha: 1) } // rgb 251 108 109
    static var textfield_Error: UIColor             { return #colorLiteral(red: 0.521568656, green: 0.1098039225, blue: 0.05098039284, alpha: 1) } // rgb 133 28 13
    static var loaderColor: UIColor                 { return #colorLiteral(red: 0.9843137255, green: 0.4235294118, blue: 0.4274509804, alpha: 1) } // rgb 251 108 109
    static var loaderBkgroundColor: UIColor         { return #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0.145708476) } // rgb 0 0 0 alpha 0.5
    static var titleNavColor: UIColor               { return #colorLiteral(red: 0.3176470588, green: 0.5803921569, blue: 0.4588235294, alpha: 1) } // rgb 81 148 117
    static var buttonBkgroundColor: UIColor         { return #colorLiteral(red: 0.9559454322, green: 0.009856586345, blue: 0.003329709405, alpha: 1) } // rgb 241 16 26
    
}
