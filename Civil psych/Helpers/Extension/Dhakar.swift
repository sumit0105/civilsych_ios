//
//  Dhakar.swift
//  Kaymo
//
//  Created by YATIN  KALRA on 04/04/19.
//  Copyright © 2019 Dhakar. All rights reserved.
//

import UIKit
import Reachability

class Dhakar: UIViewController {

    @IBOutlet weak var buttonCart: UIButton!
    @IBOutlet weak var lableNoOfCartItem: UILabel!
    @IBOutlet weak var tableViewProduct: UITableView!
    var counterItem = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()

    }
    
    @IBAction func buttonHandlerAddToCart(_ sender: UIButton) {
//
//        let buttonPosition : CGPoint = sender.convert(sender.bounds.origin, to: self.tableViewProduct)
//
//        let indexPath = self.tableViewProduct.indexPathForRow(at: buttonPosition)!
//
//        let cell = tableViewProduct.cellForRow(at: indexPath) as! ProductCell11
//
//        let imageViewPosition : CGPoint = cell.imageViewProduct.convert(cell.imageViewProduct.bounds.origin, to: self.view)
//
//
//        let imgViewTemp = UIImageView(frame: CGRect(x: imageViewPosition.x, y: imageViewPosition.y, width: cell.imageViewProduct.frame.size.width, height: cell.imageViewProduct.frame.size.height))
//
//        imgViewTemp.image = cell.imageViewProduct.image
//
//        animation(tempView: imgViewTemp)
        
    }
    
    
    func animation(tempView : UIView)  {
        self.view.addSubview(tempView)
        UIView.animate(withDuration: 1.0,
                       animations: {
                        tempView.animationZoom(scaleX: 1.5, y: 1.5)
        }, completion: { _ in
            
            UIView.animate(withDuration: 0.5, animations: {
                
                tempView.animationZoom(scaleX: 0.2, y: 0.2)
                tempView.animationRoted(angle: CGFloat(Double.pi))
                
                tempView.frame.origin.x = self.buttonCart.frame.origin.x
                tempView.frame.origin.y = self.buttonCart.frame.origin.y
                
            }, completion: { _ in
                
                tempView.removeFromSuperview()
                
                UIView.animate(withDuration: 1.0, animations: {
                    
                    self.counterItem += 1
                    self.lableNoOfCartItem.text = "\(self.counterItem)"
                    self.buttonCart.animationZoom(scaleX: 1.4, y: 1.4)
                }, completion: {_ in
                    self.buttonCart.animationZoom(scaleX: 1.0, y: 1.0)
                })
                
            })
            
        })
    }
}

extension UIView{
    func animationZoom(scaleX: CGFloat, y: CGFloat) {
        self.transform = CGAffineTransform(scaleX: scaleX, y: y)
    }
    
    func animationRoted(angle : CGFloat) {
        self.transform = self.transform.rotated(by: angle)
    }
}

extension UIViewController {
    func AlertControllerCuston(title:String?,message:String?,BtnTitle:[String]?,completion: @escaping (_ dict:String) -> Void) {
        
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        if BtnTitle != nil {
            
            for tickMark in stride(from: 0, to: BtnTitle!.count, by: 1) {
                let name :String = BtnTitle![tickMark]
                let tit : UIAlertAction = UIAlertAction(title: name, style: UIAlertAction.Style.default) { (result) in
                    print(result.title!)
                    completion(result.title!)
                }
                alert.addAction(tit)
                
            }
            self.present(alert, animated: true
                , completion:   nil)
        }
    }
    func AlertControllerOnr(title:String?,message:String?,BtnTitle:String = "OK") {
        
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        let tit : UIAlertAction = UIAlertAction(title: BtnTitle, style: UIAlertAction.Style.default) { (result) in
            print(result.title!)
        }
        alert.addAction(tit)
        self.present(alert, animated: true, completion:   nil)
    }
    func isValidEmail(testStr:String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
    }
    func isPasswordValid(_ password : String) -> Bool{
        let passwordTest = NSPredicate(format: "SELF MATCHES %@", "^(?=.*[a-z])(?=.*[$@$#!%*?&])[A-Za-z\\d$@$#!%*?&]{8,}")
        return passwordTest.evaluate(with: password)
    }
    func validate(value: String) -> Bool {
        let PHONE_REGEX = "^\\d{3}-\\d{3}-\\d{4}$"
        let phoneTest = NSPredicate(format: "SELF MATCHES %@", PHONE_REGEX)
        let result =  phoneTest.evaluate(with: value)
        return result
    }
    
    func format(with mask: String, phone: String) -> String {
        let numbers = phone.replacingOccurrences(of: "[^0-9]", with: "", options: .regularExpression)
        var result = ""
        var index = numbers.startIndex // numbers iterator
        for ch in mask where index < numbers.endIndex {
            if ch == "X" {
                result.append(numbers[index])
                index = numbers.index(after: index)

            } else {
                result.append(ch) // just append a mask character
            }
        }
        return result
    }
    
    func pavan_checkNumberOrAlphabet(Str:String) -> Bool {
       
        let emailRegEx = "[0-9+]"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        print(emailTest.evaluate(with: Str))
        if !emailTest.evaluate(with: Str) {
            return false
        }
        return true
    }
    
    func pavan_checkAlphabet(Str:String) -> Bool {
        let emailRegEx = "[A-Za-z]"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        print(emailTest.evaluate(with: Str))
        if !emailTest.evaluate(with: Str) {
            return false
        }
        return true
    }
    
    func Pavan_CheckInternet(ShowWorning:Bool) -> Bool {
        let reachability = try! Reachability()

        reachability.whenReachable = { reachability in
            if reachability.connection == .wifi {
                print("Reachable via WiFi")
            } else {
                print("Reachable via Cellular")
            }
            return
        }
        reachability.whenUnreachable = { _ in
            print("Not reachable")
            let alert = UIAlertController(title: "No Internet Connection", message: "Make sure your device is connected to the internet.", preferredStyle: .alert)
            
            alert.addAction(UIAlertAction(title: "Setting", style: .default, handler: { (result) in
                
                let settingsUrl = URL(string: UIApplication.openSettingsURLString)
                if let url = settingsUrl {
                    UIApplication.shared.openURL(url)
                }
                //                UIApplication.shared.open(URL(string:"App-Prefs:root=General")!, options: [:], completionHandler: { (success) in
                //                    print("Settings opened: \(success)") // Prints true
                //                })
            }))
            alert.addAction(UIAlertAction(title: "Cancel", style: .default, handler: { (result) in
            }))
            if ShowWorning {
                let window = UIWindow(frame: UIScreen.main.bounds)
                window.rootViewController = UIViewController()
                window.windowLevel = UIWindow.Level.alert
                window.makeKeyAndVisible()
                window.rootViewController?.present(alert, animated: false, completion: nil)
                //                self.present(alert, animated: true, completion:   nil)
            }
        }

        do {
            try reachability.startNotifier()
        } catch {
            print("Unable to start notifier")
        }
        
//        let shared = try! Reachability()
//        if shared.isReachable {
//            return true
//        }else{
//            let alert = UIAlertController(title: "No Internet Connection", message: "Make sure your device is connected to the internet.", preferredStyle: .alert)
//
//            alert.addAction(UIAlertAction(title: "Setting", style: .default, handler: { (result) in
//
//                let settingsUrl = URL(string: UIApplication.openSettingsURLString)
//                if let url = settingsUrl {
//                    UIApplication.shared.openURL(url)
//                }
////                UIApplication.shared.open(URL(string:"App-Prefs:root=General")!, options: [:], completionHandler: { (success) in
////                    print("Settings opened: \(success)") // Prints true
////                })
//            }))
//            alert.addAction(UIAlertAction(title: "Cancel", style: .default, handler: { (result) in
//            }))
//            if ShowWorning {
//                let window = UIWindow(frame: UIScreen.main.bounds)
//                window.rootViewController = UIViewController()
//                window.windowLevel = UIWindow.Level.alert
//                window.makeKeyAndVisible()
//                window.rootViewController?.present(alert, animated: false, completion: nil)
////                self.present(alert, animated: true, completion:   nil)
//            }
//        }
        return false
    }
    
    func showToast(message : String,BackGroundColor:UIColor = UIColor.black.withAlphaComponent(1),textColor:UIColor = UIColor.white) {
        DispatchQueue.main.async {
            let toastLabel = UILabel(frame: CGRect(x: self.view.frame.size.width/2 - 150, y: self.view.frame.size.height-130, width: 300, height: 60))
            toastLabel.backgroundColor = BackGroundColor
            toastLabel.textColor = textColor
            toastLabel.textAlignment = .center;
            toastLabel.font = UIFont(name: "Montserrat-Light", size: 12.0)
            toastLabel.text = message
            toastLabel.alpha = 1.0
            toastLabel.numberOfLines = 0
            toastLabel.layer.cornerRadius = 10;
            toastLabel.clipsToBounds  =  true
            self.view.addSubview(toastLabel)
            UIView.animate(withDuration: 4.0, delay: 0.1, options: .curveEaseOut, animations: {
                toastLabel.alpha = 0.0
            }, completion: {(isCompleted) in
                toastLabel.removeFromSuperview()
            })
        }
    }
    func randomStringWithLength (len : Int) -> NSString {
        
        let letters : NSString = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
        
        let randomString : NSMutableString = NSMutableString(capacity: len)
        
        for _ in 0 ... len {
            let length = UInt32 (letters.length)
            let rand = arc4random_uniform(length)
            randomString.appendFormat("%C", letters.character(at: Int(rand)))
        }
        return randomString
    }
    func randomStringWithLengthName (len : Int) -> NSString {
        
        let letters1 : NSString = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
        
        let randomString : NSMutableString = NSMutableString(capacity: len)
        if len > 0 {
            for _ in 0 ..< 1 {
                let length = UInt32 (letters1.length)
                let rand = arc4random_uniform(length)
                randomString.appendFormat("%C", letters1.character(at: Int(rand)))
            }
        }
        let letters : NSString = "abcdefghijklmnopqrstuvwxyz"
        
        for _ in 1 ... len {
            let length = UInt32 (letters.length)
            let rand = arc4random_uniform(length)
            randomString.appendFormat("%C", letters.character(at: Int(rand)))
        }
        return randomString
    }
    func pavan_tapToSeeFullImage(imageView:UIImageView) {
        imageView.isUserInteractionEnabled = true
        let tap = UITapGestureRecognizer(target: self, action: #selector(imageTapped(_:)))
        imageView.addGestureRecognizer(tap)
    }
    
    
    @IBAction func imageTapped(_ sender: UITapGestureRecognizer) {
        let imageView = sender.view as! UIImageView
        let newImageView = UIImageView(image: imageView.image)
        let frame = self.view.convert(imageView.frame, from:imageView)
        //        new_img = frame
        newImageView.frame = frame
        if #available(iOS 11.0, *) {
            newImageView.accessibilityFrame = frame
        } else {
            // Fallback on earlier versions
        }
        newImageView.backgroundColor = .black
        newImageView.contentMode = .scaleAspectFit
        newImageView.isUserInteractionEnabled = true
        //        let tap = UITapGestureRecognizer(target: self, action: #selector(dismissFullscreenImage))
        let tap = UISwipeGestureRecognizer(target: self, action: #selector(dismissFullscreenImage))
        newImageView.addGestureRecognizer(tap)
        tap.direction = .up
        self.view.addSubview(newImageView)
        self.navigationController?.isNavigationBarHidden = true
        self.tabBarController?.tabBar.isHidden = true
        
        UIView.animate(withDuration: 0.5, animations: {
            newImageView.frame = UIScreen.main.bounds
        }) { (_) in
            newImageView.frame = UIScreen.main.bounds
        }
    }
    
    @objc func dismissFullscreenImage(_ sender: UITapGestureRecognizer) {
        self.navigationController?.isNavigationBarHidden = false
        self.tabBarController?.tabBar.isHidden = false
        sender.view?.removeFromSuperview()
        
        let imageView = sender.view as! UIImageView
        let newImageView = UIImageView(image: imageView.image)
        newImageView.frame = UIScreen.main.bounds
        newImageView.backgroundColor = .black
        newImageView.contentMode = .scaleAspectFit
        newImageView.isUserInteractionEnabled = true
        self.view.addSubview(newImageView)
        let f = imageView.accessibilityFrame
        UIView.animate(withDuration: 0.5, animations: {
            newImageView.frame = f
        }) { (_) in
            newImageView.removeFromSuperview()
        }
    }
    func Pavan_OpenOnSetting(completion:@escaping(_ obj:AnyObject) -> Void) {
        let appSettings = URL(string: UIApplication.openSettingsURLString + Bundle.main.bundleIdentifier!)
        if #available(iOS 10.0, *) {
            UIApplication.shared.open(appSettings!, options: [:], completionHandler: { (success) in
                print("Settings opened: \(success)") // Prints true
            })
        } else {
            UIApplication.shared.openURL(appSettings!)
            print("Settings opened:") // Prints true
            
        }
    }
    
 
    func pavan_changeDateFoprmate(YourDate:String,FormatteFrom:String,FormatteTo:String) -> String {
        var valL = YourDate
        if YourDate != "" {
            let dateFormatte = DateFormatter()
            dateFormatte.dateFormat = FormatteFrom
            let dateNeweSE = dateFormatte.date(from: valL)
            dateFormatte.dateFormat = FormatteTo
            valL = dateFormatte.string(from: dateNeweSE!)
        }
        return valL
    }
    
    func setTextlbl(htmlText:String) -> String{
        let encodedData = htmlText.data(using: String.Encoding.utf8)!
        var attributedString: NSAttributedString
        
        do {
            attributedString = try NSAttributedString(data: encodedData, options: [NSAttributedString.DocumentReadingOptionKey.documentType:NSAttributedString.DocumentType.html,NSAttributedString.DocumentReadingOptionKey.characterEncoding:NSNumber(value: String.Encoding.utf8.rawValue)], documentAttributes: nil)
            print(attributedString.string)
            return attributedString.string
        } catch let error as NSError {
            print(error.localizedDescription)
            return error.localizedDescription
        } catch {
            print("error")
            return "Error"
        }
    }
    
    func pav_changeDateFoprmate(YourDate:String,FormatteFrom:String,FormatteTo:String) -> String {
        var valL = YourDate
        if YourDate != "" {
            let dateFormatte = DateFormatter()
            dateFormatte.dateFormat = FormatteFrom
            let dateNeweSE = dateFormatte.date(from: valL)
            dateFormatte.dateFormat = FormatteTo
            valL = dateFormatte.string(from: dateNeweSE!)
        }
        return valL
    }    
}
extension UIImageView {
    func pavan_tapToSeeFullImage() {
        self.isUserInteractionEnabled = true
        let tap = UITapGestureRecognizer(target: self, action: #selector(imageTapped(_:)))
        self.addGestureRecognizer(tap)
    }
    
    @IBAction func imageTapped(_ sender: UITapGestureRecognizer) {
        guard let imageView = sender.view as? UIImageView else{
            return
        }
        guard let im = imageView.image else {
            return
        }
        
        let newImageView = UIImageView(image: im)
        let frame = self.window!.convert(imageView.frame, from:imageView)
        //        new_img = frame
        newImageView.frame = frame
        if #available(iOS 11.0, *) {
            newImageView.accessibilityFrame = frame
        } else {
            newImageView.accessibilityFrame = frame
            // Fallback on earlier versions
        }
        newImageView.backgroundColor = .black
        newImageView.contentMode = .scaleAspectFit
        newImageView.isUserInteractionEnabled = true
        //        let tap = UITapGestureRecognizer(target: self, action: #selector(dismissFullscreenImage))
        let tap = UISwipeGestureRecognizer(target: self, action: #selector(dismissFullscreenImage))
        newImageView.addGestureRecognizer(tap)
        tap.direction = .init(arrayLiteral: .up,.down)
        self.window!.addSubview(newImageView)
        
        UIView.animate(withDuration: 0.5, animations: {
            newImageView.frame = UIScreen.main.bounds
        }) { (_) in
            newImageView.frame = UIScreen.main.bounds
        }
    }
    
    @objc func dismissFullscreenImage(_ sender: UITapGestureRecognizer) {
        sender.view?.removeFromSuperview()
        
        let imageView = sender.view as! UIImageView
        let newImageView = UIImageView(image: imageView.image)
        newImageView.frame = UIScreen.main.bounds
        newImageView.backgroundColor = .black
        newImageView.contentMode = .scaleAspectFit
        newImageView.isUserInteractionEnabled = true
        self.window!.addSubview(newImageView)
        let f = imageView.accessibilityFrame
        UIView.animate(withDuration: 0.5, animations: {
            newImageView.frame = f
        }) { (_) in
            newImageView.removeFromSuperview()
        }
    }
    
    
}
extension UIImage {
    func pavan_imageWithBorder(width: CGFloat, color: UIColor) -> UIImage? {
        let square = CGSize(width: min(size.width, size.height) + width * 2, height: min(size.width, size.height) + width * 2)
        let imageView = UIImageView(frame: CGRect(origin: CGPoint(x: 0, y: 0), size: square))
        imageView.contentMode = .center
        imageView.image = self
        imageView.layer.borderWidth = width
        imageView.layer.borderColor = color.cgColor
        UIGraphicsBeginImageContextWithOptions(imageView.bounds.size, false, scale)
        guard let context = UIGraphicsGetCurrentContext() else { return nil }
        imageView.layer.render(in: context)
        let result = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return result
    }
}
extension UITableView {
    
    func scrollToBottom(animation:Bool){
        DispatchQueue.main.async {
            let indexPath = IndexPath(
                row: self.numberOfRows(inSection: self.numberOfSections - 1) - 1,
                section: self.numberOfSections - 1)
            self.scrollToRow(at: indexPath, at: .bottom, animated: animation)
        }
    }
    func scrollToTop(animation:Bool) {
        DispatchQueue.main.async {
            let indexPath = IndexPath(row: 0, section: 0)
            self.scrollToRow(at: indexPath, at: .top, animated: animation)
        }
    }
    func scrollToLastCell(animated : Bool) {
         let lastSectionIndex = self.numberOfSections - 1 // last section
        if lastSectionIndex >= 0 {
            let lastRowIndex = self.numberOfRows(inSection: lastSectionIndex) - 1 // last row
            if lastRowIndex > 0 {
                self.scrollToRow(at: IndexPath(row: lastRowIndex, section: lastSectionIndex), at: .bottom, animated: animated)
            }
            
        }
        
    }
}
extension String {
    func removeImage(filename: String?) {
        do {
            let fileManager = FileManager.default
            
            let documentDirectory = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as String
            let path = documentDirectory.appending("/\(filename!)")
            print(path)
            let success = try? fileManager.removeItem(atPath: path)
            if (success != nil) {
                print("delete - \(filename!)")
            } else {
                print("Could not delete file")
            }
        }
        catch let error as NSError {
            print("Ooops! Something went wrong: \(error)")
        }
    }
    func SetPlistData(categoryName:String, data:Any){
        
        let pArr = NSMutableArray(contentsOfFile: "user".pListCreation(filename: categoryName) as String)
        
        if pArr == nil {
            let mutableDict : NSMutableArray = []
            mutableDict.add(data)
            mutableDict.write(toFile: "".pListCreation(filename: categoryName) as String , atomically: true)
            return
        }
        let newDictionary : NSMutableArray = []
        newDictionary.addObjects(from: pArr as! [Any])
        newDictionary.add(data)
        newDictionary.write(toFile: "userID".pListCreation(filename: categoryName) as String , atomically: true)
        
    }
    func GetPlistData(categoryName:String) -> Any {
        let pArr = NSMutableArray(contentsOfFile: "user".pListCreation(filename: categoryName) as String)
        if pArr == nil {
            return ""
        }
        return pArr as Any
    }
    func DeletePlistData(categoryName:String) {
        let newDictionary : NSMutableArray = []
        newDictionary.write(toFile: "userID".pListCreation(filename: categoryName) as String , atomically: true)
    }
    
    func pListCreation(filename: String?) -> NSString {
        
        let fileManager = FileManager.default
        let documentDirectory = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as String
        let path = documentDirectory.appending("/\(filename!)")
        print(path)
        if (!fileManager.fileExists(atPath: path)) {
            
        }else{
            print("file already exist")
        }
        return path as NSString
    }
}

extension String {
    var isPhoneNumber: Bool {
        do {
            let detector = try NSDataDetector(types: NSTextCheckingResult.CheckingType.phoneNumber.rawValue)
            let matches = detector.matches(in: self, options: [], range: NSMakeRange(0, self.count))
            if let res = matches.first {
                return res.resultType == .phoneNumber && res.range.location == 0 && res.range.length == self.count
            } else {
                return false
            }
        } catch {
            return false
        }
    }
    func isPhone()->Bool {
        if self.isAllDigits() == true {
            let phoneRegex = "[235689][0-9]{6}([0-9]{3})?"
            let predicate = NSPredicate(format: "SELF MATCHES %@", phoneRegex)
            return  predicate.evaluate(with: self)
        }else {
            return false
        }
    }
    
    func isAllDigits()->Bool {
        let charcterSet  = NSCharacterSet(charactersIn: "+0123456789").inverted
        let inputString = self.components(separatedBy: charcterSet)
        let filtered = inputString.joined(separator: "")
        return  self == filtered
    }
}
extension UISearchBar {
    public func setSerchTextcolor(color: UIColor) {
        let clrChange = subviews.flatMap { $0.subviews }
        guard let sc = (clrChange.filter { $0 is UITextField }).first as? UITextField else { return }
        sc.textColor = color
        sc.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)

        //        sc.layer.borderColor = #colorLiteral(red: 0.2549019754, green: 0.2745098174, blue: 0.3019607961, alpha: 1)
        //        sc.layer.borderWidth = 1
        sc.layer.cornerRadius = 10
        sc.clipsToBounds = true
    }
}
extension UIView{
    func roundedTopLeft(Radius:CGFloat = 15){
        let maskPath1 = UIBezierPath(roundedRect: bounds,
                                     byRoundingCorners: [.topLeft],
                                     cornerRadii: CGSize(width: Radius, height: Radius))
        let maskLayer1 = CAShapeLayer()
        maskLayer1.frame = bounds
        maskLayer1.path = maskPath1.cgPath
        layer.mask = maskLayer1
    }
    
    func roundedTopRight(Radius:CGFloat = 15){
        let maskPath1 = UIBezierPath(roundedRect: bounds,
                                     byRoundingCorners: [.topRight],
                                     cornerRadii: CGSize(width: Radius, height: Radius))
        let maskLayer1 = CAShapeLayer()
        maskLayer1.frame = bounds
        maskLayer1.path = maskPath1.cgPath
        layer.mask = maskLayer1
    }
    func roundedBottomLeft(Radius:CGFloat = 15){
        let maskPath1 = UIBezierPath(roundedRect: bounds,
                                     byRoundingCorners: [.bottomLeft],
                                     cornerRadii: CGSize(width: Radius, height: Radius))
        let maskLayer1 = CAShapeLayer()
        maskLayer1.frame = bounds
        maskLayer1.path = maskPath1.cgPath
        layer.mask = maskLayer1
    }
    func roundedBottomRight(Radius:CGFloat = 15){
        let maskPath1 = UIBezierPath(roundedRect: bounds,
                                     byRoundingCorners: [.bottomRight],
                                     cornerRadii: CGSize(width: Radius, height: Radius))
        let maskLayer1 = CAShapeLayer()
        maskLayer1.frame = bounds
        maskLayer1.path = maskPath1.cgPath
        layer.mask = maskLayer1
    }
    func roundedBottom(Radius:CGFloat = 15){
        let maskPath1 = UIBezierPath(roundedRect: bounds,
                                     byRoundingCorners: [.bottomRight , .bottomLeft],
                                     cornerRadii: CGSize(width: Radius, height: Radius))
        let maskLayer1 = CAShapeLayer()
        maskLayer1.frame = bounds
        maskLayer1.path = maskPath1.cgPath
        layer.mask = maskLayer1
    }
    func roundedTop(Radius:CGFloat = 15){
        let maskPath1 = UIBezierPath(roundedRect: bounds,
                                     byRoundingCorners: [.topRight , .topLeft],
                                     cornerRadii: CGSize(width: Radius, height: Radius))
        let maskLayer1 = CAShapeLayer()
        maskLayer1.frame = bounds
        maskLayer1.path = maskPath1.cgPath
        layer.mask = maskLayer1
    }
    func roundedLeft(Radius:CGFloat = 15){
        let maskPath1 = UIBezierPath(roundedRect: bounds,
                                     byRoundingCorners: [.topLeft , .bottomLeft],
                                     cornerRadii: CGSize(width: Radius, height: Radius))
        let maskLayer1 = CAShapeLayer()
        maskLayer1.frame = bounds
        maskLayer1.path = maskPath1.cgPath
        layer.mask = maskLayer1
    }
    func roundedRight(Radius:CGFloat = 15){
        let maskPath1 = UIBezierPath(roundedRect: bounds,
                                     byRoundingCorners: [.topRight , .bottomRight],
                                     cornerRadii: CGSize(width: Radius, height: Radius))
        let maskLayer1 = CAShapeLayer()
        maskLayer1.frame = bounds
        maskLayer1.path = maskPath1.cgPath
        layer.mask = maskLayer1
    }
    func roundedAllCorner(Radius:CGFloat = 15){
        let maskPath1 = UIBezierPath(roundedRect: bounds,
                                     byRoundingCorners: [.topRight , .bottomRight , .topLeft , .bottomLeft],
                                     cornerRadii: CGSize(width: Radius, height: Radius))
        let maskLayer1 = CAShapeLayer()
        maskLayer1.frame = bounds
        maskLayer1.path = maskPath1.cgPath
        layer.mask = maskLayer1
    }
    func roundView(cornerRadius:Bool,borderWidth:CGFloat = 0.0,borderColor:CGColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0)) {
        if cornerRadius {
            self.layer.cornerRadius = CGFloat(self.frame.size.height/2)
        }
        self.layer.borderWidth = CGFloat(borderWidth)
        self.layer.borderColor = borderColor
    }
    
}
extension String {
    func fileName() -> String {
        return URL(fileURLWithPath: self).deletingPathExtension().lastPathComponent
    }
    
    func fileExtension() -> String {
        return URL(fileURLWithPath: self).pathExtension
    }
    func fileNameWithExtension() -> String {
        let pn = self.fileName()
        let En = self.fileExtension()
        return  "\(pn).\(En)"
//        let nameStr = URL(fileURLWithPath: self).deletingPathExtension().lastPathComponent
//        let extensionStr = URL(fileURLWithPath: self).pathExtension
//        return  "\(nameStr).\(extensionStr)"
    }
    
}

extension UIColor {
    
    convenience init(hex:Int, alpha:CGFloat = 1.0) {
        self.init(
            red:   CGFloat((hex & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((hex & 0x00FF00) >> 8)  / 255.0,
            blue:  CGFloat((hex & 0x0000FF) >> 0)  / 255.0,
            alpha: alpha
        )
    }
    
}

extension UIView {
    
    func rotate(_ toValue: CGFloat, duration: CFTimeInterval = 0.2) {
        let animation = CABasicAnimation(keyPath: "transform.rotation")
        
        animation.toValue = toValue
        animation.duration = duration
        animation.isRemovedOnCompletion = false
        animation.fillMode = CAMediaTimingFillMode.forwards
        
        self.layer.add(animation, forKey: nil)
    }
    
}
extension Decimal {
    var doubleValue:Double {
        return NSDecimalNumber(decimal:self).doubleValue
    }
}
extension String {
    func remove0Fromleading() -> String {
        let fullNameArr = self.components(separatedBy: ".")
        if fullNameArr.count == 2 {
            if fullNameArr[0] == "0" {
                let oo = ".\(fullNameArr[1])"
                let ss = Double(round(Double(oo)! * 10000000)/10000000)
                return String(format: "%.8f", ss)
            }else{
                var ss = Double(self)
                ss = Double(round(ss! * 10000000)/10000000)
                return "\(ss!)"
            }
        }else{
            var ss = Double(self)
            ss = Double(round(ss! * 10000000)/10000000)
            return "\(ss!)"
        }
    }
    
    func add0InLast(length:Int) -> String {
        var newn = ""
        let pp = self.description.components(separatedBy: ".")
        if pp.count > 1 {
            for i in stride(from: 0, to: length, by: 1) {
                let str = pp[1]
                if pp[1].count > i {
                    let index = str.index(str.startIndex, offsetBy: i)
                    newn = newn + "\(str[index])"
                }else{
                    newn = newn + "0"
                }
            }
        }else{
            newn = "00000000"
        }
        newn = pp[0] + "." + newn
        return newn
    }
    func P_remove0() -> String {
        let fullNameArr = self.components(separatedBy: ".")
        if fullNameArr.count == 2 {
            if fullNameArr[0] == "0" {
                return ".\(fullNameArr[1])"
            }
        }
        return self
    }
}

extension String {
    subscript (i: Int) -> Character {
        return self[index(startIndex, offsetBy: i)]
    }
    subscript (bounds: CountableRange<Int>) -> Substring {
        let start = index(startIndex, offsetBy: bounds.lowerBound)
        let end = index(startIndex, offsetBy: bounds.upperBound)
        return self[start ..< end]
    }
    subscript (bounds: CountableClosedRange<Int>) -> Substring {
        let start = index(startIndex, offsetBy: bounds.lowerBound)
        let end = index(startIndex, offsetBy: bounds.upperBound)
        return self[start ... end]
    }
    subscript (bounds: CountablePartialRangeFrom<Int>) -> Substring {
        let start = index(startIndex, offsetBy: bounds.lowerBound)
        let end = index(endIndex, offsetBy: -1)
        return self[start ... end]
    }
    subscript (bounds: PartialRangeThrough<Int>) -> Substring {
        let end = index(startIndex, offsetBy: bounds.upperBound)
        return self[startIndex ... end]
    }
    subscript (bounds: PartialRangeUpTo<Int>) -> Substring {
        let end = index(startIndex, offsetBy: bounds.upperBound)
        return self[startIndex ..< end]
    }
}
extension Substring {
    subscript (i: Int) -> Character {
        return self[index(startIndex, offsetBy: i)]
    }
    subscript (bounds: CountableRange<Int>) -> Substring {
        let start = index(startIndex, offsetBy: bounds.lowerBound)
        let end = index(startIndex, offsetBy: bounds.upperBound)
        return self[start ..< end]
    }
    subscript (bounds: CountableClosedRange<Int>) -> Substring {
        let start = index(startIndex, offsetBy: bounds.lowerBound)
        let end = index(startIndex, offsetBy: bounds.upperBound)
        return self[start ... end]
    }
    subscript (bounds: CountablePartialRangeFrom<Int>) -> Substring {
        let start = index(startIndex, offsetBy: bounds.lowerBound)
        let end = index(endIndex, offsetBy: -1)
        return self[start ... end]
    }
    subscript (bounds: PartialRangeThrough<Int>) -> Substring {
        let end = index(startIndex, offsetBy: bounds.upperBound)
        return self[startIndex ... end]
    }
    subscript (bounds: PartialRangeUpTo<Int>) -> Substring {
        let end = index(startIndex, offsetBy: bounds.upperBound)
        return self[startIndex ..< end]
    }
}


public enum Model : String {
    
    //Simulator
    case simulator     = "simulator/sandbox",
    
    //iPod
    iPod1              = "iPod 1",
    iPod2              = "iPod 2",
    iPod3              = "iPod 3",
    iPod4              = "iPod 4",
    iPod5              = "iPod 5",
    
    //iPad
    iPad2              = "iPad 2",
    iPad3              = "iPad 3",
    iPad4              = "iPad 4",
    iPadAir            = "iPad Air ",
    iPadAir2           = "iPad Air 2",
    iPadAir3           = "iPad Air 3",
    iPad5              = "iPad 5", //iPad 2017
    iPad6              = "iPad 6", //iPad 2018
    
    //iPad Mini
    iPadMini           = "iPad Mini",
    iPadMini2          = "iPad Mini 2",
    iPadMini3          = "iPad Mini 3",
    iPadMini4          = "iPad Mini 4",
    iPadMini5          = "iPad Mini 5",
    
    //iPad Pro
    iPadPro9_7         = "iPad Pro 9.7\"",
    iPadPro10_5        = "iPad Pro 10.5\"",
    iPadPro11          = "iPad Pro 11\"",
    iPadPro12_9        = "iPad Pro 12.9\"",
    iPadPro2_12_9      = "iPad Pro 2 12.9\"",
    iPadPro3_12_9      = "iPad Pro 3 12.9\"",
    
    //iPhone
    iPhone4            = "iPhone 4",
    iPhone4S           = "iPhone 4S",
    iPhone5            = "iPhone 5",
    iPhone5S           = "iPhone 5S",
    iPhone5C           = "iPhone 5C",
    iPhone6            = "iPhone 6",
    iPhone6Plus        = "iPhone 6 Plus",
    iPhone6S           = "iPhone 6S",
    iPhone6SPlus       = "iPhone 6S Plus",
    iPhoneSE           = "iPhone SE",
    iPhone7            = "iPhone 7",
    iPhone7Plus        = "iPhone 7 Plus",
    iPhone8            = "iPhone 8",
    iPhone8Plus        = "iPhone 8 Plus",
    iPhoneX            = "iPhone X",
    iPhoneXS           = "iPhone XS",
    iPhoneXSMax        = "iPhone XS Max",
    iPhoneXR           = "iPhone XR",
    
    //Apple TV
    AppleTV            = "Apple TV",
    AppleTV_4K         = "Apple TV 4K",
    unrecognized       = "?unrecognized?"
}

// #-#-#-#-#-#-#-#-#-#-#-#-#
// MARK: UIDevice extensions
// #-#-#-#-#-#-#-#-#-#-#-#-#

public extension UIDevice {
    
    var type: Model {
        var systemInfo = utsname()
        uname(&systemInfo)
        let modelCode = withUnsafePointer(to: &systemInfo.machine) {
            $0.withMemoryRebound(to: CChar.self, capacity: 1) {
                ptr in String.init(validatingUTF8: ptr)
            }
        }
        
        var modelMap : [String: Model] = [
            
            //Simulator
            "i386"      : .simulator,
            "x86_64"    : .simulator,
            
            //iPod
            "iPod1,1"   : .iPod1,
            "iPod2,1"   : .iPod2,
            "iPod3,1"   : .iPod3,
            "iPod4,1"   : .iPod4,
            "iPod5,1"   : .iPod5,
            
            //iPad
            "iPad2,1"   : .iPad2,
            "iPad2,2"   : .iPad2,
            "iPad2,3"   : .iPad2,
            "iPad2,4"   : .iPad2,
            "iPad3,1"   : .iPad3,
            "iPad3,2"   : .iPad3,
            "iPad3,3"   : .iPad3,
            "iPad3,4"   : .iPad4,
            "iPad3,5"   : .iPad4,
            "iPad3,6"   : .iPad4,
            "iPad4,1"   : .iPadAir,
            "iPad4,2"   : .iPadAir,
            "iPad4,3"   : .iPadAir,
            "iPad5,3"   : .iPadAir2,
            "iPad5,4"   : .iPadAir2,
            "iPad6,11"  : .iPad5, //iPad 2017
            "iPad6,12"  : .iPad5,
            "iPad7,5"   : .iPad6, //iPad 2018
            "iPad7,6"   : .iPad6,
            
            //iPad Mini
            "iPad2,5"   : .iPadMini,
            "iPad2,6"   : .iPadMini,
            "iPad2,7"   : .iPadMini,
            "iPad4,4"   : .iPadMini2,
            "iPad4,5"   : .iPadMini2,
            "iPad4,6"   : .iPadMini2,
            "iPad4,7"   : .iPadMini3,
            "iPad4,8"   : .iPadMini3,
            "iPad4,9"   : .iPadMini3,
            "iPad5,1"   : .iPadMini4,
            "iPad5,2"   : .iPadMini4,
            "iPad11,1"  : .iPadMini5,
            "iPad11,2"  : .iPadMini5,
            
            //iPad Pro
            "iPad6,3"   : .iPadPro9_7,
            "iPad6,4"   : .iPadPro9_7,
            "iPad7,3"   : .iPadPro10_5,
            "iPad7,4"   : .iPadPro10_5,
            "iPad6,7"   : .iPadPro12_9,
            "iPad6,8"   : .iPadPro12_9,
            "iPad7,1"   : .iPadPro2_12_9,
            "iPad7,2"   : .iPadPro2_12_9,
            "iPad8,1"   : .iPadPro11,
            "iPad8,2"   : .iPadPro11,
            "iPad8,3"   : .iPadPro11,
            "iPad8,4"   : .iPadPro11,
            "iPad8,5"   : .iPadPro3_12_9,
            "iPad8,6"   : .iPadPro3_12_9,
            "iPad8,7"   : .iPadPro3_12_9,
            "iPad8,8"   : .iPadPro3_12_9,
            
            //iPad Air
            "iPad11,3"  : .iPadAir3,
            "iPad11,4"  : .iPadAir3,
            
            //iPhone
            "iPhone3,1" : .iPhone4,
            "iPhone3,2" : .iPhone4,
            "iPhone3,3" : .iPhone4,
            "iPhone4,1" : .iPhone4S,
            "iPhone5,1" : .iPhone5,
            "iPhone5,2" : .iPhone5,
            "iPhone5,3" : .iPhone5C,
            "iPhone5,4" : .iPhone5C,
            "iPhone6,1" : .iPhone5S,
            "iPhone6,2" : .iPhone5S,
            "iPhone7,1" : .iPhone6Plus,
            "iPhone7,2" : .iPhone6,
            "iPhone8,1" : .iPhone6S,
            "iPhone8,2" : .iPhone6SPlus,
            "iPhone8,4" : .iPhoneSE,
            "iPhone9,1" : .iPhone7,
            "iPhone9,3" : .iPhone7,
            "iPhone9,2" : .iPhone7Plus,
            "iPhone9,4" : .iPhone7Plus,
            "iPhone10,1" : .iPhone8,
            "iPhone10,4" : .iPhone8,
            "iPhone10,2" : .iPhone8Plus,
            "iPhone10,5" : .iPhone8Plus,
            "iPhone10,3" : .iPhoneX,
            "iPhone10,6" : .iPhoneX,
            "iPhone11,2" : .iPhoneXS,
            "iPhone11,4" : .iPhoneXSMax,
            "iPhone11,6" : .iPhoneXSMax,
            "iPhone11,8" : .iPhoneXR,
            
            //Apple TV
            "AppleTV5,3" : .AppleTV,
            "AppleTV6,2" : .AppleTV_4K
        ]
        
        if let model = modelMap[String.init(validatingUTF8: modelCode!)!] {
            if model == .simulator {
                if let simModelCode = ProcessInfo().environment["SIMULATOR_MODEL_IDENTIFIER"] {
                    if let simModel = modelMap[String.init(validatingUTF8: simModelCode)!] {
                        return simModel
                    }
                }
            }
            return model
        }
        return Model.unrecognized
    }
}
extension UILabel {
    func textWidth() -> CGFloat {
        return UILabel.textWidth(label: self)
    }
    
    
    class func textWidth(label: UILabel) -> CGFloat {
        return textWidth(label: label, text: label.text!)
    }
    
    class func textWidth(label: UILabel, text: String) -> CGFloat {
        return textWidth(font: label.font, text: text)
    }
    
    class func textWidth(font: UIFont, text: String) -> CGFloat {
        let myText = text as NSString
        
        let rect = CGSize(width: CGFloat.greatestFiniteMagnitude, height: CGFloat.greatestFiniteMagnitude)
        let labelSize = myText.boundingRect(with: rect, options: .usesLineFragmentOrigin, attributes: [NSAttributedString.Key.font: font], context: nil)
        return ceil(labelSize.width)
    }
    class func textHeight(font: UIFont, text: String) -> CGFloat {
        let myText = text as NSString
        
        let rect = CGSize(width: CGFloat.greatestFiniteMagnitude, height: CGFloat.greatestFiniteMagnitude)
        let labelSize = myText.boundingRect(with: rect, options: .usesLineFragmentOrigin, attributes: [NSAttributedString.Key.font: font], context: nil)
        return ceil(labelSize.height)
    }
}
extension String {
    func heightWithConstrainedWidth(width: CGFloat, font: UIFont) -> CGFloat {
        let constraintRect = CGSize(width: width, height: .greatestFiniteMagnitude)
        let boundingBox = self.boundingRect(with: constraintRect, options: [.usesLineFragmentOrigin, .usesFontLeading], attributes: [NSAttributedString.Key.font: font], context: nil)
        return boundingBox.height
    }
    
    
}

extension String {
    func height(withConstrainedWidth width: CGFloat, font: UIFont) -> CGFloat {
        let constraintRect = CGSize(width: width, height: .greatestFiniteMagnitude)
        let boundingBox = self.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, attributes: [NSAttributedString.Key.font: font], context: nil)

        return ceil(boundingBox.height)
    }

    func width(withConstrainedHeight height: CGFloat, font: UIFont) -> CGFloat {
        let constraintRect = CGSize(width: .greatestFiniteMagnitude, height: height)
        let boundingBox = self.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, attributes: [NSAttributedString.Key.font: font], context: nil)

        return ceil(boundingBox.width)
    }
}
extension UIDevice {
    var hasNotch: Bool {
        let bottom = UIApplication.shared.keyWindow?.safeAreaInsets.bottom ?? 0
        return bottom > 0
    }
}
