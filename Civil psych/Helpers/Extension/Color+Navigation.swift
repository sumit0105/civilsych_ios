//
//  Color+Navigation.swift
//
//  Created by Yes IT Labs  on 20/12/19.
//  Copyright © 2019 yesitlabs. All rights reserved.
//

import Foundation
import UIKit


class setCustomColor : NSObject {
    
    static let shared = setCustomColor()
    func image(from layer: CALayer?) -> UIImage? {
        UIGraphicsBeginImageContext(layer?.frame.size ?? CGSize.zero)
        
        if let context = UIGraphicsGetCurrentContext() {
            layer?.render(in: context)
        }
        let outputImage = UIGraphicsGetImageFromCurrentImageContext()
        
        UIGraphicsEndImageContext()
        
        return outputImage
    }
    func setC() {
        // 0b1824 , 183145
        //        #colorLiteral(red: 0.0431372549, green: 0.09411764706, blue: 0.1411764706, alpha: 1)
        let colorTop = UIColor(red: 11/255, green: 24/255, blue: 36/255, alpha: 1.0).cgColor
        let colorBottom = UIColor(red: 24/255, green: 49/255, blue: 69/255, alpha: 1.0).cgColor
        let gradientLayer = CAGradientLayer()
        gradientLayer.colors = [ colorTop, colorBottom]
        gradientLayer.locations = [ 0.0, 1.0]
        gradientLayer.frame = CGRect(x:0, y:-20, width:375, height:64)
        //        viewCus.navigationController?.navigationBar.layer.addSublayer(gradientLayer)
        UINavigationBar.appearance().setBackgroundImage(image(from: gradientLayer), for: .default)
    }
    
    
    func p_ChangeColor() -> UIColor {
        // 0b1824 , 183145
        //        #colorLiteral(red: 0.0431372549, green: 0.09411764706, blue: 0.1411764706, alpha: 1)
        let colorTop = UIColor(red: 15/255, green: 95/255, blue: 158/255, alpha: 1.0).cgColor
        let colorBottom = UIColor(red: 30/255, green: 190/255, blue: 128/255, alpha: 1.0).cgColor
        let gradientLayer = CAGradientLayer()
        gradientLayer.colors = [ colorTop, colorBottom]
        gradientLayer.locations = [ 0.0, 1.0]
        gradientLayer.frame = CGRect(x:0, y:0, width:414, height:64)
        return UIColor(patternImage: image(from: gradientLayer)!)
    }
    
    func p_setColoeOnBtn(Btn:UIButton) {
        // 0b1824 , 183145
        //        #colorLiteral(red: 0.0431372549, green: 0.09411764706, blue: 0.1411764706, alpha: 1)
        let colorTop = UIColor(red: 15/255, green: 95/255, blue: 158/255, alpha: 1.0).cgColor
        let colorBottom = UIColor(red: 30/255, green: 190/255, blue: 128/255, alpha: 1.0).cgColor
        //        let colorBottom = UIColor(red: 6/255, green: 59/255, blue: 47/255, alpha: 1.0).cgColor
        let gradientLayer = CAGradientLayer()
        gradientLayer.colors = [ colorTop, colorBottom]
        gradientLayer.locations = [ 0.0, 1.0]
        gradientLayer.frame = CGRect(x:0, y:0 , width:Btn.frame.size.width, height:Btn.frame.size.height)
        gradientLayer.startPoint = CGPoint(x: 0.0, y: 0.0)
        gradientLayer.endPoint = CGPoint(x: 0.0, y: 1.0)
        Btn.backgroundColor = UIColor(patternImage: image(from: gradientLayer)!)
    }
    
    func p_setTextBtnColor(Btn:UIButton) {
        // 0b1824 , 183145
        //        #colorLiteral(red: 0.0431372549, green: 0.09411764706, blue: 0.1411764706, alpha: 1)
        let colorTop = UIColor(red: 15/255, green: 95/255, blue: 158/255, alpha: 1.0).cgColor
        let colorBottom = UIColor(red: 30/255, green: 190/255, blue: 128/255, alpha: 1.0).cgColor
        //        let colorBottom = UIColor(red: 6/255, green: 59/255, blue: 47/255, alpha: 1.0).cgColor
        let gradientLayer = CAGradientLayer()
        gradientLayer.colors = [ colorTop, colorBottom]
        gradientLayer.locations = [ 0.0, 1.0]
        gradientLayer.frame = CGRect(x:0, y:0 , width:Btn.frame.size.width, height:Btn.frame.size.height)
        gradientLayer.startPoint = CGPoint(x: 0.0, y: 0.0)
        gradientLayer.endPoint = CGPoint(x: 0.0, y: 1.0)
        Btn.setTitleColor(UIColor(patternImage: image(from: gradientLayer)!), for: .normal)
    }
    
    func p_setTextlblColor(lbl:UILabel) {
        
        let colorTop = UIColor(red: 248/255, green: 233/255, blue: 77/255, alpha: 1.0).cgColor
        let colorBottom = UIColor(red: 237/255, green: 73/255, blue: 106/255, alpha: 1.0).cgColor
        //        let colorBottom = UIColor(red: 15/255, green: 95/255, blue: 158/255, alpha: 1.0).cgColor
        //        let colorBottom = UIColor(red: 6/255, green: 59/255, blue: 47/255, alpha: 1.0).cgColor
        let gradientLayer = CAGradientLayer()
        gradientLayer.colors = [ colorTop, colorBottom]
        gradientLayer.locations = [ 0.0, 1.0]
        gradientLayer.frame = CGRect(x:0, y:0 , width:lbl.frame.size.width, height:lbl.frame.size.height)
        gradientLayer.startPoint = CGPoint(x: 0.0, y: 0.0)
        gradientLayer.endPoint = CGPoint(x: 0.0, y: 1.0)
        lbl.textColor = UIColor(patternImage: image(from: gradientLayer)!)
        
    }
    func p_setViewColor(Vie:UIView) {
        
        //          let colorTop = UIColor(red: 30/255, green: 190/255, blue: 128/255, alpha: 1.0).cgColor
        //          let colorBottom = UIColor(red: 15/255, green: 95/255, blue: 158/255, alpha: 1.0).cgColor
        
        let colorTop = UIColor(red: 15/255, green: 95/255, blue: 158/255, alpha: 1.0).cgColor
        let colorBottom = UIColor(red: 30/255, green: 190/255, blue: 128/255, alpha: 1.0).cgColor
        //        let colorBottom = UIColor(red: 6/255, green: 59/255, blue: 47/255, alpha: 1.0).cgColor
        let gradientLayer = CAGradientLayer()
        gradientLayer.colors = [ colorTop, colorBottom]
        gradientLayer.locations = [ 0.0, 1.0]
        gradientLayer.frame = CGRect(x:0, y:0 , width:Vie.frame.size.width, height:Vie.frame.size.height)
        gradientLayer.startPoint = CGPoint(x: 0.0, y: 0.0)
        gradientLayer.endPoint = CGPoint(x: 0.0, y: 1.0)
        Vie.backgroundColor = UIColor(patternImage: image(from: gradientLayer)!)
        
    }
}
