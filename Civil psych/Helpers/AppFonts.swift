//
//  AppFonts.swift
//  DannApp
//
//  Created by Aakash Srivastav on 20/04/17.
//  Copyright © 2017 Appinventiv. All rights reserved.
//

import Foundation
import UIKit

enum AppFonts : String {
    case Poppins_Black = "Poppins-Black"
    case Poppins_BlackItalic = "Poppins-BlackItalic"
    case Poppins_Bold = "Poppins-Bold"
    case Poppins_BoldItalic = "Poppins-BoldItalic"
    case Poppins_ExtraBold = "Poppins-ExtraBold"
    case Poppins_ExtraBoldItalic = "Poppins-ExtraBoldItalic"
    case Poppins_ExtraLight = "Poppins-ExtraLight"
    case Poppins_ExtraLightItalic = "Poppins-ExtraLightItalic"
    case Poppins_Italic = "Poppins-Italic"
    case Poppins_Light = "Poppins-Light"
    case Poppins_LightItalic = "Poppins-LightItalic"
    case Poppins_MediumItalic = "Poppins-MediumItalic"
    case Poppins_Regular = "Poppins-Regular"
    case Poppins_SemiBold = "Poppins-SemiBold"
    case Poppins_SemiBoldItalic = "Poppins-SemiBoldItalic"
    case Poppins_Thin = "Poppins-Thin"
    case Poppins_ThinItalic = "Poppins-ThinItalic"
    
}

extension AppFonts {
    
    func withSize(_ fontSize: CGFloat) -> UIFont {
        return UIFont(name: self.rawValue, size: fontSize) ?? UIFont.systemFont(ofSize: fontSize)
    }
    
    func withDefaultSize() -> UIFont {
        
        return UIFont(name: self.rawValue, size: 25) ?? UIFont.systemFont(ofSize: 25.0)
    }
    
}

// USAGE : let font = AppFonts.Helvetica.withSize(13.0)
// USAGE : let font = AppFonts.Helvetica.withDefaultSize()
