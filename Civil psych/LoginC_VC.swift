//
//  LoginC_VC.swift
//  Civil psych
//
//  Created by Ankur  on 22/01/21.
//

import UIKit

class LoginC_VC: UIViewController {
    
    @IBOutlet weak var txt_email: UITextField!
    @IBOutlet weak var txt_pass: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initialSetup()
    }
    
    @IBAction func btn_ForgotPass(_ sender:UIButton) {
        
        let vc = ForgotPassC_VC.instantiate(fromAppStoryboard: .LoginClinician)
        vc.select = { [weak self] (str,status) in
            if status == 1 {
                let vc = OtpC_VC.instantiate(fromAppStoryboard: .LoginClinician)
                vc.select = { [weak self] (str,status) in
                    if status == 1 {
                        let vc = ResetPassC_VC.instantiate(fromAppStoryboard: .LoginClinician)
                        self?.present(vc, animated: false, completion: nil)
                    }
                    
                }
                self?.present(vc, animated: false, completion: nil)
            }
            
        }
        self.present(vc, animated: true, completion: nil)
        
    }
    
    @IBAction func btn_Login(_ sender:UIButton) {
        
        self.view.endEditing(true)
        guard validation() else {
            return
        }
        reset()
        let vc = TabC_VC.instantiate(fromAppStoryboard: .TabC)
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
    @IBAction func btn_LoginEmployee(_ sender:UIButton) {
        let vc = LoginE_VC.instantiate(fromAppStoryboard: .LoginEmployee)
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
    @IBAction func btn_SignupClinician(_ sender:UIButton) {
        let vc = SignupC_VC.instantiate(fromAppStoryboard: .LoginClinician)
        self.navigationController?.pushViewController(vc, animated: true)
    }
}

extension LoginC_VC:UITextFieldDelegate {
    
    private func initialSetup() {
        txt_email.delegate = self
        txt_pass.delegate = self
    }
    
    func reset() {
        txt_email.text = ""
        txt_pass.text = ""
    }
    
    func validation() -> Bool {
        
        if self.txt_email.text?.count == 0   {
            AlertControllerOnr(title: alertTitle.alert_alert, message: messageString.E_empEmailPhone)
            return false
        }
        if self.txt_pass.text?.count == 0   {
            AlertControllerOnr(title: alertTitle.alert_alert, message: messageString.E_empPass)
            return false
        }
        return true
    }
    
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
}
