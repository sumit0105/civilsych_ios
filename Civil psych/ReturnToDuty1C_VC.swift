//
//  ReturnToDuty1C_VC.swift
//  Civil psych
//
//  Created by Ankur  on 26/02/21.
//

import UIKit

class ReturnToDuty1C_VC: UIViewController {
    
    @IBOutlet weak var collect:UICollectionView!
    @IBOutlet weak var collect1:UICollectionView!
    @IBOutlet weak var collect1Height:NSLayoutConstraint!
    
    var List : [ReturnToDutyModal] = []
    
    var selectedIndex = IndexPath(item: 0, section: 0)
    
    var collectHeight: CGFloat {
          collect1.layoutIfNeeded()
          return collect1.contentSize.height
      }

    
    override func viewDidLoad() {
        super.viewDidLoad()
        Setup()
        updateHeight(Collect: collect1)
        collect1.reloadData()
    }
    
    func Setup(){
        collect.delegate = self
        collect.dataSource = self
        
        collect1.register(UINib(nibName: "ReturnToDutyC_Cell", bundle: nil), forCellWithReuseIdentifier: "ReturnToDutyC_Cell")
        collect1.delegate = self
        collect1.dataSource = self
        List.append(ReturnToDutyModal(head1: "Drug Education", head2: "How to post an update:", head3: "Employee Updated"))
    }
    
    @IBAction func btn_back(_ sender:UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btn_markTaskComplete(_ sender:UIButton) {
        
            let index = selectedIndex.item
        if index < 8 {
            let new_ind = IndexPath(item: index+1, section: 0)
            self.collectionView(self.collect, didSelectItemAt: new_ind)
        }else{
            self.navigationController?.popViewController(animated: true)
            
                    let viewControllers: [UIViewController] = self.navigationController!.viewControllers
                    for root in viewControllers {
                        if root is ViewProgressC_VC {
                            self.navigationController!.popToViewController(root, animated: false)
                        }
                    }
        
        }
        
//        return

        
        collect.reloadData()
        print(index)
       
           
    }
}
extension ReturnToDuty1C_VC:UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == collect{
            return 9
        }else{
            return List.count
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == collect {
            let cell = collect.dequeueReusableCell(withReuseIdentifier: "ReturnToReadyCategoryC_Cell", for: indexPath) as! ReturnToReadyCategoryC_Cell
            
            cell.lbl_taskName.backgroundColor = #colorLiteral(red: 0.9097240567, green: 0.9098549485, blue: 0.9096954465, alpha: 1)
            cell.lbl_taskName.textColor = #colorLiteral(red: 0.501960814, green: 0.501960814, blue: 0.501960814, alpha: 1)
            cell.lbl_taskName.cornerRadius = Double(cell.lbl_taskName.frame.size.height/2)
            if self.selectedIndex == indexPath {
                cell.lbl_taskName.backgroundColor = #colorLiteral(red: 0.07450980392, green: 0.368627451, blue: 0.7176470588, alpha: 1)
                cell.lbl_taskName.textColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            }
            cell.lbl_taskName.text = "Task \(indexPath.item + 1)"
            return cell
        }else{
            let cell = collect1.dequeueReusableCell(withReuseIdentifier: "ReturnToDutyC_Cell", for: indexPath) as! ReturnToDutyC_Cell
            let dict = List[indexPath.row]
            cell.viewDriverUpdated.isHidden = true
            cell.viewSuggestedSource.isHidden = true
            cell.viewDropDown.isHidden = true
            cell.lbl_head1.text = dict.head1
            cell.lbl_head2.text = dict.head2
            cell.lbl_head3.text = dict.head3
//           if indexPath.row == 2 {
//                cell.viewSuggestedSource.isHidden = false
//            }else if indexPath.row == 3 || indexPath.row == 4 || indexPath.row == 5 || indexPath.row == 6{
//                cell.viewDriverUpdated.isHidden = false
//                cell.viewSuggestedSource.isHidden = false
//            }
            if cell.lbl_head1.text == "Self Study"{
                cell.viewSuggestedSource.isHidden = false
            }
            if cell.lbl_head1.text == "Outpatient Individual\nTherapy"{
                cell.viewSuggestedSource.isHidden = false
                cell.viewDriverUpdated.isHidden = false

            }
            if cell.lbl_head1.text == "Outpatient Group\nTherapy"{
                cell.viewSuggestedSource.isHidden = false
                cell.viewDriverUpdated.isHidden = false

            }
            if cell.lbl_head1.text == "Intensive Outpatient\nTreatment Program"{
                cell.viewSuggestedSource.isHidden = false
                cell.viewDriverUpdated.isHidden = false

            }

            if cell.lbl_head1.text == "Inpatient Treatment"{
                cell.viewSuggestedSource.isHidden = false
                cell.viewDriverUpdated.isHidden = false
            }
            if cell.lbl_head1.text == "Physician Physical"{
                cell.viewSuggestedSource.isHidden = true
                cell.viewDriverUpdated.isHidden = true
                cell.viewDropDown.isHidden = false
            }
            if cell.lbl_head1.text == "Additional Requirements"{
                cell.viewSuggestedSource.isHidden = false
                cell.viewDriverUpdated.isHidden = true
                cell.viewDropDown.isHidden = true
            }

            return cell
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == collect {
            selectedIndex = indexPath
            if indexPath.row == 0 {
                List.removeAll()
                List.append(ReturnToDutyModal(head1: "Drug Education", head2: "How to post an update:", head3: "Employee Updated"))
                collect1.reloadData()
                
            }else if indexPath.row == 1{
                
                List.removeAll()
                List.append(ReturnToDutyModal(head1: "Recovery/Support\nGroups", head2: "How to post an update:", head3: "Employee Updated"))
                updateHeight(Collect: collect1)
                collect1.reloadData()
                
            }else if indexPath.row == 2{
                List.removeAll()
                List.append(ReturnToDutyModal(head1: "Self Study", head2: "How to post an update:", head3: "Driver Updated"))
                updateHeight(Collect: collect1)
                collect1.reloadData()
                
            }else if indexPath.row == 3{
                List.removeAll()
                List.append(ReturnToDutyModal(head1: "Outpatient Individual\nTherapy", head2: "How to post an update:", head3: "Employee Updated"))
                updateHeight(Collect: collect1)
                collect1.reloadData()
                
            }else if indexPath.row == 4{
                List.removeAll()
                List.append(ReturnToDutyModal(head1: "Outpatient Group\nTherapy", head2: "How to post an update:", head3: "Employee Updated"))
                updateHeight(Collect: collect1)
                collect1.reloadData()
                
            }else if indexPath.row == 5{
                List.removeAll()
                List.append(ReturnToDutyModal(head1: "Intensive Outpatient\nTreatment Program", head2: "How to post an update:", head3: "Employee Updated"))
                collect1.reloadData()
                
            }else if indexPath.row == 6{
                
                List.removeAll()
                List.append(ReturnToDutyModal(head1: "Inpatient Treatment", head2: "How to post an update:", head3: "Employee Updated"))
                updateHeight(Collect: collect1)
                collect1.reloadData()
                
            }else if indexPath.row == 7{
                
                List.removeAll()
                List.append(ReturnToDutyModal(head1: "Physician Physical", head2: "How to post an update:", head3: "Employee Updated"))
                updateHeight(Collect: collect1)
                collect1.reloadData()
                
            }else if indexPath.row == 8{
                
                List.removeAll()
                List.append(ReturnToDutyModal(head1: "Additional Requirements", head2: "How to post an update:", head3: "Employee Updated"))
                updateHeight(Collect: collect1)
                collect1.reloadData()
                
            }
            collect.reloadData()
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView == collect {
            let w = collect.frame.size.width/4
            let h = collect.frame.size.height
            
            return CGSize(width: w, height: h)
            
        }else{
            let w = collect1.frame.size.width
            let h = collect1.frame.size.height
            return CGSize(width: w, height: h)
            
        }
    }
    
    func updateHeight(Collect:UICollectionView)  {
        let f = collectHeight
        collect1Height.constant = f
        self.collect1.isScrollEnabled = false
        DispatchQueue.main.async {
            let ff2 = self.collectHeight
            self.collect1Height.constant = ff2
        }
    }
}

struct ReturnToDutyModal {
    var head1:String?
    var head2:String?
    var head3:String?
}
