//
//  ViewProfileC_VC.swift
//  Civil psych
//
//  Created by Ankur  on 10/02/21.
//

import UIKit

class ViewProfileC_VC: UIViewController {
    
    @IBOutlet weak var lbl_Name: UILabel!
    @IBOutlet weak var lbl_Email: UILabel!
    @IBOutlet weak var lbl_Phone: UILabel!
    @IBOutlet weak var lbl_Dob: UILabel!
    @IBOutlet weak var lbl_PhysicalAddress: UILabel!
    @IBOutlet weak var lbl_DotSegment: UILabel!
    
    @IBOutlet weak var lbl_empDetails_Consortium: UILabel!
    @IBOutlet weak var lbl_empDetails_Address: UILabel!
    @IBOutlet weak var lbl_empDetails_State: UILabel!
    @IBOutlet weak var lbl_empDetails_City: UILabel!
    @IBOutlet weak var lbl_empDetails_HrNAme: UILabel!
    @IBOutlet weak var lbl_empDetails_EmployerPhone: UILabel!
    
    @IBOutlet weak var lbl_DLNumber: UILabel!
    @IBOutlet weak var lbl_LicenseExpiryDate: UILabel!
    
    @IBOutlet weak var lbl_emergencyRef_Name: UILabel!
    @IBOutlet weak var lbl_emergencyRef_Email: UILabel!
    @IBOutlet weak var lbl_emergencyRef_Phone: UILabel!
    @IBOutlet weak var lbl_emergencyRef_Address: UILabel!
    @IBOutlet weak var lbl_emergencyRef_Relationship: UILabel!
    @IBOutlet weak var lbl_emergencyRef_State: UILabel!
    @IBOutlet weak var lbl_emergencyRef_City: UILabel!

    @IBOutlet weak var lbl_currentDrugTest_Type: UILabel!
    @IBOutlet weak var lbl_currentDrugTest_dateOfFailedTest: UILabel!
    @IBOutlet weak var lbl_currentDrugTest_TestResult: UILabel!
    
    @IBOutlet weak var lbl_currentLab_MROName: UILabel!
    @IBOutlet weak var lbl_currentLab_Name: UILabel!
    @IBOutlet weak var lbl_currentLab_Address: UILabel!
    @IBOutlet weak var lbl_currentLab_Phone: UILabel!
    @IBOutlet weak var lbl_currentLab_TypeOfSample: UILabel!
    @IBOutlet weak var lbl_currentLab_LabResult: UILabel!
    
    @IBOutlet weak var lbl_previousDrugTest_SapProcess: UILabel!
    @IBOutlet weak var lbl_previousDrugTest_dateOfTest: UILabel!
    @IBOutlet weak var lbl_previousDrugTest_TestResult: UILabel!
    @IBOutlet weak var lbl_previousDrugTest_nameOfPreviousSap: UILabel!
    @IBOutlet weak var lbl_previousDrugTest_SapAddress: UILabel!
    @IBOutlet weak var lbl_previousDrugTest_SapPhone: UILabel!
    
    @IBOutlet weak var lbl_prevLab_MROName: UILabel!
    @IBOutlet weak var lbl_prevLab_Name: UILabel!
    @IBOutlet weak var lbl_prevLab_Address: UILabel!
    @IBOutlet weak var lbl_prevLab_Phone: UILabel!
    @IBOutlet weak var lbl_prevLab_TypeOfSample: UILabel!
    @IBOutlet weak var lbl_prevLab_Results: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        SetupData()
    }
    
    @IBAction func btn_CancelAssesment(_ sender:UIButton) {
        
        let vc = CancelAssessment_PopupVC.instantiate(fromAppStoryboard: .HomeC)
        vc.select = { [weak self] (str,status) in
            if status == 1 {
                self?.navigationController?.popViewController(animated: true)
            }
            
        }
        self.present(vc, animated: true, completion: nil)
        
    }
    
    @IBAction func btn_back(_ sender:UIButton) {
        self.navigationController?.popViewController(animated: true)
    }

}

extension ViewProfileC_VC{
    
    func SetupData(){
        LoadProFileData(heading: "Name - ", Text: "Daniel Jhonson", lbl: lbl_Name)
        LoadProFileData(heading: "Email - ", Text: "Daniel Jhon123@gmail.com", lbl: lbl_Email)
        LoadProFileData(heading: "Phone - ", Text: "(+1)818-3344799", lbl: lbl_Phone)
        LoadProFileData(heading: "Date of Birth - ", Text: "01/19/1988", lbl: lbl_Dob)
        LoadProFileData(heading: "Physical Address - ", Text: "Building no-56, Suit-4 2nd Apartment, Maine Street Road, J-32, 100021 US.", lbl: lbl_PhysicalAddress)
        LoadProFileData(heading: "D.O.T. Segment - ", Text: "FAA (Airline)", lbl: lbl_DotSegment)
        
        LoadProFileData(heading: "Employer/Consortium - ", Text: "LLC", lbl: lbl_empDetails_Consortium)
        LoadProFileData(heading: "Physical Address - ", Text: "Building no-56, Suit-4 2nd Apartment, Maine Street Road, J-32, 100021 US.", lbl: lbl_empDetails_Address)
        LoadProFileData(heading: "State - ", Text: "New York", lbl: lbl_empDetails_State)
        LoadProFileData(heading: "City - ", Text: "Albony", lbl: lbl_empDetails_City)
//        LoadProFileData(heading: "DER/HR Name - ", Text: "Name of the Designated Employer", lbl: lbl_empDetails_HrNAme)
        LoadProFileData(heading: "DER/HR Name - ", Text: "Name of Designated Employee Representative (Safety Person)", lbl: lbl_empDetails_HrNAme)
        LoadProFileData(heading: "Employer Phone Number - ", Text: "(+1)818-3344799", lbl: lbl_empDetails_EmployerPhone)
        
        LoadProFileData(heading: "Employee CDL Number - ", Text: "RJ13 20120123456", lbl: lbl_DLNumber)
        LoadProFileData(heading: "License Expiration Date - ", Text: "10/20/2021", lbl: lbl_LicenseExpiryDate)
        
        LoadProFileData(heading: "Name - ", Text: "Robert Doe", lbl: lbl_emergencyRef_Name)
        LoadProFileData(heading: "Email - ", Text: "Robertdoe23@gmail.com", lbl: lbl_emergencyRef_Email)
        LoadProFileData(heading: "Phone - ", Text: "(+1)818-3344799", lbl: lbl_emergencyRef_Phone)
        LoadProFileData(heading: "Physical Address - ", Text: "Building no-56, Suit-4 2nd Apartment, Maine Street Road, J-32, 100021 US.", lbl: lbl_emergencyRef_Address)
        LoadProFileData(heading: "State - ", Text: "New York", lbl: lbl_emergencyRef_State)
        LoadProFileData(heading: "City - ", Text: "Albony", lbl: lbl_emergencyRef_City)
        LoadProFileData(heading: "Relationship - ", Text: "Friend", lbl: lbl_emergencyRef_Relationship)
        
        LoadProFileData(heading: "Type of Drug Test - ", Text: "Pre-Employement", lbl: lbl_currentDrugTest_Type)
        LoadProFileData(heading: "Date of Failed Test - ", Text: "10/19/2020", lbl: lbl_currentDrugTest_dateOfFailedTest)
        LoadProFileData(heading: "Test Results - ", Text: "Alcohol", lbl: lbl_currentDrugTest_TestResult)
        
        LoadProFileData(heading: "Medical Review Officer - ", Text: "Theanswerhub", lbl: lbl_currentLab_MROName)
        LoadProFileData(heading: "Name of Lab - ", Text: "Theanswerhub", lbl: lbl_currentLab_Name)
        LoadProFileData(heading: "Lab Address - ", Text: "Building no-56, Suit-4 2nd Apartment, Maine Street Road, J-32, 100021 US.", lbl: lbl_currentLab_Address)
        LoadProFileData(heading: "Lab Phone - ", Text: "(+1)818-3344799", lbl: lbl_currentLab_Phone)
        LoadProFileData(heading: "Type of sample - ", Text: "Saliva", lbl: lbl_currentLab_TypeOfSample)
        LoadProFileData(heading: "Lab Results - ", Text: "Current drug test", lbl: lbl_currentLab_LabResult)
        
        LoadProFileData(heading: "Have you gone through the SAP process before? - ", Text: "Yes", lbl: lbl_previousDrugTest_SapProcess)
        LoadProFileData(heading: "Date of Test - ", Text: "10/19/2020", lbl: lbl_previousDrugTest_dateOfTest)
        LoadProFileData(heading: "Test Results - ", Text: "Alcohol", lbl: lbl_previousDrugTest_TestResult)
        LoadProFileData(heading: "Name of previous SAP - ", Text: "Robert", lbl: lbl_previousDrugTest_nameOfPreviousSap)
        LoadProFileData(heading: "SAP Address - ", Text: "Building no-56, Suit-4 2nd Apartment, Maine Street Road, J-32, 100021 US.", lbl: lbl_previousDrugTest_SapAddress)
        LoadProFileData(heading: "SAP Phone Number - ", Text: "Phone Number", lbl: lbl_previousDrugTest_SapPhone)
        
        LoadProFileData(heading: "Medical Review Officer - ", Text: "Theanswerhub", lbl: lbl_prevLab_MROName)
        LoadProFileData(heading: "Name of Lab - ", Text: "Theanswerhub", lbl: lbl_prevLab_Name)
        LoadProFileData(heading: "Lab Address - ", Text: "Building no-56, Suit-4 2nd Apartment, Maine Street Road, J-32, 100021 US.", lbl: lbl_prevLab_Address)
        LoadProFileData(heading: "Lab Phone - ", Text: "(+1)818-3344799", lbl: lbl_prevLab_Phone)
        LoadProFileData(heading: "Type of sample - ", Text: "Saliva", lbl: lbl_prevLab_TypeOfSample)
        LoadProFileData(heading: "Lab Results - ", Text: "Current drug test", lbl: lbl_prevLab_Results)
    }
    
    func LoadProFileData(heading:String, Text:String, lbl:UILabel){
        
        let head1 = customizeFont(string: "\(heading)", font: UIFont(name: AppFonts.Poppins_SemiBold.rawValue, size: 14.0)!, color: .black)
//        let txt1 = customizeFont(string: "\(Text)", font: UIFont(name: AppFonts.Poppins_Regular.rawValue, size: 14.0)!, color: .darkGray)
        let txt1 = customizeFont(string: "\(Text)", font: UIFont(name: AppFonts.Poppins_Regular.rawValue, size: 14.0)!, color: .black)
        head1.append(txt1)
        lbl.attributedText = head1
    }
}
