//
//  AddEmployerModal.swift
//  Civil psych
//
//  Created by Ankur  on 22/02/21.
//

import Foundation

struct AddEmployerModal {
    var employer_Name : String?
    var employer_Address : String?
    var employer_HrName : String?
    var employer_Phone : String?
}
