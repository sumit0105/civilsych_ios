//
//  ResetPassE_VC.swift
//  Civil psych
//
//  Created by Ankur  on 20/02/21.
//

import UIKit

class ResetPassE_VC: UIViewController {
    
    @IBOutlet weak var txt_pass: UITextField!
    @IBOutlet weak var txt_confirmPass: UITextField!
    @IBOutlet weak var img_pass: UIImageView!
    @IBOutlet weak var img_confirmPass: UIImageView!


    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        img_pass.isHidden = true
        img_confirmPass.isHidden = true
    }
    
    @IBAction func btn_Submit(_ sender:UIButton) {
        if self.txt_pass.text?.count == 0   {
            AlertControllerOnr(title: alertTitle.alert_alert, message: messageString.mPasswordValidation)
            return
        }
        
        if self.txt_confirmPass.text?.count == 0   {
            AlertControllerOnr(title: alertTitle.alert_alert, message: messageString.mConfirmPasswordValidation)
            return
        }
        
        if txt_pass.text != txt_confirmPass.text {
//            AlertControllerOnr(title: alertTitle.alert_alert, message: messageString.mPaaswordmatch)
            return
        }

        self.dismiss(animated: true, completion: {
//            self.select!("s",1)
        })
    }
    
    @IBAction func btn_Back(_ sender:UIButton) {
        self.dismiss(animated: true, completion: {
//            self.select!("s",0)
        })
}

}

extension ResetPassE_VC {
        
    @IBAction func text_pass(_ sender: UITextField) {
        if sender.text!.count > 0   {
            img_pass.isHidden = false
        }else{
            img_pass.isHidden = true
        }
    }
    
    @IBAction func text_ConfirmPass(_ sender: UITextField) {
        if sender.text!.count > 0   {
            img_confirmPass.isHidden = false
        }else{
            img_confirmPass.isHidden = true
        }
        if txt_pass.text != sender.text{
            img_confirmPass.image = #imageLiteral(resourceName: "red_cross")
        }else{
            img_confirmPass.image = #imageLiteral(resourceName: "checkdG")
        }
    }
}
