//
//  ForgotPassE_VC.swift
//  Civil psych
//
//  Created by Ankur  on 20/02/21.
//

import UIKit

class ForgotPassE_VC: UIViewController {
    
    @IBOutlet weak var txt_email: UITextField!

    
    typealias selectBtn = (String,Int) -> Void
    var select : selectBtn? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    @IBAction func btn_Next(_ sender:UIButton) {
        if self.txt_email.text?.count == 0   {
            AlertControllerOnr(title: alertTitle.alert_alert, message: messageString.mEmailRegistered)
            return
        }
        
        self.dismiss(animated: true, completion: {
            self.select!("s",1)
        })
    }
    
    @IBAction func btn_Back(_ sender:UIButton) {
        self.dismiss(animated: true, completion: {
            self.select!("s",0)
        })
    }
    
}
