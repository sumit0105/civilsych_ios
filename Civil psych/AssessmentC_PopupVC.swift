//
//  AssessmentC_PopupVC.swift
//  Civil psych
//
//  Created by Ankur  on 08/03/21.
//

import UIKit

class AssessmentC_PopupVC: UIViewController {
    
    typealias selectBtn = (String,Int) -> Void
    var select : selectBtn? = nil

    override func viewDidLoad() {
        super.viewDidLoad()

        
    }
    
    @IBAction func btn_yes(_ sender:UIButton) {
        self.dismiss(animated: true, completion: {
            self.select!("s",1)
        })
    }

    @IBAction func btn_no(_ sender:UIButton) {
        self.dismiss(animated: true, completion: {
            self.select!("s",0)
        })
    }
    
}
