//
//  CancelAssessment_PopupVC.swift
//  Civil psych
//
//  Created by Ankur  on 11/02/21.
//

import UIKit

class CancelAssessment_PopupVC: UIViewController {
    
    typealias selectBtn = (String,Int) -> Void
    var select : selectBtn? = nil

    override func viewDidLoad() {
        super.viewDidLoad()

        
    }
    
    @IBAction func btn_yes(_ sender:UIButton) {
        self.dismiss(animated: true, completion: {
            self.select!("s",1)
        })
    }

    @IBAction func btn_no(_ sender:UIButton) {
        self.dismiss(animated: true, completion: {
            self.select!("s",0)
        })
    }
    
    @IBAction func btn_Close(_ sender:UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    
}
