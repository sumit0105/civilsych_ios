//
//  CreateProfileE2_VC.swift
//  Civil psych
//
//  Created by Ankur  on 20/02/21.
//

import UIKit
import DropDown

class CreateProfileE2_VC: UIViewController {
    
    @IBOutlet weak var txt_drugTestType: UITextField!
    @IBOutlet weak var txt_sampleType: UITextField!
    @IBOutlet weak var txt_prevsampleType: UITextField!
    @IBOutlet weak var drugTestOtherView: UIView!
    @IBOutlet weak var currentTestResultView: UIView!
    @IBOutlet weak var prevTestResultView: UIView!
    @IBOutlet weak var txt_TestOtherView: UITextView!
    @IBOutlet weak var txt_currentTestResult: UITextView!
    @IBOutlet weak var txt_prevTestResult: UITextView!
    @IBOutlet weak var txt_currentDrug_DateOfTest: UITextField!
    @IBOutlet weak var txt_prevDrug_DateOfTest: UITextField!
    @IBOutlet weak var img_currentDrugTest:UIImageView!
    @IBOutlet weak var img_prevDrugTest:UIImageView!
    
    
    
    @IBOutlet weak var btn1:UIButton!
    @IBOutlet weak var btn2:UIButton!
    @IBOutlet weak var btn3:UIButton!
    @IBOutlet weak var btn4:UIButton!
    @IBOutlet weak var btn5:UIButton!
    @IBOutlet weak var btn6:UIButton!
    @IBOutlet weak var btn7:UIButton!
    
    @IBOutlet weak var btnP1:UIButton!
    @IBOutlet weak var btnP2:UIButton!
    @IBOutlet weak var btnP3:UIButton!
    @IBOutlet weak var btnP4:UIButton!
    @IBOutlet weak var btnP5:UIButton!
    @IBOutlet weak var btnP6:UIButton!
    @IBOutlet weak var btnP7:UIButton!
    
    @IBOutlet weak var lblYes:UILabel!
    @IBOutlet weak var lblNo:UILabel!
    
    let datePicker = UIDatePicker()
    let dateFormatter = DateFormatter()
    var toolBar = UIToolbar()
    let screenWidth = UIScreen.main.bounds.width
    
    var imagePicker1: ImagePicker!
    var imageaAtt : imageArray?
    var imgStatus = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        txt_TestOtherView.delegate = self
        txt_currentTestResult.delegate = self
        txt_prevTestResult.delegate = self
        DatePick()
        if #available(iOS 13.4, *) {
            datePicker.preferredDatePickerStyle = .wheels
        }
        imagePicker1 = ImagePicker(presentationController: self, delegate: self)
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        drugTestOtherView.isHidden = true
        currentTestResultView.isHidden = true
        prevTestResultView.isHidden = true
        lblNo.isHidden = true
    }
    
    //MARK: - DropDown's
    
    let drugTestTypeDrop = DropDown()
    let sampleTypeDrop = DropDown()
    let prevsampleTypeDrop = DropDown()
    
    lazy var dropDowns: [DropDown] = {
        return [
            self.drugTestTypeDrop,
            self.sampleTypeDrop,
            self.prevsampleTypeDrop
        ]
    }()
    
    @IBAction func btn_skip(_ sender:UIButton) {
        let vc = TabE_VC.instantiate(fromAppStoryboard: .TabE)
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func btn_uploadCurrentDrugTest(_ sender:UIButton) {
        imagePicker1.present(from: sender)
        imgStatus = 1
    }
    @IBAction func btn_uploadPrevDrugTest(_ sender:UIButton) {
        imagePicker1.present(from: sender)
        imgStatus = 2
    }
    
    @IBAction func btn_removeCurrentDrugTest(_ sender:UIButton) {
        img_currentDrugTest.image = nil
    }
    @IBAction func btn_removePrevDrugTest(_ sender:UIButton) {
        img_prevDrugTest.image = nil
    }

    
    @IBAction func btn_drugTestType(_ sender: UIButton){
        
        self.drugTestTypeDrop.anchorView = sender
        self.drugTestTypeDrop.bottomOffset = CGPoint(x: 0, y: sender.bounds.height + 0)
        self.drugTestTypeDrop.textColor = .black
        self.drugTestTypeDrop.separatorColor = .clear
        self.drugTestTypeDrop.selectionBackgroundColor = .clear
        self.drugTestTypeDrop.backgroundColor = #colorLiteral(red: 0.8541120291, green: 0.9235828519, blue: 0.9914466739, alpha: 1)
        self.drugTestTypeDrop.dataSource.removeAll()
        self.drugTestTypeDrop.cellHeight = 35
        self.drugTestTypeDrop.dataSource.append(contentsOf: ["Pre-Employment","For cause - Accident","For cause – Reasonable suspicion","Random","Follow up (SAP)","Others"])
        self.drugTestTypeDrop.selectionAction = { [unowned self] (index, item) in
            self.txt_drugTestType.text = item
            
            if index == 5 {
                self.drugTestOtherView.isHidden = false
            }else{
                self.drugTestOtherView.isHidden = true
            }
        }
        self.drugTestTypeDrop.show()
        
    }
    
    @IBAction func btn_SampleType(_ sender: UIButton){
        
        self.sampleTypeDrop.anchorView = sender
        self.sampleTypeDrop.bottomOffset = CGPoint(x: 0, y: sender.bounds.height + 0)
        self.sampleTypeDrop.textColor = .black
        self.sampleTypeDrop.separatorColor = .clear
        self.sampleTypeDrop.selectionBackgroundColor = .clear
        self.sampleTypeDrop.backgroundColor = #colorLiteral(red: 0.8541120291, green: 0.9235828519, blue: 0.9914466739, alpha: 1)
        self.sampleTypeDrop.dataSource.removeAll()
        self.sampleTypeDrop.cellHeight = 35
        self.sampleTypeDrop.dataSource.append(contentsOf: ["Urine","Breathalyzer","Hair Follicle","Blood","Saliva"])
        self.sampleTypeDrop.selectionAction = { [unowned self] (index, item) in
            self.txt_sampleType.text = item
        }
        self.sampleTypeDrop.show()
        
    }
    
    @IBAction func btn_PrevSampleType(_ sender: UIButton){
        
        self.prevsampleTypeDrop.anchorView = sender
        self.prevsampleTypeDrop.bottomOffset = CGPoint(x: 0, y: sender.bounds.height + 0)
        self.prevsampleTypeDrop.textColor = .black
        self.prevsampleTypeDrop.separatorColor = .clear
        self.prevsampleTypeDrop.selectionBackgroundColor = .clear
        self.prevsampleTypeDrop.backgroundColor = #colorLiteral(red: 0.8541120291, green: 0.9235828519, blue: 0.9914466739, alpha: 1)
        self.prevsampleTypeDrop.dataSource.removeAll()
        self.prevsampleTypeDrop.cellHeight = 35
        self.prevsampleTypeDrop.dataSource.append(contentsOf: ["Urine","Breathalyzer","Hair Follicle","Blood","Saliva"])
        self.prevsampleTypeDrop.selectionAction = { [unowned self] (index, item) in
            self.txt_prevsampleType.text = item
        }
        self.prevsampleTypeDrop.show()
    }
    
    var p = 0
    
    @IBAction func btn_testResult(_ sender:UIButton){
        sender.setImage(#imageLiteral(resourceName: "uncheck"), for: .normal)
        
        if sender.tag == 0 {
            btn1.setImage(#imageLiteral(resourceName: "checked"), for: .normal)
            
//            btn2.setImage(#imageLiteral(resourceName: "uncheck"), for: .normal)
//            btn3.setImage(#imageLiteral(resourceName: "uncheck"), for: .normal)
//            btn4.setImage(#imageLiteral(resourceName: "uncheck"), for: .normal)
//            btn5.setImage(#imageLiteral(resourceName: "uncheck"), for: .normal)
//            btn6.setImage(#imageLiteral(resourceName: "uncheck"), for: .normal)
//            btn7.setImage(#imageLiteral(resourceName: "uncheck"), for: .normal)
            currentTestResultView.isHidden = true
            p = 1
        }else if sender.tag == 1{
            btn2.setImage(#imageLiteral(resourceName: "checked"), for: .normal)
//            btn1.setImage(#imageLiteral(resourceName: "uncheck"), for: .normal)
//            btn3.setImage(#imageLiteral(resourceName: "uncheck"), for: .normal)
//            btn4.setImage(#imageLiteral(resourceName: "uncheck"), for: .normal)
//            btn5.setImage(#imageLiteral(resourceName: "uncheck"), for: .normal)
//            btn6.setImage(#imageLiteral(resourceName: "uncheck"), for: .normal)
//            btn7.setImage(#imageLiteral(resourceName: "uncheck"), for: .normal)
            currentTestResultView.isHidden = true
            p = 2
        }else if sender.tag == 2{
            btn3.setImage(#imageLiteral(resourceName: "checked"), for: .normal)
//            btn2.setImage(#imageLiteral(resourceName: "uncheck"), for: .normal)
//            btn1.setImage(#imageLiteral(resourceName: "uncheck"), for: .normal)
//            btn4.setImage(#imageLiteral(resourceName: "uncheck"), for: .normal)
//            btn5.setImage(#imageLiteral(resourceName: "uncheck"), for: .normal)
//            btn6.setImage(#imageLiteral(resourceName: "uncheck"), for: .normal)
//            btn7.setImage(#imageLiteral(resourceName: "uncheck"), for: .normal)
            currentTestResultView.isHidden = true
            p = 3
        }else if sender.tag == 3{
            btn4.setImage(#imageLiteral(resourceName: "checked"), for: .normal)
//            btn2.setImage(#imageLiteral(resourceName: "uncheck"), for: .normal)
//            btn3.setImage(#imageLiteral(resourceName: "uncheck"), for: .normal)
//            btn1.setImage(#imageLiteral(resourceName: "uncheck"), for: .normal)
//            btn5.setImage(#imageLiteral(resourceName: "uncheck"), for: .normal)
//            btn6.setImage(#imageLiteral(resourceName: "uncheck"), for: .normal)
//            btn7.setImage(#imageLiteral(resourceName: "uncheck"), for: .normal)
            currentTestResultView.isHidden = true
            p = 4
        }else if sender.tag == 4{
            btn5.setImage(#imageLiteral(resourceName: "checked"), for: .normal)
//            btn2.setImage(#imageLiteral(resourceName: "uncheck"), for: .normal)
//            btn3.setImage(#imageLiteral(resourceName: "uncheck"), for: .normal)
//            btn4.setImage(#imageLiteral(resourceName: "uncheck"), for: .normal)
//            btn1.setImage(#imageLiteral(resourceName: "uncheck"), for: .normal)
//            btn6.setImage(#imageLiteral(resourceName: "uncheck"), for: .normal)
//            btn7.setImage(#imageLiteral(resourceName: "uncheck"), for: .normal)
            currentTestResultView.isHidden = true
            p = 5
        }else if sender.tag == 5{
            btn6.setImage(#imageLiteral(resourceName: "checked"), for: .normal)
//            btn2.setImage(#imageLiteral(resourceName: "uncheck"), for: .normal)
//            btn3.setImage(#imageLiteral(resourceName: "uncheck"), for: .normal)
//            btn4.setImage(#imageLiteral(resourceName: "uncheck"), for: .normal)
//            btn1.setImage(#imageLiteral(resourceName: "uncheck"), for: .normal)
//            btn5.setImage(#imageLiteral(resourceName: "uncheck"), for: .normal)
//            btn7.setImage(#imageLiteral(resourceName: "uncheck"), for: .normal)
            currentTestResultView.isHidden = false
            p = 6
        }else{
            btn7.setImage(#imageLiteral(resourceName: "checked"), for: .normal)
//            btn2.setImage(#imageLiteral(resourceName: "uncheck"), for: .normal)
//            btn3.setImage(#imageLiteral(resourceName: "uncheck"), for: .normal)
//            btn4.setImage(#imageLiteral(resourceName: "uncheck"), for: .normal)
//            btn1.setImage(#imageLiteral(resourceName: "uncheck"), for: .normal)
//            btn5.setImage(#imageLiteral(resourceName: "uncheck"), for: .normal)
//            btn6.setImage(#imageLiteral(resourceName: "uncheck"), for: .normal)
            currentTestResultView.isHidden = false
            p = 7
        }
    }
    
    var i = 0
    
    @IBAction func btn_previoustestResult(_ sender:UIButton){
        sender.setImage(#imageLiteral(resourceName: "uncheck"), for: .normal)
        
        if sender.tag == 0 {
            btnP1.setImage(#imageLiteral(resourceName: "checked"), for: .normal)
            btnP2.setImage(#imageLiteral(resourceName: "uncheck"), for: .normal)
            btnP3.setImage(#imageLiteral(resourceName: "uncheck"), for: .normal)
            btnP4.setImage(#imageLiteral(resourceName: "uncheck"), for: .normal)
            btnP5.setImage(#imageLiteral(resourceName: "uncheck"), for: .normal)
            btnP6.setImage(#imageLiteral(resourceName: "uncheck"), for: .normal)
            btnP7.setImage(#imageLiteral(resourceName: "uncheck"), for: .normal)
            prevTestResultView.isHidden = true
            i = 1
        }else if sender.tag == 1{
            btnP2.setImage(#imageLiteral(resourceName: "checked"), for: .normal)
            btnP1.setImage(#imageLiteral(resourceName: "uncheck"), for: .normal)
            btnP3.setImage(#imageLiteral(resourceName: "uncheck"), for: .normal)
            btnP4.setImage(#imageLiteral(resourceName: "uncheck"), for: .normal)
            btnP5.setImage(#imageLiteral(resourceName: "uncheck"), for: .normal)
            btnP6.setImage(#imageLiteral(resourceName: "uncheck"), for: .normal)
            btnP7.setImage(#imageLiteral(resourceName: "uncheck"), for: .normal)
            prevTestResultView.isHidden = true
            i = 2
        }else if sender.tag == 2{
            btnP3.setImage(#imageLiteral(resourceName: "checked"), for: .normal)
            btnP2.setImage(#imageLiteral(resourceName: "uncheck"), for: .normal)
            btnP1.setImage(#imageLiteral(resourceName: "uncheck"), for: .normal)
            btnP4.setImage(#imageLiteral(resourceName: "uncheck"), for: .normal)
            btnP5.setImage(#imageLiteral(resourceName: "uncheck"), for: .normal)
            btnP6.setImage(#imageLiteral(resourceName: "uncheck"), for: .normal)
            btnP7.setImage(#imageLiteral(resourceName: "uncheck"), for: .normal)
            prevTestResultView.isHidden = true
            i = 3
        }else if sender.tag == 3{
            btnP4.setImage(#imageLiteral(resourceName: "checked"), for: .normal)
            btnP2.setImage(#imageLiteral(resourceName: "uncheck"), for: .normal)
            btnP1.setImage(#imageLiteral(resourceName: "uncheck"), for: .normal)
            btnP3.setImage(#imageLiteral(resourceName: "uncheck"), for: .normal)
            btnP5.setImage(#imageLiteral(resourceName: "uncheck"), for: .normal)
            btnP6.setImage(#imageLiteral(resourceName: "uncheck"), for: .normal)
            btnP7.setImage(#imageLiteral(resourceName: "uncheck"), for: .normal)
            prevTestResultView.isHidden = true
            i = 4
        }else if sender.tag == 4{
            btnP5.setImage(#imageLiteral(resourceName: "checked"), for: .normal)
            btnP2.setImage(#imageLiteral(resourceName: "uncheck"), for: .normal)
            btnP1.setImage(#imageLiteral(resourceName: "uncheck"), for: .normal)
            btnP4.setImage(#imageLiteral(resourceName: "uncheck"), for: .normal)
            btnP3.setImage(#imageLiteral(resourceName: "uncheck"), for: .normal)
            btnP6.setImage(#imageLiteral(resourceName: "uncheck"), for: .normal)
            btnP7.setImage(#imageLiteral(resourceName: "uncheck"), for: .normal)
            prevTestResultView.isHidden = true
            i = 5
        }else if sender.tag == 5{
            btnP6.setImage(#imageLiteral(resourceName: "checked"), for: .normal)
            btnP2.setImage(#imageLiteral(resourceName: "uncheck"), for: .normal)
            btnP1.setImage(#imageLiteral(resourceName: "uncheck"), for: .normal)
            btnP4.setImage(#imageLiteral(resourceName: "uncheck"), for: .normal)
            btnP3.setImage(#imageLiteral(resourceName: "uncheck"), for: .normal)
            btnP5.setImage(#imageLiteral(resourceName: "uncheck"), for: .normal)
            btnP7.setImage(#imageLiteral(resourceName: "uncheck"), for: .normal)
            prevTestResultView.isHidden = false
            i = 6
        }else{
            btnP7.setImage(#imageLiteral(resourceName: "checked"), for: .normal)
            btnP2.setImage(#imageLiteral(resourceName: "uncheck"), for: .normal)
            btnP1.setImage(#imageLiteral(resourceName: "uncheck"), for: .normal)
            btnP4.setImage(#imageLiteral(resourceName: "uncheck"), for: .normal)
            btnP3.setImage(#imageLiteral(resourceName: "uncheck"), for: .normal)
            btnP6.setImage(#imageLiteral(resourceName: "uncheck"), for: .normal)
            btnP5.setImage(#imageLiteral(resourceName: "uncheck"), for: .normal)
            prevTestResultView.isHidden = false
            i = 7
        }
    }
    
    @IBAction func btn_Back(_ sender:UIButton) {
        
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func btn_Next(_ sender:UIButton) {
        let vc = ConsentFormE_VC.instantiate(fromAppStoryboard: .LoginEmployee)
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    var b = 0
    
    @IBAction func btn_sapProcess(_ sender:UIButton){
        if sender.tag == 0 {
            lblYes.isHidden = false
            lblNo.isHidden = true
            b = 1
        }else{
            lblNo.isHidden = false
            lblYes.isHidden = true
            b = 2
        }
    }
    
    @IBAction func btn_infoCurrentLab_MRO(_ sender:UIButton) {
        let vc = PopupVC.instantiate(fromAppStoryboard: .LoginEmployee)
        vc.type = .Medical_ReviewOfficer
        self.present(vc, animated: true, completion: nil)
    }
    @IBAction func btn_infoCurrentLab_LabName(_ sender:UIButton) {
        let vc = PopupVC.instantiate(fromAppStoryboard: .LoginEmployee)
        vc.type = .Current_Lab_Name
        self.present(vc, animated: true, completion: nil)
    }
    
    @IBAction func btn_infoPrevDrug_Tes(_ sender:UIButton) {
        let vc = PopupVC.instantiate(fromAppStoryboard: .LoginEmployee)
        vc.type = .Enter_previous_failed_drug_test
        self.present(vc, animated: true, completion: nil)
    }
    
    @IBAction func btn_infoPrevDrug_Abuse(_ sender:UIButton) {
        let vc = PopupVC.instantiate(fromAppStoryboard: .LoginEmployee)
        vc.type = .previous_Substance_Abuse
        self.present(vc, animated: true, completion: nil)
    }

    
    @IBAction func btn_infoPrevLab_MRO(_ sender:UIButton) {
        let vc = PopupVC.instantiate(fromAppStoryboard: .LoginEmployee)
        vc.type = .Medical_ReviewOfficer
        self.present(vc, animated: true, completion: nil)
    }
    @IBAction func btn_infoPrevLab_LabName(_ sender:UIButton) {
        let vc = PopupVC.instantiate(fromAppStoryboard: .LoginEmployee)
        vc.type = .Current_Lab_Name
        self.present(vc, animated: true, completion: nil)    }
}

extension CreateProfileE2_VC : UITextViewDelegate{
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if txt_TestOtherView.text == "Type here.." || txt_prevTestResult.text == "Type here.." || txt_currentTestResult.text == "Type here.." {
            
            txt_TestOtherView.text = ""
            txt_prevTestResult.text = ""
            txt_currentTestResult.text = ""
            //     txt_chat.textColor = .white
        }
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if text == "\n" {
            txt_TestOtherView.resignFirstResponder()
            txt_prevTestResult.resignFirstResponder()
            txt_currentTestResult.resignFirstResponder()
        }
        return true
    }
    //    func textViewShouldBeginEditing(_ textView: UITextView) -> Bool {
    //
    //        return true
    //    }
    func textViewDidEndEditing(_ textView: UITextView) {
        if txt_TestOtherView.text == "" || txt_currentTestResult.text == "" || txt_prevTestResult.text == ""{
            txt_TestOtherView.text = "Type here.."
            txt_prevTestResult.text = "Type here.."
            txt_currentTestResult.text = "Type here.."
            //   textView.textColor = .lightGray
        }
    }
}

extension CreateProfileE2_VC: UITextFieldDelegate {
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == txt_currentDrug_DateOfTest {
            datePicker.datePickerMode = .date
        }else if textField == txt_prevDrug_DateOfTest {
            datePicker.datePickerMode = .date
        }
    }
    
    @objc func doneButtonTapped() {
        if txt_currentDrug_DateOfTest.isFirstResponder {
            dateFormatter.dateStyle = .medium
            dateFormatter.timeStyle = .none
            dateFormatter.dateFormat = "MM/dd/yyyy"
//            dateFormatter.dateFormat = "yyyy-MM-dd"
            txt_currentDrug_DateOfTest.text = dateFormatter.string(from: datePicker.date)
        }else if txt_prevDrug_DateOfTest.isFirstResponder {
//            let date: Date? = dateFormatterGet.date(from: "01/01/2021")
            dateFormatter.dateStyle = .medium
            dateFormatter.timeStyle = .none
                        dateFormatter.dateFormat = "MM/dd/yyyy"
//            dateFormatter.dateFormat = "yyyy-MM-dd"
            txt_prevDrug_DateOfTest.text = dateFormatter.string(from: datePicker.date)
//            txt_prevDrug_DateOfTest.text = dateFormatter.string(from: date!)
            
        }
        self.view.endEditing(true)
    }
    
    func DatePick(){
        
        toolBar = UIToolbar(frame: CGRect(x: 0.0, y: 0.0, width: screenWidth, height: 44.0)) //4
        
        toolBar.sizeToFit()
        //        toolBar.backgroundColor = .white
        toolBar.tintColor = .systemBlue
        toolBar.isUserInteractionEnabled = true
        let doneButton = UIBarButtonItem(title: "Done", style: .done, target: self, action: #selector(doneButtonTapped))
        doneButton.tintColor = .black
        toolBar.setItems([doneButton], animated: false)
        txt_currentDrug_DateOfTest.inputView = datePicker
        txt_prevDrug_DateOfTest.inputView = datePicker
        txt_currentDrug_DateOfTest.inputAccessoryView   = toolBar
        txt_prevDrug_DateOfTest.inputAccessoryView   = toolBar
        txt_currentDrug_DateOfTest.delegate = self
        txt_prevDrug_DateOfTest.delegate = self
        datePicker.minimumDate = Date()
    }
}

extension CreateProfileE2_VC : ImagePickerDelegate {
    func didSelect(image: UIImage?) {
        guard let img = image else {
            return
        }
        //        self.imageaAtt.append(imageArray(image: img, data: nil, url: nil, image_id: nil))
        if imgStatus == 1 {
            self.img_currentDrugTest.image = image
        }else{
            self.img_prevDrugTest.image = image
        }
        
    }
}
