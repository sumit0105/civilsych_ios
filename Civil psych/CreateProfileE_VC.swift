//
//  CreateProfileE_VC.swift
//  Civil psych
//
//  Created by Ankur  on 20/02/21.
//

import UIKit

class CreateProfileE_VC: UIViewController {
    
    @IBOutlet weak var txt_name: UITextField!
    @IBOutlet weak var txt_email: UITextField!
    @IBOutlet weak var txt_phone: UITextField!
    @IBOutlet weak var imgProfile:UIImageView!
    @IBOutlet weak var imgLicense:UIImageView!
    @IBOutlet weak var btn1:UIButton!
    @IBOutlet weak var btn2:UIButton!
    @IBOutlet weak var btn3:UIButton!
    @IBOutlet weak var btn4:UIButton!
    @IBOutlet weak var btn5:UIButton!
    
    @IBOutlet weak var tableEmp:UITableView!
    @IBOutlet weak var tableRef:UITableView!
    @IBOutlet weak var scroll: UIScrollView!
    @IBOutlet weak var tablehEmp : NSLayoutConstraint!
    @IBOutlet weak var tablehRef: NSLayoutConstraint!
    @IBOutlet weak var txt_dob: UITextField!
    @IBOutlet weak var txt_licenseExpDate: UITextField!
    @IBOutlet weak var txt_Address: UITextView!

    var imagePicker1: ImagePicker!
    var imageaAtt : imageArray?
    var imgStatus = 0
    var arrRef = [1]
    var arrEmp = [1]
    var signupData:SignupE_Modal = SignupE_Modal(fullName: "", email: "", phone: "")
    
    let datePicker = UIDatePicker()
    let dateFormatter = DateFormatter()
    var toolBar = UIToolbar()
    let screenWidth = UIScreen.main.bounds.width
    
    override func viewDidLoad() {
        super.viewDidLoad()
        SetSignupData()
        print(SetSignupData())
        imagePicker1 = ImagePicker(presentationController: self, delegate: self)
        setup()
        DatePick()
        if #available(iOS 13.4, *) {
            datePicker.preferredDatePickerStyle = .wheels
        }
    }
    
//    override func viewWillAppear(_ animated: Bool) {
//        super.viewWillAppear(animated)
//        viewQuesMark.isHidden = true
//    }
    
    func setup(){
        tableRef.register(UINib(nibName: "ReferencesCell", bundle: nil), forCellReuseIdentifier: "ReferencesCell")
        tableEmp.register(UINib(nibName: "AddEmployerCell", bundle: nil), forCellReuseIdentifier: "AddEmployerCell")
        tableEmp.delegate = self
        tableEmp.dataSource = self
        tableRef.delegate = self
        tableRef.dataSource = self
        txt_Address.delegate = self
        txt_phone.delegate = self
        DispatchQueue.main.async {
            self.tableEmp.reloadData()
            self.tableRef.reloadData()
        }

    }

    
    @IBAction func btn_camera(_ sender:UIButton) {
        
        imagePicker1.present(from: sender)
        imgStatus = 1
    }
    
    @IBAction func btn_uploadLicence(_ sender:UIButton) {
        imagePicker1.present(from: sender)
        imgStatus = 2
    }
    
    @IBAction func btn_Next(_ sender:UIButton) {
        let vc = CreateProfileE2_VC.instantiate(fromAppStoryboard: .LoginEmployee)
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    

    @IBAction func btn_skip(_ sender:UIButton) {
        let vc = TabE_VC.instantiate(fromAppStoryboard: .TabE)
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func btn_AddEmployer(_ sender:UIButton) {
        self.arrEmp.append(2)
        self.tableEmp.reloadData()
    }
    
    @IBAction func btn_AddReferences(_ sender:UIButton) {
        self.arrRef.append(2)
        self.tableRef.reloadData()
    }
    
    @IBAction func btn_infoDer(_ sender:UIButton) {
        let vc = PopupVC.instantiate(fromAppStoryboard: .LoginEmployee)
        vc.type = .DER_HR_Name
        self.present(vc, animated: true, completion: nil)
    }
    
    @IBAction func btn_infoEmgergency(_ sender:UIButton) {
        let vc = PopupVC.instantiate(fromAppStoryboard: .LoginEmployee)
        vc.type = .Emergency_Contact
        self.present(vc, animated: true, completion: nil)
    }

    
    var p = 0
    @IBAction func btn_paymentOffer(_ sender:UIButton){
        if sender.tag == 0 {
            btn1.setImage(#imageLiteral(resourceName: "btn_RoundFill"), for: .normal)
            btn2.setImage(#imageLiteral(resourceName: "btn_RoundBlank"), for: .normal)
            p = 1
        }else{
            btn2.setImage(#imageLiteral(resourceName: "btn_RoundFill"), for: .normal)
            btn1.setImage(#imageLiteral(resourceName: "btn_RoundBlank"), for: .normal)
            p = 2
        }
    }
    
    @IBAction func btn_DOT_Segment(_ sender:UIButton){
        sender.setImage(#imageLiteral(resourceName: "uncheck"), for: .normal)
        if sender.tag == 0 {
            btn1.setImage(#imageLiteral(resourceName: "checked"), for: .normal)
            btn2.setImage(#imageLiteral(resourceName: "uncheck"), for: .normal)
            btn3.setImage(#imageLiteral(resourceName: "uncheck"), for: .normal)
            btn4.setImage(#imageLiteral(resourceName: "uncheck"), for: .normal)
            btn5.setImage(#imageLiteral(resourceName: "uncheck"), for: .normal)
            sender.tag = 1
        }else if sender.tag == 1{
            btn2.setImage(#imageLiteral(resourceName: "checked"), for: .normal)
            btn1.setImage(#imageLiteral(resourceName: "uncheck"), for: .normal)
            btn3.setImage(#imageLiteral(resourceName: "uncheck"), for: .normal)
            btn4.setImage(#imageLiteral(resourceName: "uncheck"), for: .normal)
            btn5.setImage(#imageLiteral(resourceName: "uncheck"), for: .normal)
            sender.tag = 2
        }else if sender.tag == 2{
            btn3.setImage(#imageLiteral(resourceName: "checked"), for: .normal)
            btn2.setImage(#imageLiteral(resourceName: "uncheck"), for: .normal)
            btn1.setImage(#imageLiteral(resourceName: "uncheck"), for: .normal)
            btn4.setImage(#imageLiteral(resourceName: "uncheck"), for: .normal)
            btn5.setImage(#imageLiteral(resourceName: "uncheck"), for: .normal)
            sender.tag = 3
        }else if sender.tag == 3{
            btn4.setImage(#imageLiteral(resourceName: "checked"), for: .normal)
            btn2.setImage(#imageLiteral(resourceName: "uncheck"), for: .normal)
            btn3.setImage(#imageLiteral(resourceName: "uncheck"), for: .normal)
            btn1.setImage(#imageLiteral(resourceName: "uncheck"), for: .normal)
            btn5.setImage(#imageLiteral(resourceName: "uncheck"), for: .normal)
            sender.tag = 4
        }else{
            btn5.setImage(#imageLiteral(resourceName: "checked"), for: .normal)
            btn2.setImage(#imageLiteral(resourceName: "uncheck"), for: .normal)
            btn3.setImage(#imageLiteral(resourceName: "uncheck"), for: .normal)
            btn4.setImage(#imageLiteral(resourceName: "uncheck"), for: .normal)
            btn1.setImage(#imageLiteral(resourceName: "uncheck"), for: .normal)
            sender.tag = 5
        }
    }
}




extension CreateProfileE_VC : ImagePickerDelegate {
    func didSelect(image: UIImage?) {
        guard let img = image else {
            return
        }
        //        self.imageaAtt.append(imageArray(image: img, data: nil, url: nil, image_id: nil))
        if imgStatus == 1 {
            self.imgProfile.image = image
        }else {
            self.imgLicense.image = image
        }
        
    }
    
    
}


extension CreateProfileE_VC:UITableViewDelegate,UITableViewDataSource{
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == tableEmp {
            return arrEmp.count
        }else{
            return arrRef.count
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == tableEmp {
            let cell = tableEmp.dequeueReusableCell(withIdentifier: "AddEmployerCell") as! AddEmployerCell
            updateHight(table: tableEmp)
            cell.viewQuesMark.isHidden = true
            cell.btn_QuestionMark.tag = indexPath.row
            cell.btn_infoDer.tag = indexPath.row
            cell.btn_QuestionMark.addTarget(self, action: #selector(btn_QuestionMark(_:)), for: .touchUpInside)
            cell.btn_infoDer.addTarget(self, action: #selector(btn_infoDer(_:)), for: .touchUpInside)
            return cell
        }else{
            let cell = tableRef.dequeueReusableCell(withIdentifier: "ReferencesCell") as! ReferencesCell
            updateHight(table: tableRef)
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if tableView == tableEmp {
//            return 404
            return 641
        }else{
           return 481
        }
    }
    
    @IBAction func btn_QuestionMark(_ sender:UIButton) {
        let cell =  tableEmp.cellForRow(at: IndexPath(row: 0, section: 0)) as! AddEmployerCell
        cell.viewQuesMark.isHidden = !cell.viewQuesMark.isHidden
       
    }
    
    
    func updateHight(table:UITableView) {
        if table == tableEmp {
//            let hight = CGFloat(arrEmp.count) * CGFloat(404.0)
            let hight = CGFloat(arrEmp.count) * CGFloat(641.0)
            self.tablehEmp.constant = CGFloat(hight)
            self.scroll.contentSize.height = CGFloat(hight + 2000 + self.tablehRef.constant)
            print("Scroll height - \(CGFloat(hight + 2000 + self.tablehRef.constant))")
            self.tableEmp.isScrollEnabled = false
        }else{
            let hight = CGFloat(arrRef.count) * CGFloat(482.0)
            self.tablehRef.constant = CGFloat(hight)
            self.scroll.contentSize.height = CGFloat(hight + 2402)
            print("Scroll height - \(CGFloat(hight + 2402))")
            self.tableRef.isScrollEnabled = false
        }
    }
}

extension CreateProfileE_VC {
    func SetSignupData(){
        txt_name.text! = signupData.fullName
        txt_phone.text! = signupData.phone
        txt_email.text! = signupData.email
    }
}

extension CreateProfileE_VC: UITextFieldDelegate {
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == txt_dob {
            datePicker.datePickerMode = .date
        }else if textField == txt_licenseExpDate {
            datePicker.datePickerMode = .date
        }
    }
    
    @objc func doneButtonTapped() {
        if txt_dob.isFirstResponder {
            dateFormatter.dateStyle = .medium
            dateFormatter.timeStyle = .none
            dateFormatter.dateFormat = "MM/dd/yyyy"
//            dateFormatter.dateFormat = "yyyy-MM-dd"
            txt_dob.text = dateFormatter.string(from: datePicker.date)
        }else if txt_licenseExpDate.isFirstResponder {
            dateFormatter.dateStyle = .medium
            dateFormatter.timeStyle = .none
            dateFormatter.dateFormat = "MM/dd/yyyy"
//            dateFormatter.dateFormat = "yyyy-MM-dd"
            txt_licenseExpDate.text = dateFormatter.string(from: datePicker.date)
        }
        self.view.endEditing(true)
    }
    func DatePick(){
        
        toolBar = UIToolbar(frame: CGRect(x: 0.0, y: 0.0, width: screenWidth, height: 44.0)) //4
        
        toolBar.sizeToFit()
        //        toolBar.backgroundColor = .white
        toolBar.tintColor = .systemBlue
        toolBar.isUserInteractionEnabled = true
        let doneButton = UIBarButtonItem(title: "Done", style: .done, target: self, action: #selector(doneButtonTapped))
        doneButton.tintColor = .black
        toolBar.setItems([doneButton], animated: false)
        txt_dob.inputView = datePicker
        txt_dob.inputAccessoryView   = toolBar
        txt_dob.delegate = self
        
        txt_licenseExpDate.inputView = datePicker
        txt_licenseExpDate.inputAccessoryView   = toolBar
        txt_licenseExpDate.delegate = self
    
//        datePicker.minimumDate = Date()
       
        datePicker.minimumDate = Date(timeIntervalSince1970: 86400)
    }
}

extension CreateProfileE_VC : UITextViewDelegate{
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if txt_Address.text == "Enter Address" {
            txt_Address.text = ""
        }
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if text == "\n" {
            txt_Address.resignFirstResponder()
        }
        return true
    }

    func textViewDidEndEditing(_ textView: UITextView) {
        if txt_Address.text == ""{
            txt_Address.text = "Enter Address"
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == txt_phone {
            guard let text = textField.text else { return false }
            let newString = (text as NSString).replacingCharacters(in: range, with: string)
//            textField.text = format(with: "(XXX) XXX-XXXX", phone: newString)
            textField.text = format(with: "(+X)XXX-XXXXXXX", phone: newString)
            return false
        }
        return true
    }
}
