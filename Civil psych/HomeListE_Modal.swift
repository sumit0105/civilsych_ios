//
//  HomeListE_Modal.swift
//  Civil psych
//
//  Created by Ankur  on 03/03/21.
//

import Foundation
import UIKit

struct HomeListE_Modal{
    var clinician_Name : String?
    var distance : String?
    var price : String?
    var paymentMethod : String?
    var imgUser : UIImage?
    var isSelected:Bool = false
}
