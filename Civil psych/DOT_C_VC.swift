//
//  DOT_C_VC.swift
//  Civil psych
//
//  Created by Ankur  on 12/02/21.
//

import UIKit
import LUExpandableTableView

class DOT_C_VC: UIViewController {
    
    @IBOutlet weak var table:UITableView!
    
    @IBOutlet weak var expandableTableView : LUExpandableTableView!
    private let cellReuseIdentifier = "DotC_expandView1"
    private let cellReuseIdentifier2 = "DotC_expandView2"
    private let sectionHeaderReuseIdentifier = "DotC_HeaderCell"
    var headerList : [ViewProgressC_Modal] = []
    var arr = [1]
    var selected : NSMutableArray = []
    var selected2 : NSMutableArray = []
    
    var arrStatus = [""]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //        table.register(UINib(nibName: "DotC_Cell", bundle: nil), forCellReuseIdentifier: "DotC_Cell")
        //        table.delegate = self
        //        table.dataSource = self
        
        Setup()
    }
    
    func Setup(){
        headerList.append(ViewProgressC_Modal(options: "D.O.T. Clearinghouse Registration", status: "Completed"))
        
        headerList.append(ViewProgressC_Modal(options: "SAP requested", status: "Pending"))
        
        headerList.append(ViewProgressC_Modal(options: "SAP Approval", status: "Pending"))
        
        headerList.append(ViewProgressC_Modal(options: "Initial Assessment Date", status: "Pending"))
        
        headerList.append(ViewProgressC_Modal(options: "Follow Assessment Date", status: "Pending"))
        
        
        expandableTableView.register(UINib(nibName: cellReuseIdentifier, bundle: Bundle.main), forCellReuseIdentifier: cellReuseIdentifier)
        expandableTableView.register(UINib(nibName: cellReuseIdentifier2, bundle: Bundle.main), forCellReuseIdentifier: cellReuseIdentifier2)
        //        expandableTableView.register(UINib(nibName: "heightCell", bundle: Bundle.main), forCellReuseIdentifier: "heightCell")
        expandableTableView.register(UINib(nibName: "DotC_HeaderCell", bundle: Bundle.main), forHeaderFooterViewReuseIdentifier: sectionHeaderReuseIdentifier)
        
        expandableTableView.expandableTableViewDataSource = self
        expandableTableView.expandableTableViewDelegate = self
    }
    
    @IBAction func btn_Approval(_ sender: UIButton) {
        //        expandableTableView.collapseSections(at: [0,1,2])
        if sender.tag == 1 {
            let stat = ViewProgressC_Modal(options: "SAP requested", status: "Completed")
            headerList[sender.tag] = stat
           
        }else if sender.tag == 2 {
            let stat = ViewProgressC_Modal(options: "SAP Approval", status: "Completed")
            headerList[sender.tag] = stat
        }
        expandableTableView.collapseSections(at: [sender.tag])
        expandableTableView.reloadData()
        
    }
    
    @IBAction func btn_Submit(_ sender: UIButton) {
        //        expandableTableView.collapseSections(at: [3,4])
        if sender.tag == 3 {
            let stat = ViewProgressC_Modal(options: "Initial Assessment Date", status: "Completed")
            headerList[sender.tag] = stat
        }else if sender.tag == 4 {
            let stat = ViewProgressC_Modal(options: "Follow Assessment Date", status: "Completed")
            headerList[sender.tag] = stat
            self.navigationController?.popViewController(animated: true)
        }
        expandableTableView.collapseSections(at: [sender.tag])
        expandableTableView.reloadData()
        
    }
    
    @IBAction func btn_back(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
}

// MARK: - LUExpandableTableViewDataSource

extension DOT_C_VC: LUExpandableTableViewDataSource {
    func numberOfSections(in expandableTableView: LUExpandableTableView) -> Int {
        return headerList.count
    }
    
    func expandableTableView(_ expandableTableView: LUExpandableTableView, numberOfRowsInSection section: Int) -> Int {
        //        if section == 0 {
        //            return 1
        //        }
        //        if section == headerList.count - 1 {
        //            return 0
        //        }
        //        return headerList[section].list.count
        //        return headerList.count
        return arr.count
    }
    
    func expandableTableView(_ expandableTableView: LUExpandableTableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        //        if indexPath.section == 0 || indexPath.section == 1 || indexPath.section == 2 {
        if indexPath.section < 3 {
            guard let cell = expandableTableView.dequeueReusableCell(withIdentifier: cellReuseIdentifier) as? DotC_expandView1 else {
                assertionFailure("Cell shouldn't be nil")
                return UITableViewCell()
            }
            let dict = headerList[indexPath.section].options
            cell.btn_approval.tag = indexPath.section
            cell.btn_approval.addTarget(self, action: #selector(btn_Approval(_:)), for: .touchUpInside)
            cell.lbl_heading.text = dict
            
            return cell
        }else{
            guard let cell = expandableTableView.dequeueReusableCell(withIdentifier: cellReuseIdentifier2) as? DotC_expandView2 else {
                assertionFailure("Cell shouldn't be nil")
                return UITableViewCell()
            }
            
            let dict = headerList[indexPath.section].options
            cell.lbl_heading.text = dict
            cell.btn_submit.tag = indexPath.section
            cell.btn_submit.addTarget(self, action: #selector(btn_Submit(_:)), for: .touchUpInside)
            
            return cell
        }
        
        
    }
    
    func expandableTableView(_ expandableTableView: LUExpandableTableView, sectionHeaderOfSection section: Int) -> LUExpandableTableViewSectionHeader {
        guard let sectionHeader = expandableTableView.dequeueReusableHeaderFooterView(withIdentifier: sectionHeaderReuseIdentifier) as? DotC_HeaderCell else {
            assertionFailure("Section header shouldn't be nil")
            return LUExpandableTableViewSectionHeader()
        }
        if headerList[section].status == "Completed"{
            sectionHeader.lbl_status.textColor = #colorLiteral(red: 0.337254902, green: 0.7411764706, blue: 0.4392156863, alpha: 1)
            sectionHeader.lbl_status.text = headerList[section].status
            sectionHeader.imgSelect.image = #imageLiteral(resourceName: "checkdG")
        }else{
            sectionHeader.lbl_status.textColor = #colorLiteral(red: 0.9254902005, green: 0.2352941185, blue: 0.1019607857, alpha: 1)
            sectionHeader.lbl_status.text = headerList[section].status
            sectionHeader.imgSelect.image = #imageLiteral(resourceName: "uncheckG")
        }
        sectionHeader.lbl_options.text = headerList[section].options
        //        sectionHeader.lbl_status.text = headerList[section].status
        
        //     sectionHeader.img.image = headerList[section].icon
        //        if section == headerList.count - 1 {
        //            sectionHeader.imgSelect.image = UIImage(named: "fwd_arrow")
        //        }
        return sectionHeader
    }
}

// MARK: - LUExpandableTableViewDelegate

extension DOT_C_VC: LUExpandableTableViewDelegate {
    func expandableTableView(_ expandableTableView: LUExpandableTableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        /// Returning `UITableViewAutomaticDimension` value on iOS 9 will cause reloading all cells due to an iOS 9 bug with automatic dimensions
        //        return 50
        return UITableView.automaticDimension
    }
    
    func expandableTableView(_ expandableTableView: LUExpandableTableView, heightForHeaderInSection section: Int) -> CGFloat {
        /// Returning `UITableViewAutomaticDimension` value on iOS 9 will cause reloading all cells due to an iOS 9 bug with automatic dimensions
        return UITableView.automaticDimension
    }
    
    // MARK: - Optional
    
    func expandableTableView(_ expandableTableView: LUExpandableTableView, didSelectRowAt indexPath: IndexPath) {
        print("Did select cell at section \(indexPath.section) row \(indexPath.row)")
        if indexPath.section == headerList.count - 1 || indexPath.section == 0 {
            return
        }
        
        //        if indexPath.section == headerList.count - 2 {
        //            if selected2.contains(indexPath) {
        //                selected2.remove(indexPath)
        //                let cell = expandableTableView.cellForRow(at: indexPath) as! MyTableViewCell
        //                cell.label.textColor = #colorLiteral(red: 0.2352941176, green: 0.2352941176, blue: 0.262745098, alpha: 0.6)
        //            }else{
        //                let cell = expandableTableView.cellForRow(at: indexPath) as! MyTableViewCell
        //                cell.label.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        //                selected2.add(indexPath)
        //            }
        //            return
        //        }
        //
        //        if selected.contains(indexPath) {
        //            let filter = selected.filter { (v) -> Bool in
        //                let v1 = v as! IndexPath
        //                return v1.section == indexPath.section
        //            }
        //            for i in filter {
        //                if let cell = expandableTableView.cellForRow(at: i as! IndexPath) as? MyTableViewCell {
        //                    cell.label.textColor = #colorLiteral(red: 0.2352941176, green: 0.2352941176, blue: 0.262745098, alpha: 0.6)
        //                }
        //                selected.remove(i)
        //            }
        ////            selected.remove(indexPath)
        //            let cell = expandableTableView.cellForRow(at: indexPath) as! MyTableViewCell
        //            cell.label.textColor = #colorLiteral(red: 0.2352941176, green: 0.2352941176, blue: 0.262745098, alpha: 0.6)
        //        }else{
        //            let filter = selected.filter { (v) -> Bool in
        //                let v1 = v as! IndexPath
        //                return v1.section == indexPath.section
        //            }
        //            for i in filter {
        //                if let cell = expandableTableView.cellForRow(at: i as! IndexPath) as? MyTableViewCell {
        //                    cell.label.textColor = #colorLiteral(red: 0.2352941176, green: 0.2352941176, blue: 0.262745098, alpha: 0.6)
        //                }
        //                selected.remove(i)
        //            }
        //            let cell = expandableTableView.cellForRow(at: indexPath) as! MyTableViewCell
        //            cell.label.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        //            selected.add(indexPath)
        //        }
        
    }
    
    func expandableTableView(_ expandableTableView: LUExpandableTableView, didSelectSectionHeader sectionHeader: LUExpandableTableViewSectionHeader, atSection section: Int) {
        print("Did select section header at section \(section)")
        
    }
    
    func expandableTableView(_ expandableTableView: LUExpandableTableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        print("Will display cell at section \(indexPath.section) row \(indexPath.row)")
    }
    
    func expandableTableView(_ expandableTableView: LUExpandableTableView, willDisplaySectionHeader sectionHeader: LUExpandableTableViewSectionHeader, forSection section: Int) {
        print("Will display section header for section \(section)")
    }
    
    func expandableTableView(_ expandableTableView: LUExpandableTableView, viewForFooterInSection section: Int) -> UIView? {
        return nil
    }
    
    func expandableTableView(_ expandableTableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0
    }
}

