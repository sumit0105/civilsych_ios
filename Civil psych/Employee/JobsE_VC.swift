//
//  JobsE_VC.swift
//  Civil psych
//
//  Created by Ankur  on 01/03/21.
//

import UIKit

class JobsE_VC: UIViewController {
    
    
    @IBOutlet weak var table:UITableView!

    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
    }
    
    func setup(){
        self.table.register(UINib(nibName: "JobsE_Cell", bundle: nil), forCellReuseIdentifier: "JobsE_Cell")
        table.delegate = self
        table.dataSource = self
    }
}

extension JobsE_VC :UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 12
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 129
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = table.dequeueReusableCell(withIdentifier: "JobsE_Cell", for: indexPath) as! JobsE_Cell
      
        cell.btnViewMore.tag = indexPath.row
        cell.btnViewMore.addTarget(self, action: #selector(btn_ViewMore(_:)), for: .touchUpInside)
       
        return cell
    }
    
    @IBAction func btn_ViewMore(_ sender: UIButton){
        let vc = JobsDescE_VC.instantiate(fromAppStoryboard: .HomeE)
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
}

