//
//  ProfileE_VC.swift
//  Civil psych
//
//  Created by Ankur  on 01/03/21.
//

import UIKit

class ProfileE_VC:UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = true
    }

        
    @IBAction func btn_Profile(_ sender:UIButton) {
        let vc = EditProfileE_VC.instantiate(fromAppStoryboard: .ProfileE)
        vc.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(vc, animated: true)
    }
        
    @IBAction func btn_Support(_ sender:UIButton) {
        let vc = SupportC_VC.instantiate(fromAppStoryboard: .ProfileC)
        self.navigationController?.pushViewController(vc, animated: true)
    }

    
    @IBAction func btn_ChangePassword(_ sender:UIButton) {
        let vc = ChangePassVC.instantiate(fromAppStoryboard: .ProfileC)
        self.present(vc, animated: true, completion: nil)
    }
    
    @IBAction func btn_Aboutus(_ sender:UIButton) {
        let vc = AboutusC_VC.instantiate(fromAppStoryboard: .ProfileC)
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func btn_Terms(_ sender:UIButton) {
        let vc = TermsC_VC.instantiate(fromAppStoryboard: .ProfileC)
        self.navigationController?.pushViewController(vc, animated: true)

    }
    
    @IBAction func btn_Privacy(_ sender:UIButton) {
        let vc = PrivacyC_VC.instantiate(fromAppStoryboard: .ProfileC)
        self.navigationController?.pushViewController(vc, animated: true)

    }
    
    
    @IBAction func btn_Logout(_ sender:UIButton) {
        let vc = LoginE_VC.instantiate(fromAppStoryboard: .LoginEmployee)
        vc.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(vc, animated: true)
//        self.navigationController?.navigationController?.popViewController(animated: true)
    }
}


