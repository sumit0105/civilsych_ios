//
//  SearchE_VC.swift
//  Civil psych
//
//  Created by Ankur  on 03/03/21.
//

import UIKit

class SearchE_VC: UIViewController {
    
    @IBOutlet weak var table: UITableView!
    @IBOutlet weak var searchTxt: UITextField!
    var List : [HomeListE_Modal] = []
    var filteredData: [HomeListE_Modal] = []
    

    override func viewDidLoad() {
        super.viewDidLoad()

        setUp()
       
    }
    
    func setUp()  {
        self.table.register(UINib(nibName: "SearchE_Cell", bundle: nil), forCellReuseIdentifier: "SearchE_Cell")
       
        table.delegate = self
        table.dataSource = self
        
        List.append(HomeListE_Modal(clinician_Name: "Dr. Mike Linchon", distance: "107 miles away", price: "$350", paymentMethod: "One Time Payment", imgUser: #imageLiteral(resourceName: "i13")))
        List.append(HomeListE_Modal(clinician_Name: "Dr. Alina Methew", distance: "107 miles away", price: "$350", paymentMethod: "One Time Payment", imgUser: #imageLiteral(resourceName: "profile-Image")))
        List.append(HomeListE_Modal(clinician_Name: "Dr. Mike Linchon", distance: "107 miles away", price: "$350", paymentMethod: "One Time Payment", imgUser: #imageLiteral(resourceName: "i13")))
        List.append(HomeListE_Modal(clinician_Name: "Dr. Alina Methew", distance: "107 miles away", price: "$350", paymentMethod: "One Time Payment", imgUser: #imageLiteral(resourceName: "profile-Image")))
        List.append(HomeListE_Modal(clinician_Name: "Dr. Mike Linchon", distance: "107 miles away", price: "$350", paymentMethod: "One Time Payment", imgUser: #imageLiteral(resourceName: "i13")))
        List.append(HomeListE_Modal(clinician_Name: "Dr. Alina Methew", distance: "107 miles away", price: "$350", paymentMethod: "One Time Payment", imgUser: #imageLiteral(resourceName: "profile-Image")))
        self.filteredData = self.List
    }
    
}

extension SearchE_VC: UITableViewDelegate,UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return filteredData.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
            let cell = table.dequeueReusableCell(withIdentifier: "SearchE_Cell") as! SearchE_Cell
//            let dict = List[indexPath.row]
            let dict = filteredData[indexPath.row]
            cell.lbl_ClinicianName.text = dict.clinician_Name
            cell.lbl_price.text = dict.price
            cell.lbl_distance.text = dict.distance
            cell.lbl_paymentType.text = dict.paymentMethod
            cell.img_user.image = dict.imgUser
          
            if indexPath.row % 2 != 0 {
                cell.backgroundColor = #colorLiteral(red: 0.9046487808, green: 0.9457306266, blue: 0.9795823693, alpha: 1)
            }else{
                cell.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            }
            
            return cell
            
        }
        
    
//    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        if indexPath.section == 0 {
//            let cell = table.cellForRow(at: indexPath) as! HomeE_AllSelectCell
//            cell.img_Select.image = #imageLiteral(resourceName: "checked")
//        //    table.reloadData()
//        }else{
//            let cell = table.cellForRow(at: indexPath) as! HomeE_Cell
//            cell.img_Select.image = #imageLiteral(resourceName: "checked")
//       //     table.reloadData()
//        }
//    }
    
    
}
extension SearchE_VC: UITextFieldDelegate {
    @IBAction func changeText(_ sender:UITextField) {
        if sender.text?.count == 0 {
            self.filteredData = self.List
        }else{
            filteredData = List.filter({ (text) -> Bool in
                let tmp:NSString = text.clinician_Name! as NSString
                let range = tmp.range(of: sender.text!, options: NSString.CompareOptions.caseInsensitive)
                return range.location != NSNotFound
            })
        }
        table.reloadData()
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
 
        print("textFieldDidBeginEditing")
    }

}
