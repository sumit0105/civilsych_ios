//
//  FiltersE_VC.swift
//  Civil psych
//
//  Created by Ankur  on 01/03/21.
//

import UIKit

class FiltersE_VC: UIViewController {
    
    @IBOutlet weak var table: UITableView!
    @IBOutlet weak var tableHeight: NSLayoutConstraint!
    @IBOutlet weak var table1: UITableView!
    @IBOutlet weak var tableHeight1: NSLayoutConstraint!
    
    let arrSortBy = ["Recent Signup","Recent Activity"]
    let arrSections = ["Location","Language","Preferred Payment Type"]
    let arrDotSegment = ["Arabic","Chinese","English","Farsi","French","Russian","Spanish"]
    let arrAssessmentStage = ["One-Time","Monthly × 4"]
    var List : [Preferred_PaymentModel] = []
    
    var selectedIndex1 = IndexPath(row: -1, section: 0)
    var selectedIndex2 = IndexPath(row: -1, section: 0)
   
    //  var selectedIndex3 = IndexPath(row: -1, section: 0)
    
    var tableViewHeight: CGFloat {
        table.layoutIfNeeded()
        return table.contentSize.height
    }
    
    var tableViewHeight1: CGFloat {
        table1.layoutIfNeeded()
        return table1.contentSize.height
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        table1.isHidden = true
        setUp()
        updateHeight(tble: table)
        self.table.allowsMultipleSelection = true
//        self.table.allowsMultipleSelectionDuringEditing = true

        
    }
    
    
    func setUp()  {
        self.table.register(UINib(nibName: "FilterE_LocationCell", bundle: nil), forCellReuseIdentifier: "FilterE_LocationCell")
        self.table.register(UINib(nibName: "FilterE_LanguageCell", bundle: nil), forCellReuseIdentifier: "FilterE_LanguageCell")
        self.table.register(UINib(nibName: "FilterE_PaymentCell", bundle: nil), forCellReuseIdentifier: "FilterE_PaymentCell")
        self.table1.register(UINib(nibName: "Preferred_PaymentCell", bundle: nil), forCellReuseIdentifier: "Preferred_PaymentCell")
        
        List.append(Preferred_PaymentModel(emi_Num: "Your card will be charged First Time", amount: "$ 400", date: "17 June 2021"))
        List.append(Preferred_PaymentModel(emi_Num: "Your card will be charged Second Time", amount: "$ 400", date: "17 October 2021"))
        List.append(Preferred_PaymentModel(emi_Num: "Your card will be charged Third Time", amount: "$ 400", date: "17 Feb 2022"))
        List.append(Preferred_PaymentModel(emi_Num: "Your card will be charged Fourth Time", amount: "$ 400", date: "17 June 2022"))
        table.delegate = self
        table.dataSource = self
        table1.delegate = self
        table1.dataSource = self
    }
    
    @IBAction func btn_Back(_ sender:UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func btn_Apply(_ sender:UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func btn_ClearAll(_ sender:UIButton) {
        selectedIndex1 = IndexPath(row: -1, section: 0)
        selectedIndex2 = IndexPath(row: -1, section: 0)
        table.reloadData()
        table1.isHidden = true

    }
    
    
}

extension FiltersE_VC: UITableViewDelegate,UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if tableView == table {
            return arrSections.count
        }else{
            return 1
        }
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == table {
            if section == 0 {
                return 1
            }else if section == 1 {
                return arrDotSegment.count
            }else{
                return arrAssessmentStage.count
            }
            
        }else{
            return List.count
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if tableView == table {
            let headerView = UIView.init(frame: CGRect.init(x: 0, y: 0, width: tableView.frame.width, height: 30))
            headerView.backgroundColor = #colorLiteral(red: 0.07450980392, green: 0.368627451, blue: 0.7176470588, alpha: 1)
            let label = UILabel()
            label.frame = CGRect.init(x: 20, y: 0, width: headerView.frame.width, height: headerView.frame.height)
            
            label.text = arrSections[section]
            label.textColor = UIColor.white
            label.font = UIFont(name: AppFonts.Poppins_SemiBold.rawValue, size: 15.0)
            
            headerView.addSubview(label)
            
            return headerView
        }else{
            return tableView
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if tableView == table {
            if indexPath.section == 0 {
                let cell = table.dequeueReusableCell(withIdentifier: "FilterE_LocationCell") as! FilterE_LocationCell
                
                return cell
                
            }else if indexPath.section == 1{
                let cell = table.dequeueReusableCell(withIdentifier: "FilterE_LanguageCell") as! FilterE_LanguageCell
                cell.lbl_txt.text = arrDotSegment[indexPath.row]
           //     cell.imgSelect.image = #imageLiteral(resourceName: "uncheck")
//                if selectedIndex1 == indexPath {
//                    cell.imgSelect.image = #imageLiteral(resourceName: "checked")
//                }
                
                return cell
                
            }else{
                let cell = table.dequeueReusableCell(withIdentifier: "FilterE_PaymentCell") as! FilterE_PaymentCell
                cell.lbl_txt.text = arrAssessmentStage[indexPath.row]
                cell.imgSelect.image = #imageLiteral(resourceName: "uncheck")
                if selectedIndex2 == indexPath {
                    cell.imgSelect.image = #imageLiteral(resourceName: "checked")
                    
                }
                
                return cell
                
            }
            
        }else{
            let cell = table1.dequeueReusableCell(withIdentifier: "Preferred_PaymentCell") as! Preferred_PaymentCell
            let dict = List[indexPath.row]
            cell.lbl_Installment.text = dict.emi_Num
            cell.lbl_date.text = dict.date
            cell.txt_amount.text = dict.amount
            
            return cell
        }
        
        
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView == table {
            if indexPath.section == 1 {
//                let indexPath = table.indexPathForSelectedRow
                if indexPath == selectedIndex1 {
                    let cell = table.cellForRow(at: indexPath) as! FilterE_LanguageCell
                    selectedIndex1 = indexPath
                   
//                    cell.imgSelect.image = #imageLiteral(resourceName: "checked")
                    cell.imgSelect.image = #imageLiteral(resourceName: "uncheck")
                } else {
                    let cell = table.cellForRow(at: indexPath) as! FilterE_LanguageCell
                    cell.imgSelect.image = #imageLiteral(resourceName: "checked")
//                    cell.imgSelect.image = #imageLiteral(resourceName: "uncheck")
                }

//                selectedIndex1 = indexPath
                table.reloadData()
            }else if indexPath.section == 2 {
                selectedIndex2 = indexPath
                if selectedIndex2.row == 1{
                    updateHeight(tble: table1)
                    print("index \(selectedIndex2)")
                    table1.isHidden = false
                    //                    table1.reloadData()
                }else{
                    table1.isHidden = true
                    //                    table1.reloadData()
                }
                
                table.reloadData()
            }
            
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if tableView == table {
            return 30
        }else{
            return 0
        }
        
    }
    //    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    //        if indexPath.section == 0 {
    //            return 35
    //        }else{
    //            return 40
    //        }
    //    }
    
    func updateHeight(tble:UITableView)  {
        if tble == table {
            let f = tableViewHeight
            tableHeight.constant = f
            self.table.isScrollEnabled = false
            DispatchQueue.main.async {
                let ff2 = self.tableViewHeight
                self.tableHeight.constant = ff2
            }
        }else{
            let f = tableViewHeight1
            tableHeight1.constant = f
            self.table1.isScrollEnabled = false
            DispatchQueue.main.async {
                let ff2 = self.tableViewHeight1
                self.tableHeight1.constant = ff2
            }
        }
        
    }
}
