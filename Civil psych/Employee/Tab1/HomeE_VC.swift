//
//  HomeE_VC.swift
//  Civil psych
//
//  Created by Ankur  on 01/03/21.
//

import UIKit

class HomeE_VC: UIViewController {
    
    @IBOutlet weak var btnView_oneTime: UIView!
    @IBOutlet weak var btnView_Monthly: UIView!
    @IBOutlet weak var btn_oneTime: UIButton!
    @IBOutlet weak var btn_Monthly: UIButton!


    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    var b = 0
    
    @IBAction func btn_paymentType(_ sender:UIButton){
        if sender.tag == 0 {
            btn_oneTime.setTitleColor(.white, for: .normal)
            btn_Monthly.setTitleColor(.darkGray, for: .normal)
            btnView_oneTime.backgroundColor = #colorLiteral(red: 0.07617305964, green: 0.3686580062, blue: 0.7158398032, alpha: 1)
            btnView_Monthly.backgroundColor = #colorLiteral(red: 0.8549019608, green: 0.9215686275, blue: 0.9882352941, alpha: 1)
            b = 1
        }else{
            btnView_Monthly.backgroundColor = #colorLiteral(red: 0.07617305964, green: 0.3686580062, blue: 0.7158398032, alpha: 1)
            btnView_oneTime.backgroundColor = #colorLiteral(red: 0.8549019608, green: 0.9215686275, blue: 0.9882352941, alpha: 1)
            btn_Monthly.setTitleColor(.white, for: .normal)
            btn_oneTime.setTitleColor(.darkGray, for: .normal)

            b = 2
        }
    }
    
    @IBAction func btn_Filter(_ sender:UIButton) {
        let vc = FiltersE_VC.instantiate(fromAppStoryboard: .HomeE)
        self.present(vc, animated: true, completion: nil)
    }
    
    @IBAction func btn_Submit(_ sender:UIButton) {
        let vc = HomeListE_VC.instantiate(fromAppStoryboard: .HomeE)
        self.navigationController?.pushViewController(vc, animated: true)
    }

    @IBAction func btn_Search(_ sender:UIButton) {
        let vc = SearchE_VC.instantiate(fromAppStoryboard: .HomeE)
        self.navigationController?.pushViewController(vc, animated: true)
    }


}
