//
//  JobsDescE_VC.swift
//  Civil psych
//
//  Created by Ankur  on 01/03/21.
//

import UIKit
import MobileCoreServices

class JobsDescE_VC: UIViewController {
    
    @IBOutlet weak var txt_url:UITextField!

    override func viewDidLoad() {
        super.viewDidLoad()

//        clickFunction()
    }
    @IBAction func btn_back(_ sender:UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btn_apply(_ sender:UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func btn_uploadResume(_ sender:UIButton) {
        attachDocument()
    }
    
    private func attachDocument() {
        let types = [kUTTypePDF, kUTTypeText]
        let importMenu = UIDocumentPickerViewController(documentTypes: types as [String], in: .import)

        if #available(iOS 11.0, *) {
            importMenu.allowsMultipleSelection = false
        }

        importMenu.delegate = self
        importMenu.modalPresentationStyle = .formSheet

        present(importMenu, animated: true)
    }

}
extension JobsDescE_VC: UIDocumentPickerDelegate,UINavigationControllerDelegate {

    public func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentsAt urls: [URL]) {
        guard let myURL = urls.first else {
            return
        }
        print("import result : \(myURL)")
        txt_url.text = myURL.absoluteString
    }
    
    func documentPickerWasCancelled(_ controller: UIDocumentPickerViewController) {
        print("view was cancelled")
        dismiss(animated: true, completion: nil)
    }
}
