//
//  HomeListE_VC.swift
//  Civil psych
//
//  Created by Ankur  on 03/03/21.
//

import UIKit

class HomeListE_VC: UIViewController {
    
    @IBOutlet weak var table: UITableView!
    var List : [HomeListE_Modal] = []
    
    var selectedIndex = IndexPath(row: -1, section: 0)
    var isAllSelected = false
    //  var selectedIndex3 = IndexPath(row: -1, section: 0)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUp()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        selectedIndex = IndexPath(row: -1, section: 0)
        table.reloadData()
    }
    
    @IBAction func btn_Filter(_ sender:UIButton) {
        let vc = FiltersE_VC.instantiate(fromAppStoryboard: .HomeE)
        self.present(vc, animated: true, completion: nil)
    }
    
    @IBAction func btn_Search(_ sender:UIButton) {
        let vc = SearchE_VC.instantiate(fromAppStoryboard: .HomeE)
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func setUp()  {
        self.table.register(UINib(nibName: "HomeE_AllSelectCell", bundle: nil), forCellReuseIdentifier: "HomeE_AllSelectCell")
        self.table.register(UINib(nibName: "HomeE_Cell", bundle: nil), forCellReuseIdentifier: "HomeE_Cell")
        
        table.delegate = self
        table.dataSource = self
        
        List.append(HomeListE_Modal(clinician_Name: "Dr. Mike Linchon", distance: "107 miles away", price: "$350", paymentMethod: "One Time Payment", imgUser: #imageLiteral(resourceName: "i13")))
        List.append(HomeListE_Modal(clinician_Name: "Dr. Alina Methew", distance: "107 miles away", price: "$350", paymentMethod: "One Time Payment", imgUser: #imageLiteral(resourceName: "profile-Image")))
        List.append(HomeListE_Modal(clinician_Name: "Dr. Mike Linchon", distance: "107 miles away", price: "$350", paymentMethod: "One Time Payment", imgUser: #imageLiteral(resourceName: "i13")))
        List.append(HomeListE_Modal(clinician_Name: "Dr. Alina Methew", distance: "107 miles away", price: "$350", paymentMethod: "One Time Payment", imgUser: #imageLiteral(resourceName: "profile-Image")))
        List.append(HomeListE_Modal(clinician_Name: "Dr. Mike Linchon", distance: "107 miles away", price: "$350", paymentMethod: "One Time Payment", imgUser: #imageLiteral(resourceName: "i13")))
        List.append(HomeListE_Modal(clinician_Name: "Dr. Alina Methew", distance: "107 miles away", price: "$350", paymentMethod: "One Time Payment", imgUser: #imageLiteral(resourceName: "profile-Image")))
        
    }
    
    
}

extension HomeListE_VC: UITableViewDelegate,UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return 1
        }else{
            return List.count
        }
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        
        if indexPath.section == 0 {
            
            let cell = table.dequeueReusableCell(withIdentifier: "HomeE_AllSelectCell") as! HomeE_AllSelectCell
            cell.img_Select.image = #imageLiteral(resourceName: "uncheck")
            cell.btn_requestAssement.tag = indexPath.row
            cell.btn_requestAssement.addTarget(self, action: #selector(btn_reqAssessment(_:)), for: .touchUpInside)
//            if selectedIndex == indexPath{
//                cell.img_Select.image = #imageLiteral(resourceName: "checked")
//            }
            
            var select = false
            for i in 0..<List.count {
                select = self.List[i].isSelected
                if select == false {
                    break
                }
            }
            isAllSelected = false
            if select == true {
                isAllSelected = true
                cell.img_Select.image = #imageLiteral(resourceName: "checked")
            }
            return cell
            
        }else{
            let cell = table.dequeueReusableCell(withIdentifier: "HomeE_Cell") as! HomeE_Cell
            let dict = List[indexPath.row]
            cell.lbl_ClinicianName.text = dict.clinician_Name
            cell.lbl_price.text = dict.price
            cell.lbl_distance.text = dict.distance
            cell.lbl_paymentType.text = dict.paymentMethod
            cell.img_user.image = dict.imgUser
            cell.img_Select.image = #imageLiteral(resourceName: "uncheck")
//            if selectedIndex == indexPath{
//                cell.img_Select.image = #imageLiteral(resourceName: "checked")
//            }
            if self.List[indexPath.row].isSelected == true {
                cell.img_Select.image = #imageLiteral(resourceName: "checked")
            }
            
            if indexPath.row % 2 != 0 {
                cell.backgroundColor = #colorLiteral(red: 0.9046487808, green: 0.9457306266, blue: 0.9795823693, alpha: 1)
            }else{
                cell.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            }
            
            return cell
            
        }
        
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        self.List[indexPath.row].isSelected = !self.List[indexPath.row].isSelected
//        self.table.reloadData()
//        return
        if indexPath.section == 0 {
            if isAllSelected == true{
                for i in 0..<List.count {
                    self.List[i].isSelected = false
                }
            }else{
                for i in 0..<List.count {
                    self.List[i].isSelected = true
                }
            }
            self.table.reloadData()
//            let cell = table.cellForRow(at: indexPath) as! HomeE_AllSelectCell
//            cell.img_Select.image = #imageLiteral(resourceName: "checked")
            
        //    table.reloadData()
        }else{
            self.List[indexPath.row].isSelected = !self.List[indexPath.row].isSelected
            self.table.reloadData()
//            let cell = table.cellForRow(at: indexPath) as! HomeE_Cell
//            cell.img_Select.image = #imageLiteral(resourceName: "checked")
            
       //     table.reloadData()
        }
    }
    
    @IBAction func btn_reqAssessment(_ sender:UIButton) {
        tabBarController?.selectedIndex = 3
    }

    
//        func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
//            if indexPath.section == 0 {
//                let cell = table.cellForRow(at: indexPath) as! HomeE_AllSelectCell
//                cell.img_Select.image = #imageLiteral(resourceName: "uncheck")
//            }else{
//                let cell = table.cellForRow(at: indexPath) as! HomeE_Cell
//                cell.img_Select.image = #imageLiteral(resourceName: "uncheck")
//            }
//
//        }
    
//        func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//            if indexPath.section == 0 {
//                return 35
//            }else{
//                return 40
//            }
//        }
    
}
