//
//  CongratsE_VC.swift
//  Civil psych
//
//  Created by Ankur  on 08/03/21.
//

import UIKit

class CongratsE_VC: UIViewController {
    
    typealias selectBtn = (String,Int) -> Void
    var select : selectBtn? = nil

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func BtnBack(_ sender:UIButton){
        self.dismiss(animated: true, completion: {
            self.select!("s",1)
        })
    }

    
}
