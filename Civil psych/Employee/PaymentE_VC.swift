//
//  PaymentE_VC.swift
//  Civil psych
//
//  Created by Ankur  on 08/03/21.
//

import UIKit

class PaymentE_VC: UIViewController {
    
    @IBOutlet weak var txtName : UITextField!
    @IBOutlet weak var txtNumber : UITextField!
    @IBOutlet weak var txtDate : UITextField!
    @IBOutlet weak var txtCVV : UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.txtName.delegate = self
        self.txtNumber.delegate = self
        self.txtDate.delegate = self
        self.txtCVV.delegate = self
    }
    
    @IBAction func BtnBack(_ sender:UIButton){
        self.navigationController?.popViewController(animated: false)
    }
    
    @IBAction func Btn_PayAction(_ sender:UIButton){
        Validation()
        let vc = CongratsE_VC.instantiate(fromAppStoryboard: .HomeE)
        vc.select = { [weak self] (str,status) in
            if status == 1 {
                self?.tabBarController?.selectedIndex = 0
            }
    }
        self.present(vc, animated: true, completion: nil)
}
}
extension PaymentE_VC: UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if textField == txtNumber {
            if range.location == 16 {
                return false
            }
            
            if string.count == 0 {
                return true
            }
            if !self.pavan_checkNumberOrAlphabet(Str: string) || string == " " {
                return false
            }
            if (range.location == 4) || (range.location == 9) || (range.location == 14) {
                //                let str = "\(textField.text!) "
                //                textField.text = str
            }
            return true
        }
        if textField == txtCVV {
            if range.location == 3 {
                return false
            }
            
            if string.count == 0 {
                return true
            }
            
            
            if !self.pavan_checkNumberOrAlphabet(Str: string) || string == " " {
                return false
            }
            
            
            return true
        }
        if textField == txtDate {
            if range.location == 7 {
                return false
            }
            
            if string.count == 0 {
                
                return true
            }
            if string == " " {
                return false
            }
            
            if !self.pavan_checkNumberOrAlphabet(Str: string) {
                return false
            }
            if (range.location == 2) {
                let str = "\(textField.text!)/"
                textField.text = str
            }
            return true
        }
        if textField == txtName {
            
            if string.count == 0 {
                return true
            }
            if string == " " {
                return true
            }
            if !self.pavan_checkAlphabet(Str: string) {
                return false
            }
            return true
        }
        return true
    }
}


extension PaymentE_VC {
    func Validation() {
        
        if txtName.text == "" {
            self.AlertControllerOnr(title: "Alert", message: "Please enter name", BtnTitle: "OK")
            return
        }
        
        if txtNumber.text == "" {
            self.AlertControllerOnr(title: "Alert", message: "Please enter credit card number.", BtnTitle: "OK")
            return
        }
        if txtNumber.text!.count < 16  {
            self.AlertControllerOnr(title: "Alert", message: "Please enter a valid credit card number.", BtnTitle: "OK")
            return
        }
        if txtDate.text == "" {
            self.AlertControllerOnr(title: "Alert", message: "Please enter Expiry Month and Year", BtnTitle: "OK")
            return
        }
        if txtDate.text!.count < 5 {
            self.AlertControllerOnr(title: "Alert", message: "Please enter a valid Month and Year", BtnTitle: "OK")
            return
        }
        
        if txtCVV.text == "" {
            self.AlertControllerOnr(title: "Alert", message: "Please enter CVV", BtnTitle: "OK")
            return
        }
        if txtCVV.text!.count < 3 {
            self.AlertControllerOnr(title: "Alert", message: "Please enter a valid CVV", BtnTitle: "OK")
            return
        }
    }
    
    
}
