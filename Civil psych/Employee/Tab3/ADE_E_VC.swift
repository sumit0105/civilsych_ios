//
//  ADE_E_VC.swift
//  Civil psych
//
//  Created by Ankur  on 04/03/21.
//

import UIKit

class ADE_E_VC: UIViewController {
    
    @IBOutlet weak var table: UITableView!
    @IBOutlet weak var btn_prev: UIButton!
    @IBOutlet weak var btn_next: UIButton!

    let arr1 = ["Q 1","Q 2","Q 3","Q 4","Q 5"]
    let arr2 = ["Q 6","Q 7","Q 8","Q 9","Q 10"]
    let arr3 = ["Q 11","Q 12","Q 13","Q 14","Q 15"]
    override func viewDidLoad() {
        super.viewDidLoad()

        setUp()
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        btn_prev.isHidden = true
    }
    
    func setUp()  {
        self.table.register(UINib(nibName: "PAQ_E_Cell", bundle: nil), forCellReuseIdentifier: "PAQ_E_Cell")
        table.delegate = self
        table.dataSource = self
    }

    @IBAction func btn_Next(_ sender:UIButton) {
        btn_next.tag = btn_next.tag + 1
        print("Tag:\(btn_next.tag)")
        table.reloadData()
        
        if btn_next.tag == 1 {
            btn_prev.isHidden = false
            btn_next.setTitle("Next", for: .normal)
        }else if btn_next.tag == 2 {
            btn_next.setTitle("Submit", for: .normal)
            btn_prev.isHidden = false
        }else{
            btn_prev.isHidden = true
            btn_next.setTitle("Next", for: .normal)
            self.navigationController?.popViewController(animated: true)
        }
    }
    @IBAction func btn_Prev(_ sender:UIButton) {
        table.reloadData()
        btn_next.tag = btn_next.tag - 1
        
        if btn_next.tag == 0 {
            btn_prev.isHidden = true
            btn_next.setTitle("Next", for: .normal)
        }else if btn_next.tag == 1 {
            btn_prev.isHidden = false
            btn_next.setTitle("Next", for: .normal)
        }
    }
    
    @IBAction func btn_Back(_ sender:UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
}


extension ADE_E_VC: UITableViewDelegate,UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if btn_next.tag == 0 {
            return arr1.count
        }else if btn_next.tag == 1{
            return arr2.count
        }else{
            return arr3.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = table.dequeueReusableCell(withIdentifier: "PAQ_E_Cell") as! PAQ_E_Cell
        
        if indexPath.row % 2 == 0 {
            cell.mainView.backgroundColor = .white
            cell.viewText.backgroundColor = #colorLiteral(red: 0.9646214843, green: 0.9647598863, blue: 0.9645913243, alpha: 1)
            cell.lbl_quesCount.text = "\(indexPath.row + 1)"
             
        }else{
            cell.mainView.backgroundColor = #colorLiteral(red: 0.9084902406, green: 0.9498965144, blue: 0.9871194959, alpha: 1)
            cell.viewText.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            cell.lbl_quesCount.text = "\(indexPath.row + 1)"
           
        }
        if btn_next.tag == 0 {
            cell.lbl_quesCount.text = arr1[indexPath.row]
        }else if btn_next.tag == 1{
            cell.lbl_quesCount.text = arr2[indexPath.row]
        }else{
            cell.lbl_quesCount.text = arr3[indexPath.row]
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}
