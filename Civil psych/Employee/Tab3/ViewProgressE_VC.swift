//
//  ViewProgressE_VC.swift
//  Civil psych
//
//  Created by Ankur  on 01/03/21.
//

import UIKit

class ViewProgressE_VC:UIViewController {
    
    @IBOutlet weak var collect:UICollectionView!
    @IBOutlet weak var table:UITableView!
    
    var CollectViewHeight: CGFloat {
        collect.layoutIfNeeded()
        return collect.contentSize.height
    }
    
    var selectedIndex = IndexPath(item: 0, section: 0)
    var list : [Overall_StatusModal] = []
//    let category_Arr = ["Overall Status","Profile","DOT","PAQ","ADE","Initial Assessment","Return to Duty Requirements","Follow Up Assessment","Post RTD Requirements"]
    
    let category_Arr = ["Overall Status","Profile","DOT","PAQ","ADE","Initial Assessment","Return to Duty Requirements","Follow Up Assessment","RTD Aftercare Requirements"]
//    let category_Arr = ["Overall Status","Profile","DOT","PAQ","ADE","Initial Assessment","Return to duty","Follow Up Assessment","Post RTD Requirements"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        LoadData()
    }
    
    @IBAction func btn_Back(_ sender:UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func LoadData(){
        collect.register(UINib(nibName: "ViewProgressC_OptionsCell", bundle: nil), forCellWithReuseIdentifier: "ViewProgressC_OptionsCell")
        table.register(UINib(nibName: "ViewProgressC_Cell", bundle: nil), forCellReuseIdentifier: "ViewProgressC_Cell")
        
        list.append(Overall_StatusModal(options: "Profile", status: "Completed", mark_status: "Completed", img_markstatus: #imageLiteral(resourceName: "MarkC")))
        list.append(Overall_StatusModal(options: "D.O.T. Clearinghouse Registration", status: "Completed", mark_status: "Completed", img_markstatus: #imageLiteral(resourceName: "MarkC")))
        list.append(Overall_StatusModal(options: "Pre-Assessment Questionnaire(PAQ)", status: "Completed", mark_status: "Completed", img_markstatus: #imageLiteral(resourceName: "MarkC")))
        list.append(Overall_StatusModal(options: "Psychometric Questionnaire(ADE)", status: "Pending", mark_status: "Mark Completed", img_markstatus: #imageLiteral(resourceName: "blank_MarkC")))
        list.append(Overall_StatusModal(options: "Initial Assessment", status: "Pending", mark_status: "Mark Completed", img_markstatus: #imageLiteral(resourceName: "blank_MarkC")))
        list.append(Overall_StatusModal(options: "Return to Duty Requirements", status: "Completed", mark_status: "Completed", img_markstatus: #imageLiteral(resourceName: "MarkC")))
        list.append(Overall_StatusModal(options: "Follow Up Assessment", status: "Pending", mark_status: "Mark Completed", img_markstatus: #imageLiteral(resourceName: "blank_MarkC")))
//        list.append(Overall_StatusModal(options: "Post RTD Requirements", status: "Completed", mark_status: "Completed", img_markstatus: #imageLiteral(resourceName: "MarkC")))
        list.append(Overall_StatusModal(options: "RTD Aftercare Requirements", status: "Completed", mark_status: "Completed", img_markstatus: #imageLiteral(resourceName: "MarkC")))
        
        collect.delegate = self
        collect.dataSource = self
        table.delegate = self
        table.dataSource = self
        
        
    }
}

extension ViewProgressE_VC:UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return category_Arr.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collect.dequeueReusableCell(withReuseIdentifier: "ViewProgressC_OptionsCell", for: indexPath) as! ViewProgressC_OptionsCell
        
        cell.lbl_options.text = category_Arr[indexPath.item]
        cell.viewBack.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        cell.lbl_options.textColor = #colorLiteral(red: 0.07450980392, green: 0.368627451, blue: 0.7176470588, alpha: 1)
        cell.viewBack.cornerRadius = Double(cell.viewBack.frame.size.height/2)
        if self.selectedIndex == indexPath {
            cell.viewBack.backgroundColor = #colorLiteral(red: 0.07450980392, green: 0.368627451, blue: 0.7176470588, alpha: 1)
            cell.lbl_options.textColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        }
       
//        DispatchQueue.main.async {
//            self.collect.layoutIfNeeded()
//        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        selectedIndex = indexPath
        
        if indexPath.row == 1 {
            let vc = CreateProfileE_VC.instantiate(fromAppStoryboard: .LoginEmployee)
            vc.hidesBottomBarWhenPushed = true
            self.navigationController?.pushViewController(vc, animated: true)
        }else if indexPath.row == 2 {
            
            let vc = DOT_E_VC.instantiate(fromAppStoryboard: .HomeE)
            vc.hidesBottomBarWhenPushed = true
            self.navigationController?.pushViewController(vc, animated: true)
            
        }else if indexPath.row == 3 {
            
            let vc = PAQ_E_VC.instantiate(fromAppStoryboard: .HomeE)
            vc.hidesBottomBarWhenPushed = false
            self.navigationController?.pushViewController(vc, animated: true)
            
        }else if indexPath.row == 4 {
            
            let vc = ADE_E_VC.instantiate(fromAppStoryboard: .HomeE)
            vc.hidesBottomBarWhenPushed = false
            self.navigationController?.pushViewController(vc, animated: true)
            
        }else if indexPath.row == 5 {
            let vc = InitialAssesmentC_VC.instantiate(fromAppStoryboard: .HomeC)
            vc.hidesBottomBarWhenPushed = false
            self.navigationController?.pushViewController(vc, animated: true)

        }else if indexPath.row == 6 {
            let vc = ReturnToDutyE_VC.instantiate(fromAppStoryboard: .HomeE)
            vc.hidesBottomBarWhenPushed = false
            self.navigationController?.pushViewController(vc, animated: true)
        }else if indexPath.row == 7 {
            let vc = FollowUpAssesmentC_VC.instantiate(fromAppStoryboard: .HomeC)
            vc.hidesBottomBarWhenPushed = false
            self.navigationController?.pushViewController(vc, animated: true)
        }else if indexPath.row == 8 {
            let vc = PostRtdE_VC.instantiate(fromAppStoryboard: .HomeE)
            vc.hidesBottomBarWhenPushed = false
            self.navigationController?.pushViewController(vc, animated: true)
        }
        
        collect.reloadData()
    }
        func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
            let tt = self.category_Arr[indexPath.item]
            let lbl = UILabel()
            lbl.text = tt
            let pp = lbl.textWidth()
       //     let w = collect.frame.size.width/3
            let h = collect.frame.size.height
    
            return CGSize(width: pp + 60, height: h)
    
        }
}

extension ViewProgressE_VC: UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return list.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = table.dequeueReusableCell(withIdentifier: "ViewProgressC_Cell") as! ViewProgressC_Cell
        let dict = list[indexPath.row]
        if dict.mark_status == "Completed"{
            cell.lbl_status.textColor = #colorLiteral(red: 0.337254902, green: 0.7411764706, blue: 0.4392156863, alpha: 1)
            cell.lbl_status.text = dict.status
        }else{
            cell.lbl_status.textColor = #colorLiteral(red: 0.9254902005, green: 0.2352941185, blue: 0.1019607857, alpha: 1)
            cell.lbl_status.text = dict.status
        }
        cell.lbl_options.text = dict.options
        
        cell.lbl_markStatus.text = dict.mark_status
        cell.img_markStatus.image = dict.img_markstatus
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 0 {
            let vc = CreateProfileE_VC.instantiate(fromAppStoryboard: .LoginEmployee)
            vc.hidesBottomBarWhenPushed = true
            self.navigationController?.pushViewController(vc, animated: true)
        }else if indexPath.row == 1 {
            
            let vc = DOT_E_VC.instantiate(fromAppStoryboard: .HomeE)
            vc.hidesBottomBarWhenPushed = true
            self.navigationController?.pushViewController(vc, animated: true)
            
        }else if indexPath.row == 2 {
            
            let vc = PAQ_E_VC.instantiate(fromAppStoryboard: .HomeE)
            vc.hidesBottomBarWhenPushed = false
            self.navigationController?.pushViewController(vc, animated: true)
            
        }else if indexPath.row == 3 {
            
            let vc = ADE_E_VC.instantiate(fromAppStoryboard: .HomeE)
            vc.hidesBottomBarWhenPushed = false
            self.navigationController?.pushViewController(vc, animated: true)
            
        }else if indexPath.row == 4 {
            let vc = InitialAssesmentC_VC.instantiate(fromAppStoryboard: .HomeC)
            vc.hidesBottomBarWhenPushed = false
            self.navigationController?.pushViewController(vc, animated: true)

        }else if indexPath.row == 5 {
            let vc = ReturnToDutyE_VC.instantiate(fromAppStoryboard: .HomeE)
            vc.hidesBottomBarWhenPushed = false
            self.navigationController?.pushViewController(vc, animated: true)
        }else if indexPath.row == 6 {
            let vc = FollowUpAssesmentC_VC.instantiate(fromAppStoryboard: .HomeC)
            vc.hidesBottomBarWhenPushed = false
            self.navigationController?.pushViewController(vc, animated: true)
        }else if indexPath.row == 7 {
            let vc = PostRtdE_VC.instantiate(fromAppStoryboard: .HomeE)
            vc.hidesBottomBarWhenPushed = false
            self.navigationController?.pushViewController(vc, animated: true)
        }

    }
}
