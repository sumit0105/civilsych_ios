//
//  DOT_E_VC.swift
//  Civil psych
//
//  Created by Ankur  on 04/03/21.
//

import UIKit
import LUExpandableTableView

class DOT_E_VC:UIViewController {
    
    @IBOutlet weak var table:UITableView!
    
    @IBOutlet weak var scroll: UIScrollView!
    @IBOutlet weak var tableHeight: NSLayoutConstraint!
    @IBOutlet weak var expandableTableView : LUExpandableTableView!
    @IBOutlet weak var btn_Yes:UIButton!
    @IBOutlet weak var btn_No:UIButton!
    
    private let cellReuseIdentifier = "DOT_E_Cell"
    private let cellReuseIdentifier2 = "DOT_E_Cell2"
    private let sectionHeaderReuseIdentifier = "DotC_HeaderCell"
    var headerList : [ViewProgressC_Modal] = []
    var arr = [1]
    var selected : NSMutableArray = []
    var selected2 : NSMutableArray = []
    
    var tableViewHeight: CGFloat {
        expandableTableView.layoutIfNeeded()
        return expandableTableView.sectionHeaderHeight
      }
    
    override func viewDidLoad() {
        super.viewDidLoad()
//        table.register(UINib(nibName: "DotC_Cell", bundle: nil), forCellReuseIdentifier: "DotC_Cell")
//        table.delegate = self
//        table.dataSource = self
        Setup()
        updateHeight(table: expandableTableView)
//        updateHight(table: expandableTableView)
        
    }
    
    func Setup(){
        headerList.append(ViewProgressC_Modal(options: "D.O.T. Clearinghouse Registration", status: "Completed"))
        
        headerList.append(ViewProgressC_Modal(options: "SAP requested", status: "Pending"))
        
        headerList.append(ViewProgressC_Modal(options: "SAP Approval", status: "Pending"))
        
        headerList.append(ViewProgressC_Modal(options: "Initial Assessment Date", status: "Pending"))
        
        headerList.append(ViewProgressC_Modal(options: "Follow Assessment Date", status: "Pending"))
                
        
        expandableTableView.register(UINib(nibName: cellReuseIdentifier, bundle: Bundle.main), forCellReuseIdentifier: cellReuseIdentifier)
        expandableTableView.register(UINib(nibName: cellReuseIdentifier2, bundle: Bundle.main), forCellReuseIdentifier: cellReuseIdentifier2)
//        expandableTableView.register(UINib(nibName: "heightCell", bundle: Bundle.main), forCellReuseIdentifier: "heightCell")
        expandableTableView.register(UINib(nibName: "DotC_HeaderCell", bundle: Bundle.main), forHeaderFooterViewReuseIdentifier: sectionHeaderReuseIdentifier)
        
        expandableTableView.expandableTableViewDataSource = self
        expandableTableView.expandableTableViewDelegate = self
    }
    
    @IBAction func btn_Approval(_ sender: UIButton) {
//        expandableTableView.collapseSections(at: [3,4])
        if sender.tag == 3 {
            let stat = ViewProgressC_Modal(options: "Initial Assessment Date", status: "Completed")
            headerList[sender.tag] = stat
        }else if sender.tag == 4 {
            let stat = ViewProgressC_Modal(options: "Follow Assessment Date", status: "Completed")
            headerList[sender.tag] = stat
            self.navigationController?.popViewController(animated: true)
        }
        expandableTableView.collapseSections(at: [sender.tag])
        expandableTableView.reloadData()
    }
    
    @IBAction func btn_Submit(_ sender: UIButton) {
        if sender.tag == 1 {
            let stat = (ViewProgressC_Modal(options: "SAP requested", status: "Completed"))
            headerList[sender.tag] = stat
        }else if sender.tag == 2 {
            let stat = ViewProgressC_Modal(options: "SAP Approval", status: "Completed")
            headerList[sender.tag] = stat
        }
        expandableTableView.collapseSections(at: [sender.tag])
        expandableTableView.reloadData()
    }
    
    @IBAction func btn_back(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }

    @IBAction func btn_proceed(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    var p = 0
    @IBAction func btn_selection(_ sender:UIButton){
        btn_No.setImage(#imageLiteral(resourceName: "uncheck"), for: .normal)
        btn_Yes.setImage(#imageLiteral(resourceName: "uncheck"), for: .normal)

        if sender.tag == 0 {
            btn_Yes.setImage(#imageLiteral(resourceName: "checked"), for: .normal)
            btn_No.setImage(#imageLiteral(resourceName: "uncheck"), for: .normal)
            p = 1
        }else{
            btn_No.setImage(#imageLiteral(resourceName: "checked"), for: .normal)
            btn_Yes.setImage(#imageLiteral(resourceName: "uncheck"), for: .normal)
            p = 2
        }
    }
}

// MARK: - LUExpandableTableViewDataSource

extension DOT_E_VC: LUExpandableTableViewDataSource {
    
    func updateHeight(table:LUExpandableTableView)  {
        let cellH = table.expandedSections.count * 700 + 360
        let f = tableViewHeight + CGFloat(cellH)
        tableHeight.constant = f
        self.scroll.contentSize.height = CGFloat(f + 150)
        self.expandableTableView.isScrollEnabled = false
        DispatchQueue.main.async {
            let cellH = table.expandedSections.count * 700 + 360
            let ff2 = self.tableViewHeight + CGFloat(cellH)
            self.tableHeight.constant = ff2
            self.scroll.contentSize.height = CGFloat(ff2 + 150)
            
        }
    }
        
    func numberOfSections(in expandableTableView: LUExpandableTableView) -> Int {
        return headerList.count
    }
    
    func expandableTableView(_ expandableTableView: LUExpandableTableView, numberOfRowsInSection section: Int) -> Int {
//        if section == 0 {
//            return 1
//        }
//        if section == headerList.count - 1 {
//            return 0
//        }
//        return headerList[section].list.count
//        return headerList.count
        return arr.count
    }
    
    func expandableTableView(_ expandableTableView: LUExpandableTableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
//        if indexPath.section == 0 || indexPath.section == 1 || indexPath.section == 2 {
        if indexPath.section < 3 {
            guard let cell = expandableTableView.dequeueReusableCell(withIdentifier: cellReuseIdentifier) as? DOT_E_Cell else {
                assertionFailure("Cell shouldn't be nil")
                return UITableViewCell()
            }
            let dict = headerList[indexPath.section].options
            let txt1 = customizeFont(string: "What to do: ", font: UIFont(name: AppFonts.Poppins_SemiBold.rawValue, size: 12)!, color: .black)
            let txt2 = customizeFont(string: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.", font: UIFont(name: AppFonts.Poppins_Regular.rawValue, size: 12)!, color: .darkGray)
            
            let txtH = customizeFont(string: "How it Works: ", font: UIFont(name: AppFonts.Poppins_SemiBold.rawValue, size: 12)!, color: .black)
            let txtH1 = customizeFont(string: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.", font: UIFont(name: AppFonts.Poppins_Regular.rawValue, size: 12)!, color: .darkGray)
            
            txt1.append(txt2)
            txtH.append(txtH1)
            cell.lbl_whatTodo.attributedText = txt1
            cell.lbl_howItWorks.attributedText = txtH
            cell.btn_submit.tag = indexPath.section
            cell.btn_submit.addTarget(self, action: #selector(btn_Submit(_:)), for: .touchUpInside)

            cell.lbl_heading.text = dict
           
            return cell
        }else{
            guard let cell = expandableTableView.dequeueReusableCell(withIdentifier: cellReuseIdentifier2) as? DOT_E_Cell2 else {
                assertionFailure("Cell shouldn't be nil")
                return UITableViewCell()
            }

            let dict = headerList[indexPath.section].options
            let txt1 = customizeFont(string: "What to do: ", font: UIFont(name: AppFonts.Poppins_SemiBold.rawValue, size: 12)!, color: .black)
            let txt2 = customizeFont(string: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.", font: UIFont(name: AppFonts.Poppins_Regular.rawValue, size: 12)!, color: .darkGray)
            
            let txtH = customizeFont(string: "How it Works: ", font: UIFont(name: AppFonts.Poppins_SemiBold.rawValue, size: 12)!, color: .black)
            let txtH1 = customizeFont(string: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.", font: UIFont(name: AppFonts.Poppins_Regular.rawValue, size: 12)!, color: .darkGray)
            
            txt1.append(txt2)
            txtH.append(txtH1)
            cell.lbl_whatTodo.attributedText = txt1
            cell.lbl_howItWorks.attributedText = txtH
            cell.lbl_heading.text = dict
            cell.btn_approval.tag = indexPath.section
            cell.btn_approval.addTarget(self, action: #selector(btn_Approval(_:)), for: .touchUpInside)
           
            return cell
        }
        

    }
    
    func expandableTableView(_ expandableTableView: LUExpandableTableView, sectionHeaderOfSection section: Int) -> LUExpandableTableViewSectionHeader {
        guard let sectionHeader = expandableTableView.dequeueReusableHeaderFooterView(withIdentifier: sectionHeaderReuseIdentifier) as? DotC_HeaderCell else {
            assertionFailure("Section header shouldn't be nil")
            return LUExpandableTableViewSectionHeader()
        }
        if headerList[section].status == "Completed"{
            sectionHeader.lbl_status.textColor = #colorLiteral(red: 0.337254902, green: 0.7411764706, blue: 0.4392156863, alpha: 1)
            sectionHeader.lbl_status.text = headerList[section].status
            sectionHeader.imgSelect.image = #imageLiteral(resourceName: "checkdG")
        }else{
            sectionHeader.lbl_status.textColor = #colorLiteral(red: 0.9254902005, green: 0.2352941185, blue: 0.1019607857, alpha: 1)
            sectionHeader.lbl_status.text = headerList[section].status
            sectionHeader.imgSelect.image = #imageLiteral(resourceName: "uncheckG")
        }
        sectionHeader.lbl_options.text = headerList[section].options
//        sectionHeader.lbl_status.text = headerList[section].status
       
   //     sectionHeader.img.image = headerList[section].icon
//        if section == headerList.count - 1 {
//            sectionHeader.imgSelect.image = UIImage(named: "fwd_arrow")
//        }
        return sectionHeader
    }
}

// MARK: - LUExpandableTableViewDelegate

extension DOT_E_VC: LUExpandableTableViewDelegate {
    func expandableTableView(_ expandableTableView: LUExpandableTableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        /// Returning `UITableViewAutomaticDimension` value on iOS 9 will cause reloading all cells due to an iOS 9 bug with automatic dimensions
//        return 50
        print("row: \(indexPath.row)")
        return UITableView.automaticDimension
    }
    
    func expandableTableView(_ expandableTableView: LUExpandableTableView, heightForHeaderInSection section: Int) -> CGFloat {
        /// Returning `UITableViewAutomaticDimension` value on iOS 9 will cause reloading all cells due to an iOS 9 bug with automatic dimensions
        return UITableView.automaticDimension
    }
    
    // MARK: - Optional
    
    func expandableTableView(_ expandableTableView: LUExpandableTableView, didSelectRowAt indexPath: IndexPath) {
        print("Did select cell at section \(indexPath.section) row \(indexPath.row)")
        if indexPath.section == headerList.count - 1 || indexPath.section == 0 {
            return
        }
    }
   
    func expandableTableView(_ expandableTableView: LUExpandableTableView, didSelectSectionHeader sectionHeader: LUExpandableTableViewSectionHeader, atSection section: Int) {
        updateHeight(table: expandableTableView)
        print("Did select section header at section \(section)")

    }
    
    func expandableTableView(_ expandableTableView: LUExpandableTableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        updateHeight(table: expandableTableView)
        print("Will display cell at section \(indexPath.section) row \(indexPath.row)")
    }
    
    func expandableTableView(_ expandableTableView: LUExpandableTableView, willDisplaySectionHeader sectionHeader: LUExpandableTableViewSectionHeader, forSection section: Int) {
        updateHeight(table: expandableTableView)
        print("Will display section header for section \(section)")
    }

    func expandableTableView(_ expandableTableView: LUExpandableTableView, viewForFooterInSection section: Int) -> UIView? {
        return nil
    }

    func expandableTableView(_ expandableTableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0
    }
}

