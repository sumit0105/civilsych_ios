//
//  NotificationE_VC.swift
//  Civil psych
//
//  Created by Ankur  on 01/03/21.
//

import UIKit

class NotificationE_VC: UIViewController {
    
    var imgArr = [#imageLiteral(resourceName: "profile-Image"),#imageLiteral(resourceName: "i13"),#imageLiteral(resourceName: "profile-Image"),#imageLiteral(resourceName: "i13"),#imageLiteral(resourceName: "profile-Image"),#imageLiteral(resourceName: "i13"),#imageLiteral(resourceName: "profile-Image")]
    let imgArr1 = [#imageLiteral(resourceName: "profile-Image"),#imageLiteral(resourceName: "i13"),#imageLiteral(resourceName: "profile-Image"),#imageLiteral(resourceName: "i13"),#imageLiteral(resourceName: "profile-Image"),#imageLiteral(resourceName: "i13"),#imageLiteral(resourceName: "profile-Image")]
    
    @IBOutlet weak var table:UITableView!

    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
    }
    
    func setup(){
        self.table.register(UINib(nibName: "NotificationE_Cell", bundle: nil), forCellReuseIdentifier: "NotificationE_Cell")
        table.delegate = self
        table.dataSource = self
    }
}

extension NotificationE_VC :UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return imgArr.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = table.dequeueReusableCell(withIdentifier: "NotificationE_Cell", for: indexPath) as! NotificationE_Cell
        cell.img_user.image = imgArr[indexPath.row]
        cell.btnConfirmPay.tag = indexPath.row
        cell.btnConfirmPay.addTarget(self, action: #selector(btn_ConfirmPay(_:)), for: .touchUpInside)
        cell.btnView.isHidden = true
       
        if indexPath.row % 2 != 0 {
            cell.backgroundColor = #colorLiteral(red: 0.9046487808, green: 0.9457306266, blue: 0.9795823693, alpha: 1)
            cell.btnView.isHidden = true
        }else{
            cell.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            cell.btnView.isHidden = false
        }
            
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
    @IBAction func btn_ConfirmPay(_ sender: UIButton){
        let vc = PaymentE_VC.instantiate(fromAppStoryboard: .HomeE)
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
}

