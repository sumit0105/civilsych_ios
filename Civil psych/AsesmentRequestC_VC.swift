//
//  AsesmentRequestC_VC.swift
//  Civil psych
//
//  Created by Ankur  on 27/02/21.
//

import UIKit

class AsesmentRequestC_VC: UIViewController {
    
    var imgArr = [#imageLiteral(resourceName: "profile-Image"),#imageLiteral(resourceName: "i13"),#imageLiteral(resourceName: "profile-Image"),#imageLiteral(resourceName: "i13"),#imageLiteral(resourceName: "profile-Image"),#imageLiteral(resourceName: "i13"),#imageLiteral(resourceName: "profile-Image")]
    let imgArr1 = [#imageLiteral(resourceName: "profile-Image"),#imageLiteral(resourceName: "i13"),#imageLiteral(resourceName: "profile-Image"),#imageLiteral(resourceName: "i13"),#imageLiteral(resourceName: "profile-Image"),#imageLiteral(resourceName: "i13"),#imageLiteral(resourceName: "profile-Image")]
    
    @IBOutlet weak var table:UITableView!

    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
    }
    
    func setup(){
        self.table.register(UINib(nibName: "AssessmentC_Cell", bundle: nil), forCellReuseIdentifier: "AssessmentC_Cell")
        table.delegate = self
        table.dataSource = self
        tabBarController?.delegate = self
    }
}

extension AsesmentRequestC_VC :UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return imgArr.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = table.dequeueReusableCell(withIdentifier: "AssessmentC_Cell", for: indexPath) as! AssessmentC_Cell
        cell.img_user.image = imgArr[indexPath.row]
        cell.btnDecline.tag = indexPath.row
        cell.btnAccept.tag = indexPath.row
        cell.btnAccept.addTarget(self, action: #selector(btn_Accept(_:)), for: .touchUpInside)
        cell.btnDecline.addTarget(self, action: #selector(btn_Decline(_:)), for: .touchUpInside)
       
        if indexPath.row % 2 == 0 {
            cell.backgroundColor = #colorLiteral(red: 0.9046487808, green: 0.9457306266, blue: 0.9795823693, alpha: 1)
        }else{
            cell.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        }
        
//        if indexPath.row == 3 {
            let txt1 = customizeFont(string: "Richard Albert ", font: UIFont(name: AppFonts.Poppins_SemiBold.rawValue, size: 12)!, color: .black)
            
            let txt2 = customizeFont(string: "sent you assessment request", font: UIFont(name: AppFonts.Poppins_SemiBold.rawValue, size: 11)!, color: .lightGray)
        
            txt1.append(txt2)
            cell.lblComment.attributedText = txt1
            
//        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
    @IBAction func btn_Accept(_ sender: UIButton){
        tabBarController?.selectedIndex = 0
    }
    
    @IBAction func btn_Decline(_ sender: UIButton){
                
            let vc = AssessmentC_PopupVC.instantiate(fromAppStoryboard: .HomeC)
            vc.select = { [weak self] (str,status) in
                if status == 1 {
                    self?.imgArr.remove(at: sender.tag)
                    self?.table.reloadData()
                }
                
            }
            self.present(vc, animated: true, completion: nil)
            
        
        
    }
    
    
}

extension AsesmentRequestC_VC:UITabBarControllerDelegate {
    
    func tabBarController(_ tabBarController: UITabBarController, didSelect viewController: UIViewController) {
        let index = tabBarController.selectedIndex
        if index == 1 {
            for i in imgArr1 {
                imgArr.append(i)
            }
            table.reloadData()
        }
        
    }
}
