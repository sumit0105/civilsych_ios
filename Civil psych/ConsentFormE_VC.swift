//
//  ConsentFormE_VC.swift
//  Civil psych
//
//  Created by Ankur  on 25/02/21.
//

import UIKit

class ConsentFormE_VC: UIViewController {
    
    @IBOutlet weak var btn_accept:UIButton!
    

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func btn_Submit(_ sender:UIButton){
        if btn_accept.tag == 0 {
            self.AlertControllerOnr(title: alertTitle.alert_alert, message: messageString.E_empAccept)
            return
        }
        let vc = TabE_VC.instantiate(fromAppStoryboard: .TabE)
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func btn_accept(_ sender:UIButton){
        if sender.tag == 0 {
            sender.setImage(#imageLiteral(resourceName: "checked"), for: .normal)
            sender.tag = 1
            btn_accept.tag = 1
        }else{
            sender.setImage(#imageLiteral(resourceName: "uncheck"), for: .normal)
            sender.tag = 0
            btn_accept.tag = 0
        }
    }
}
