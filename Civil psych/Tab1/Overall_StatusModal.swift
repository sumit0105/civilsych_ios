//
//  Overall_StatusModal.swift
//  Civil psych
//
//  Created by Ankur  on 26/02/21.
//

import Foundation
import UIKit

struct Overall_StatusModal {
    var options:String?
    var status:String?
    var mark_status:String?
    var img_markstatus:UIImage?
}
