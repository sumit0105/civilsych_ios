//
//  SignupC_VC.swift
//  Civil psych
//
//  Created by Ankur  on 30/01/21.
//

import UIKit

class SignupC_VC: UIViewController {
    
    @IBOutlet weak var txt_pass: UITextField!
    @IBOutlet weak var txt_confirmPass: UITextField!
    @IBOutlet weak var img_pass: UIImageView!
    @IBOutlet weak var img_confirmPass: UIImageView!
    @IBOutlet weak var txt_fullName: UITextField!
    @IBOutlet weak var txt_email: UITextField!
    @IBOutlet weak var txt_phone: UITextField!
    @IBOutlet weak var txt_clinicName: UITextField!
    
    var signupData:SignupC_Modal = SignupC_Modal(fullName: "", email: "", phone: "")
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initialSetup()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        img_pass.isHidden = true
        img_confirmPass.isHidden = true
    }
    
    @IBAction func btn_Login(_ sender:UIButton) {
        let vc = LoginC_VC.instantiate(fromAppStoryboard: .LoginClinician)
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func btn_SignupEmployee(_ sender:UIButton) {
        let vc = SignupE_VC.instantiate(fromAppStoryboard: .LoginEmployee)
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func btn_Submit(_ sender:UIButton) {
        self.view.endEditing(true)
        guard validation() else {
            return
        }
        signupData.fullName = txt_fullName.text!
        signupData.email = txt_email.text!
        signupData.phone = txt_phone.text!
        UserDefaults.standard.setValue(txt_fullName.text!, forKey: "txtNameC")
        UserDefaults.standard.setValue(txt_phone.text!, forKey: "txtPhoneC")
        UserDefaults.standard.setValue(txt_email.text!, forKey: "txtEmailC")
        
        let vc = VerifyEmailPhoneC_VC.instantiate(fromAppStoryboard: .LoginClinician)
        vc.signupData = signupData
        self.navigationController?.pushViewController(vc, animated: true)
    }
}

extension SignupC_VC {
    
    @IBAction func text_pass(_ sender: UITextField) {
        if sender.text!.count > 0   {
            img_pass.isHidden = false
        }else{
            img_pass.isHidden = true
        }
    }
    
    @IBAction func text_ConfirmPass(_ sender: UITextField) {
        if sender.text!.count > 0   {
            img_confirmPass.isHidden = false
        }else{
            img_confirmPass.isHidden = true
        }
        if txt_pass.text != sender.text{
            img_confirmPass.image = #imageLiteral(resourceName: "red_cross")
        }else{
            img_confirmPass.image = #imageLiteral(resourceName: "checkdG")
        }
    }
}

extension SignupC_VC:UITextFieldDelegate {
    
    private func initialSetup() {
        txt_fullName.delegate = self
        txt_email.delegate = self
        txt_phone.delegate = self
    }
    
    func reset() {
        txt_fullName.text = ""
        txt_email.text = ""
        txt_phone.text = ""
    }
    
    func validation() -> Bool {
        
        if self.txt_clinicName.text?.count == 0   {
            AlertControllerOnr(title: alertTitle.alert_alert, message: messageString.E_empClinicName)
            return false
        }
        
        if self.txt_fullName.text?.count == 0   {
            AlertControllerOnr(title: alertTitle.alert_alert, message: messageString.E_empFullName)
            return false
        }
        if self.txt_email.text?.count == 0   {
            AlertControllerOnr(title: alertTitle.alert_alert, message: messageString.E_empEmail)
            return false
        }
        if self.txt_phone.text?.count == 0   {
            AlertControllerOnr(title: alertTitle.alert_alert, message: messageString.E_empPhone)
            return false
        }
        
        if self.txt_pass.text?.count == 0   {
            AlertControllerOnr(title: alertTitle.alert_alert, message: messageString.E_empPass)
            return false
        }
        
        if self.txt_confirmPass.text?.count == 0   {
            AlertControllerOnr(title: alertTitle.alert_alert, message: messageString.E_empConfirmPass)
            return false
        }
        if self.txt_pass.text != self.txt_confirmPass.text   {
            //            AlertControllerOnr(title: alertTitle.alert_alert, message: messageString.E_empConfirmPass)
            return false
        }
        return true
    }
    
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == txt_phone {
            guard let text = textField.text else { return false }
            let newString = (text as NSString).replacingCharacters(in: range, with: string)
            textField.text = format(with: "(+X)XXX-XXXXXXX", phone: newString)
            return false
        }
        return true
    }
}

struct SignupC_Modal {
    var fullName:String
    var email:String
    var phone:String
}
