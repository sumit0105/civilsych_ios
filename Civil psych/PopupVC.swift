//
//  PopupVC.swift
//  Civil psych
//
//  Created by Ankur on 20/07/21.
//

import UIKit

class PopupVC: UIViewController {
    
    @IBOutlet weak var lbl_content: UILabel!
    var type:nameView = .Medical_ReviewOfficer
    

    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
    }
    
    @IBAction func btn_back(_ sender:UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func setup(){
        if type == .Emergency_Contact{
            lbl_content.text = "Spouse/partner/family member"
        }else if type == .Medical_ReviewOfficer {
            lbl_content.text = "This is the name of the doctor who signed off on the lab report"
        }else if type == .Current_Lab_Name {
            lbl_content.text = "Lab where you took your failed drug test"
        }else if type == .DER_HR_Name{
            lbl_content.text = "This is usually the safety person/HR person/owner"
        }else if type == .Enter_previous_failed_drug_test{
            lbl_content.text = "Drug tests failed before the one above"
        }else if type == .previous_Substance_Abuse{
            lbl_content.text = "Name of SAP who did your assessment for your drug test"
        }
    }

}

enum nameView:String {
    case Emergency_Contact,
         Medical_ReviewOfficer,
         DER_HR_Name,
         Current_Lab_Name,
         Enter_previous_failed_drug_test,
         previous_Substance_Abuse
}
