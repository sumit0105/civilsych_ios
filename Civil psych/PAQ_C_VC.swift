//
//  PAQ_C_VC.swift
//  Civil psych
//
//  Created by Ankur  on 26/02/21.
//

import UIKit

class PAQ_C_VC: UIViewController {
    
    @IBOutlet weak var table: UITableView!
    @IBOutlet weak var btn_prev: UIButton!
    @IBOutlet weak var btn_next: UIButton!

    let arr1 = ["Q 1","Q 2","Q 3","Q 4","Q 5"]
    let arr2 = ["Q 6","Q 7","Q 8","Q 9","Q 10"]
    let arr3 = ["Q 11","Q 12","Q 13","Q 14","Q 15"]
    var indexRow : NSMutableArray = []
    override func viewDidLoad() {
        super.viewDidLoad()

        setUp()
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        btn_prev.isHidden = true
//        table.rowHeight = UITableView.automaticDimension
//        table.estimatedRowHeight = 124
    }
    
    func setUp()  {
        self.table.register(UINib(nibName: "PAQ_C_Cell", bundle: nil), forCellReuseIdentifier: "PAQ_C_Cell")
        table.delegate = self
        table.dataSource = self
    }

    @IBAction func btn_Next(_ sender:UIButton) {
        btn_next.tag = btn_next.tag + 1
        print("Tag:\(btn_next.tag)")
        table.reloadData()
        
        if btn_next.tag == 1 {
            btn_prev.isHidden = false
            btn_next.setTitle("Next", for: .normal)
            table.reloadData()
        }else if btn_next.tag == 2 {
            btn_next.setTitle("Submit", for: .normal)
            btn_prev.isHidden = false
            table.reloadData()
        }else{
            btn_prev.isHidden = true
            btn_next.setTitle("Next", for: .normal)
            self.navigationController?.popViewController(animated: true)
            table.reloadData()
        }
    }
    @IBAction func btn_Prev(_ sender:UIButton) {
        table.reloadData()
        btn_next.tag = btn_next.tag - 1
        table.reloadData()
        if btn_next.tag == 0 {
            btn_prev.isHidden = true
            btn_next.setTitle("Next", for: .normal)
            table.reloadData()
        }
    }
    
    @IBAction func btn_Back(_ sender:UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
}


extension PAQ_C_VC: UITableViewDelegate,UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if btn_next.tag == 0 {
            return arr1.count
        }else if btn_next.tag == 1{
            return arr2.count
        }else{
            return arr3.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = table.dequeueReusableCell(withIdentifier: "PAQ_C_Cell") as! PAQ_C_Cell
        
        cell.viewComment.isHidden = true
//        if cell.viewComment.isHidden == true {
//            cell.commentViewHeight.constant = 0
//        table.rowHeight = cell.ViewHeight.constant - cell.commentViewHeight.constant
//        }else{
//            cell.commentViewHeight.constant = 80
//            table.rowHeight = cell.ViewHeight.constant
//        }
        if indexRow.contains(indexPath.row) {
            cell.viewComment.isHidden = false
            cell.commentViewHeight.constant = 80
        }else{
            cell.viewComment.isHidden = true
            cell.commentViewHeight.constant = 0
        }

        
        cell.btn_addComment.tag = indexPath.row
        cell.btn_addComment.addTarget(self, action: #selector(btn_AddComment(_:)), for: .touchUpInside)
        if indexPath.row % 2 == 0 {
            cell.backgroundColor = .white
            cell.lbl_quesCount.text = "\(indexPath.row + 1)"
             
        }else{
            cell.backgroundColor = #colorLiteral(red: 0.9084902406, green: 0.9498965144, blue: 0.9871194959, alpha: 1)
            cell.lbl_quesCount.text = "\(indexPath.row + 1)"
           
        }
        if btn_next.tag == 0 {
            cell.lbl_quesCount.text = arr1[indexPath.row]
        }else if btn_next.tag == 1{
            cell.lbl_quesCount.text = arr2[indexPath.row]
        }else{
            cell.lbl_quesCount.text = arr3[indexPath.row]
        }
        
        cell.lbl_ans.text = "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s"
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    @IBAction func btn_AddComment(_ sender:UIButton) {
        
//        let alert = UIAlertController(title: "Add Comment", message: nil, preferredStyle: .alert)
//        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
//
//        alert.addTextField(configurationHandler: { textField in
//            textField.placeholder = "Type here"
//        })
//
//        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
//
//            if let name = alert.textFields?.first?.text {
//                print("\(name)")
//            }
//        }))
//
//        self.present(alert, animated: true)
        let indexPath = IndexPath(row: sender.tag, section: 0)
//        let cell = table.cellForRow(at: IndexPath(row: sender.tag, section: 0)) as! PAQ_C_Cell
        let cell = table.cellForRow(at: indexPath) as! PAQ_C_Cell
        
//        cell.viewComment.isHidden = !cell.viewComment.isHidden
        let i = indexPath.row
        if indexRow.contains(i) {
            indexRow.remove(i)
        }else{
            indexRow.add(i)
        }
        table.reloadData()

       
        
    }

}
