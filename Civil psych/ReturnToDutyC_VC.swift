//
//  ReturnToDutyC_VC.swift
//  Civil psych
//
//  Created by Ankur  on 26/02/21.
//

import UIKit
import DropDown

class ReturnToDutyC_VC: UIViewController {
    
    @IBOutlet weak var txt_selectTask: UITextField!
    @IBOutlet weak var txt_frequency: UITextField!
    @IBOutlet weak var txt_endDate: UITextField!
    
    let datePicker = UIDatePicker()
    let dateFormatter = DateFormatter()
    var toolBar = UIToolbar()
    let screenWidth = UIScreen.main.bounds.width
    
    override func viewDidLoad() {
        super.viewDidLoad()
        DatePick()
        if #available(iOS 13.4, *) {
            datePicker.preferredDatePickerStyle = .wheels
        }
    }
    
    //MARK: - DropDown's
    
    let drop_selectTask = DropDown()
    let drop_frequency = DropDown()
    
    lazy var dropDowns: [DropDown] = {
        return [
            self.drop_selectTask,
            self.drop_frequency
        ]
    }()
    
    @IBAction func btn_selectTask(_ sender: UIButton){
        
        self.drop_selectTask.anchorView = sender
        self.drop_selectTask.bottomOffset = CGPoint(x: 0, y: sender.bounds.height + 0)
        self.drop_selectTask.textColor = .black
        self.drop_selectTask.separatorColor = .clear
        self.drop_selectTask.selectionBackgroundColor = .clear
        self.drop_selectTask.backgroundColor = #colorLiteral(red: 0.8541120291, green: 0.9235828519, blue: 0.9914466739, alpha: 1)
        self.drop_selectTask.dataSource.removeAll()
//        self.drop_selectTask.cellHeight = 35
//        self.drop_selectTask.dataSource.append(contentsOf: ["Task 1","Task 2","Task 3","Task 4","Task 5","Task 6"])
        self.drop_selectTask.dataSource.append(contentsOf: ["12 Hours Drug education","Weekly attendance of two hours drug education for four weeks","Liver panel from physician","Read book on recovery and write a one page summary","Read a web page on THC and write a one page summary"])
        
        self.drop_selectTask.selectionAction = { [unowned self] (index, item) in
            self.txt_selectTask.text = item
        }
        self.drop_selectTask.show()
        
    }
    
    @IBAction func btn_selectFrequncy(_ sender: UIButton){
        
        self.drop_frequency.anchorView = sender
        self.drop_frequency.bottomOffset = CGPoint(x: 0, y: sender.bounds.height + 0)
        self.drop_frequency.textColor = .black
        self.drop_frequency.separatorColor = .clear
        self.drop_frequency.selectionBackgroundColor = .clear
        self.drop_frequency.backgroundColor = #colorLiteral(red: 0.8541120291, green: 0.9235828519, blue: 0.9914466739, alpha: 1)
        self.drop_frequency.dataSource.removeAll()
        self.drop_frequency.cellHeight = 35
        self.drop_frequency.dataSource.append(contentsOf: ["Every Week","Every 2 Weeks","Every Month","Every Quarter","Every Month","Every Quarter"])
        self.drop_frequency.selectionAction = { [unowned self] (index, item) in
            self.txt_frequency.text = item
        }
        self.drop_frequency.show()
        
    }
    
    @IBAction func btn_back(_ sender:UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btn_AssignTask(_ sender:UIButton) {
        let vc = ReturnToDuty1C_VC.instantiate(fromAppStoryboard: .HomeC)
        self.navigationController?.pushViewController(vc, animated: true)
    }

}

extension ReturnToDutyC_VC: UITextFieldDelegate {
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == txt_endDate {
            datePicker.datePickerMode = .date
        }
        
    }
    
    @objc func doneButtonTapped() {
        if txt_endDate.isFirstResponder {
            dateFormatter.dateStyle = .medium
            dateFormatter.timeStyle = .none
            dateFormatter.dateFormat = "MM/dd/yyyy"
            //            dateFormatter.dateFormat = "yyyy-MM-dd"
            txt_endDate.text = dateFormatter.string(from: datePicker.date)
        }
        self.view.endEditing(true)
    }
    
    func DatePick(){
        toolBar = UIToolbar(frame: CGRect(x: 0.0, y: 0.0, width: screenWidth, height: 44.0)) //4
        
        toolBar.sizeToFit()
        //        toolBar.backgroundColor = .white
        toolBar.tintColor = .systemBlue
        toolBar.isUserInteractionEnabled = true
        let doneButton = UIBarButtonItem(title: "Done", style: .done, target: self, action: #selector(doneButtonTapped))
        doneButton.tintColor = .black
        toolBar.setItems([doneButton], animated: false)
        txt_endDate.inputView = datePicker
        txt_endDate.inputAccessoryView   = toolBar
        txt_endDate.delegate = self
        datePicker.minimumDate = Date()
    }
}
