//
//  SplashVC.swift
//  Civil psych
//
//  Created by Ankur  on 22/01/21.
//

import UIKit

class SplashVC: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
//        let vc = TabC_VC.instantiate(fromAppStoryboard: .TabC)
//        self.navigationController?.pushViewController(vc, animated: true)
        
//                let vc = TabE_VC.instantiate(fromAppStoryboard: .TabE)
//                self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func btn_Clinician(_ sender:UIButton) {
        let vc = SignupC_VC.instantiate(fromAppStoryboard: .LoginClinician)
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func btn_Employee(_ sender:UIButton) {
        let vc = SignupE_VC.instantiate(fromAppStoryboard: .LoginEmployee)
        self.navigationController?.pushViewController(vc, animated: true)
    }
}
