//
//  ChatListC_VC.swift
//  Civil psych
//
//  Created by Ankur  on 28/02/21.
//

import UIKit

class ChatListC_VC: UIViewController {
    
    @IBOutlet weak var table:UITableView!
    let arr =   [#imageLiteral(resourceName: "i13"),#imageLiteral(resourceName: "profile-Image"),#imageLiteral(resourceName: "i13"),#imageLiteral(resourceName: "profile-Image"),#imageLiteral(resourceName: "i13"),#imageLiteral(resourceName: "profile-Image"),#imageLiteral(resourceName: "i13"),#imageLiteral(resourceName: "profile-Image"),#imageLiteral(resourceName: "i13"),#imageLiteral(resourceName: "profile-Image"),#imageLiteral(resourceName: "i13")]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
    }
    
    func setup(){
        self.table.register(UINib(nibName: "ChatCell", bundle: nil), forCellReuseIdentifier: "ChatCell")
        table.delegate = self
        table.dataSource = self
    }
    
}

extension ChatListC_VC :UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arr.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 75
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = table.dequeueReusableCell(withIdentifier: "ChatCell", for: indexPath) as! ChatCell
        cell.lblcountMessage.isHidden = true
        cell.img_user.image = arr[indexPath.row]
        if indexPath.row == 0 {
            cell.lblcountMessage.isHidden = false
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = ChatC_VC.instantiate(fromAppStoryboard: .HomeC)
        vc.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(vc, animated: true)
    }

}
