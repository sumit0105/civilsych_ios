//
//  ChangePassVC.swift
//  Civil psych
//
//  Created by Ankur  on 28/02/21.
//

import UIKit

class ChangePassVC: UIViewController {
    
    @IBOutlet weak var txt_pass: UITextField!
    @IBOutlet weak var txt_confirmPass: UITextField!

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func btn_Submit(_ sender:UIButton) {
        if self.txt_pass.text?.count == 0   {
            AlertControllerOnr(title: alertTitle.alert_alert, message: messageString.mPasswordValidation)
            return
        }
        
        if self.txt_confirmPass.text?.count == 0   {
            AlertControllerOnr(title: alertTitle.alert_alert, message: messageString.mConfirmPasswordValidation)
            return
        }
        
        if txt_pass.text != txt_confirmPass.text {
            AlertControllerOnr(title: alertTitle.alert_alert, message: messageString.mPaaswordmatch)
            return
        }

        self.dismiss(animated: true, completion: {
//            self.select!("s",1)
        })
    }
    
    @IBAction func btn_Back(_ sender:UIButton) {
        self.dismiss(animated: true, completion: {
//            self.select!("s",0)
        })
}
}
