//
//  EditProfilleC_VC.swift
//  Civil psych
//
//  Created by Ankur  on 28/02/21.
//

import UIKit

class EditProfilleC_VC:UIViewController {
    
    @IBOutlet weak var txt_name: UITextField!
    @IBOutlet weak var txt_email: UITextField!
    @IBOutlet weak var txt_phone: UITextField!
    @IBOutlet weak var txt_licenseNumber: UITextField!
    @IBOutlet weak var txt_dateExpiry: UITextField!
    @IBOutlet weak var txt_orgNumber: UITextField!
    @IBOutlet weak var txt_dateCompletion: UITextField!
    @IBOutlet weak var txt_monthlyCost: UITextField!
    @IBOutlet weak var imgProfile:UIImageView!
    @IBOutlet weak var imgLicense:UIImageView!
    @IBOutlet weak var imgCertificate:UIImageView!
    @IBOutlet weak var imgLogo:UIImageView!
    @IBOutlet weak var btn1:UIButton!
    @IBOutlet weak var btn2:UIButton!
    @IBOutlet weak var txt_orgAddress: UITextView!

    @IBOutlet weak var viewInfo: UIView!
   
    let datePicker = UIDatePicker()
    let dateFormatter = DateFormatter()
    var toolBar = UIToolbar()
    let screenWidth = UIScreen.main.bounds.width
    
    var imagePicker1: ImagePicker!
    var imageaAtt : imageArray?
    var imgStatus = 0
    var signupData:SignupC_Modal = SignupC_Modal(fullName: "", email: "", phone: "")
    
    override func viewDidLoad() {
        super.viewDidLoad()
        imagePicker1 = ImagePicker(presentationController: self, delegate: self)
        DatePick()
        if #available(iOS 13.4, *) {
            datePicker.preferredDatePickerStyle = .wheels
        }
//        txt_name.text = "Daniel Jhonson"
//        txt_email.text = "DanielJhon23@gmail.com"
//        txt_phone.text = "+1(818) 334-4799"
        SetData()
        txt_orgAddress.delegate = self
        txt_phone.delegate = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
//        viewInfo.isHidden = true
    }

    
    @IBAction func btn_camera(_ sender:UIButton) {
        
        imagePicker1.present(from: sender)
        imgStatus = 1
    }
    
    @IBAction func btn_uploadLicence(_ sender:UIButton) {
        imagePicker1.present(from: sender)
        imgStatus = 2
    }
    @IBAction func btn_uploadCeuCertificate(_ sender:UIButton) {
        imagePicker1.present(from: sender)
        imgStatus = 3
    }
    @IBAction func btn_uploadLogo(_ sender:UIButton) {
        imagePicker1.present(from: sender)
        imgStatus = 4
    }
    
    @IBAction func btn_removeUploadLicence(_ sender:UIButton) {
        imgLicense.image = nil
    }
    @IBAction func btn_removeCeuCertificate(_ sender:UIButton) {
        imgCertificate.image = nil
    }
    
    @IBAction func btn_removeLogo(_ sender:UIButton) {
        imgLogo.image = nil
    }
    
    @IBAction func btn_submit(_ sender:UIButton) {
        self.navigationController?.popViewController(animated: true)
    }

    @IBAction func btn_back(_ sender:UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btn_info(_ sender:UIButton) {
//        self.viewInfo.isHidden = !self.viewInfo.isHidden
        let vc = PaymentInfoVC.instantiate(fromAppStoryboard: .LoginClinician)
        self.present(vc, animated: true, completion: nil)
    }

    
    var p = 0
    @IBAction func btn_paymentOffer(_ sender:UIButton){
        if sender.tag == 0 {
            btn1.setImage(#imageLiteral(resourceName: "btn_RoundFill"), for: .normal)
            btn2.setImage(#imageLiteral(resourceName: "btn_RoundBlank"), for: .normal)
            p = 1
        }else{
            btn2.setImage(#imageLiteral(resourceName: "btn_RoundFill"), for: .normal)
            btn1.setImage(#imageLiteral(resourceName: "btn_RoundBlank"), for: .normal)
            p = 2
        }
    }
}




extension EditProfilleC_VC : ImagePickerDelegate {
    func didSelect(image: UIImage?) {
        guard let img = image else {
            return
        }
        //        self.imageaAtt.append(imageArray(image: img, data: nil, url: nil, image_id: nil))
        if imgStatus == 1 {
            self.imgProfile.image = image
        }else if imgStatus == 2 {
            self.imgLicense.image = image
        }else if imgStatus == 3{
            self.imgCertificate.image = image
        }else{
            self.imgLogo.image = image
        }
        
    }
}

extension EditProfilleC_VC: UITextFieldDelegate {
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == txt_dateExpiry {
            datePicker.datePickerMode = .date
        }else if textField == txt_dateCompletion {
            datePicker.datePickerMode = .date
        }
    }
    
    @objc func doneButtonTapped() {
        if txt_dateExpiry.isFirstResponder {
            dateFormatter.dateStyle = .medium
            dateFormatter.timeStyle = .none
            dateFormatter.dateFormat = "MM/dd/yyyy"
//            dateFormatter.dateFormat = "yyyy-MM-dd"
            txt_dateExpiry.text = dateFormatter.string(from: datePicker.date)
        }else if txt_dateCompletion.isFirstResponder {
//            let date: Date? = dateFormatterGet.date(from: "01/01/2021")
            dateFormatter.dateStyle = .medium
            dateFormatter.timeStyle = .none
                        dateFormatter.dateFormat = "MM/dd/yyyy"
//            dateFormatter.dateFormat = "yyyy-MM-dd"
            txt_dateCompletion.text = dateFormatter.string(from: datePicker.date)
//            txt_prevDrug_DateOfTest.text = dateFormatter.string(from: date!)
            
        }
        self.view.endEditing(true)
    }
    
    func DatePick(){
        
        toolBar = UIToolbar(frame: CGRect(x: 0.0, y: 0.0, width: screenWidth, height: 44.0)) //4
        
        toolBar.sizeToFit()
        //        toolBar.backgroundColor = .white
        toolBar.tintColor = .systemBlue
        toolBar.isUserInteractionEnabled = true
        let doneButton = UIBarButtonItem(title: "Done", style: .done, target: self, action: #selector(doneButtonTapped))
        doneButton.tintColor = .black
        toolBar.setItems([doneButton], animated: false)
        txt_dateExpiry.inputView = datePicker
        txt_dateCompletion.inputView = datePicker
        txt_dateExpiry.inputAccessoryView   = toolBar
        txt_dateCompletion.inputAccessoryView   = toolBar
        txt_dateExpiry.delegate = self
        txt_dateCompletion.delegate = self
        datePicker.minimumDate = Date()
    }
}

extension EditProfilleC_VC : UITextViewDelegate{
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if txt_orgAddress.text == "Enter Organization Address" {
            txt_orgAddress.text = ""
        }
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if text == "\n" {
            txt_orgAddress.resignFirstResponder()
        }
        return true
    }

    func textViewDidEndEditing(_ textView: UITextView) {
        if txt_orgAddress.text == ""{
            txt_orgAddress.text = "Enter Organization Address"
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == txt_phone {
            guard let text = textField.text else { return false }
            let newString = (text as NSString).replacingCharacters(in: range, with: string)
//            textField.text = format(with: "(XXX) XXX-XXXX", phone: newString)
            textField.text = format(with: "(+X)XXX-XXXXXXX", phone: newString)
            return false
        }
        return true
    }
}

extension EditProfilleC_VC {
    func SetData(){
        txt_name.text = UserDefaults.standard.object(forKey: "txtNameC") as? String
        txt_phone.text = UserDefaults.standard.object(forKey: "txtPhoneC") as? String
        txt_email.text = UserDefaults.standard.object(forKey: "txtEmailC") as? String
        
    }
}
