//
//  ProfileC_VC.swift
//  Civil psych
//
//  Created by Ankur  on 28/02/21.
//

import UIKit

class ProfileC_VC: UIViewController {
    
    @IBOutlet weak var imgProfile:UIImageView!
    
    var imagePicker1: ImagePicker!
    var imageaAtt : imageArray?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        imagePicker1 = ImagePicker(presentationController: self, delegate: self)
    }
    
    @IBAction func btn_camera(_ sender:UIButton) {
        imagePicker1.present(from: sender)
    }
    
    @IBAction func btn_Profile(_ sender:UIButton) {
        let vc = EditProfilleC_VC.instantiate(fromAppStoryboard: .ProfileC)
        vc.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func btn_SponsorResult(_ sender:UIButton) {
        let vc = SponsorSearchResultC_VC.instantiate(fromAppStoryboard: .ProfileC)
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func btn_Support(_ sender:UIButton) {
        let vc = SupportC_VC.instantiate(fromAppStoryboard: .ProfileC)
        self.navigationController?.pushViewController(vc, animated: true)
    }

    
    @IBAction func btn_ChangePassword(_ sender:UIButton) {
        let vc = ChangePassVC.instantiate(fromAppStoryboard: .ProfileC)
        self.present(vc, animated: true, completion: nil)
    }
    
    @IBAction func btn_Aboutus(_ sender:UIButton) {
        let vc = AboutusC_VC.instantiate(fromAppStoryboard: .ProfileC)
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func btn_Terms(_ sender:UIButton) {
        let vc = TermsC_VC.instantiate(fromAppStoryboard: .ProfileC)
        self.navigationController?.pushViewController(vc, animated: true)

    }
    
    @IBAction func btn_Privacy(_ sender:UIButton) {
        let vc = PrivacyC_VC.instantiate(fromAppStoryboard: .ProfileC)
        self.navigationController?.pushViewController(vc, animated: true)

    }
    
    
    @IBAction func btn_Logout(_ sender:UIButton) {
        let vc = LoginC_VC.instantiate(fromAppStoryboard: .LoginClinician)
        vc.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(vc, animated: true)
    }
}

extension ProfileC_VC : ImagePickerDelegate {
    func didSelect(image: UIImage?) {
        guard let img = image else {
            return
        }
        //        self.imageaAtt.append(imageArray(image: img, data: nil, url: nil, image_id: nil))
        self.imgProfile.image = image
    }
}

