//
//  SponsorSearchResultC_VC.swift
//  Civil psych
//
//  Created by Ankur  on 28/02/21.
//

import UIKit

class SponsorSearchResultC_VC: UIViewController {
    
    @IBOutlet weak var table:UITableView!
    
    let arr = ["50 appearances in $20/-","100 appearances in $35/-","200 appearances in $59/-"]
    
    override func viewDidLoad() {
        super.viewDidLoad()

       setup()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = true
    }

    @IBAction func btn_back(_ sender:UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func setup(){
        self.table.register(UINib(nibName: "SponsorResultsC_Cell", bundle: nil), forCellReuseIdentifier: "SponsorResultsC_Cell")
        table.delegate = self
        table.dataSource = self
    }

}

extension SponsorSearchResultC_VC :UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arr.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = table.dequeueReusableCell(withIdentifier: "SponsorResultsC_Cell", for: indexPath) as! SponsorResultsC_Cell
        cell.lbl_planName.text = arr[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = ChatC_VC.instantiate(fromAppStoryboard: .HomeC)
        vc.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(vc, animated: true)
    }

}
