//
//  SupportC_VC.swift
//  Civil psych
//
//  Created by Ankur  on 28/02/21.
//

import UIKit

class SupportC_VC: UIViewController {
    
    @IBOutlet weak var txt_email: UITextField!
    @IBOutlet weak var txt_requestMsg: UITextField!
    @IBOutlet weak var txt_comments: UITextField!

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = true
    }
    
    @IBAction func btn_submit(_ sender:UIButton) {
        self.view.endEditing(true)
        guard validation() else {
            return
        }

        self.navigationController?.popViewController(animated: true)
    }

    @IBAction func btn_back(_ sender:UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
}

extension SupportC_VC {
    
    func validation() -> Bool {
        
        if self.txt_email.text?.count == 0   {
            AlertControllerOnr(title: alertTitle.alert_alert, message: messageString.E_empEmail)
            return false
        }
        if self.txt_requestMsg.text?.count == 0   {
            AlertControllerOnr(title: alertTitle.alert_alert, message: messageString.C_reqMsg)
            return false
        }
        
        if self.txt_comments.text?.count == 0   {
            AlertControllerOnr(title: alertTitle.alert_alert, message: messageString.C_comments)
            return false
        }

        return true
    }
}
