//
//  ChatC_VC.swift
//  Civil psych
//
//  Created by Ankur  on 26/02/21.
//

import UIKit
import MobileCoreServices

class ChatC_VC: UIViewController {
    
    @IBOutlet weak var btnMicSend: UIButton!
    @IBOutlet weak var txtChat: UITextField!
    
    var imagePicker1: ImagePicker!
    var imageaAtt : imageArray?
    var imgStatus = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        imagePicker1 = ImagePicker(presentationController: self, delegate: self)
        txtChat.delegate = self
    }
    
    @IBAction func btn_back(_ sender:UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func btn_camera(_ sender:UIButton) {
        imagePicker1.presentCamera(from: sender)
    }
    @IBAction func btn_sendAttachment(_ sender:UIButton) {
//        imagePicker1.presentLibrary(from: sender)
        ChooseFile()
    }
    
    @IBAction func btn_sendMessage(_ sender:UIButton) {
        self.txtChat.text = nil
    }
    
}


extension ChatC_VC : ImagePickerDelegate {
    func didSelect(image: UIImage?) {
        guard let img = image else {
            return
        }
        //        self.imageaAtt.append(imageArray(image: img, data: nil, url: nil, image_id: nil))
    }
}

extension ChatC_VC:UITextFieldDelegate {
    
    
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        print("textFieldDidBeginEditing")
        btnMicSend.setImage(#imageLiteral(resourceName: "sendM"), for: .normal)
        //        textField.placeholder = nil
        
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField.placeholder == "Type a message" && textField.text == ""{
            btnMicSend.setImage(#imageLiteral(resourceName: "mic"), for: .normal)
            //            btnEmoji.setImage(#imageLiteral(resourceName: "fappyface"), for: .normal)
        }else{
            btnMicSend.setImage(#imageLiteral(resourceName: "sendM"), for: .normal)
            //            btnEmoji.setImage(#imageLiteral(resourceName: "fappyface"), for: .normal)
        }
        
        print("textFieldDidEndEditing")
    }
    
}

extension ChatC_VC:UIDocumentPickerDelegate,UIDocumentMenuDelegate {
    func ChooseFile(){
        // if #available(iOS 14.0, *) {
        // let types = UTType.types(tag: "json",
        // tagClass: UTTagClass.filenameExtension,
        // conformingTo: nil)
        // let documentPickerController = UIDocumentPickerViewController(
        // forOpeningContentTypes: types)
        // documentPickerController.delegate = self
        // self.present(documentPickerController, animated: true, completion: nil)
        // } else {
        
        let importMenu = UIDocumentPickerViewController(documentTypes: [String(kUTTypePDF),String(kUTTypePNG),String(kUTTypeJPEG),String(kUTTypeImage),String(kUTTypeXML),String(kUTTypeRTF),String(kUTTypePlainText),String(kUTTypeContent),String(kUTTypeItem),String(kUTTypeData),kUTTypeZipArchive as String,"com.microsoft.word.doc","com.adobe.photoshop-image","com.adobe.illustrator.ai-image","com.apple.keynote.key"], in: .import)
        importMenu.delegate = self
        importMenu.modalPresentationStyle = .formSheet
        self.present(importMenu, animated: true, completion: nil)
        // }
        
    }
    public func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentAt url: URL) {
        let cico = url as URL
        print("import result : \(cico)")
        do {
            _ = try NSData(contentsOf: cico, options: NSData.ReadingOptions())
            let p : String = "\(cico)"
            let pdfData = try Data(contentsOf: cico, options: Data.ReadingOptions())
            let filee = "." + p.fileExtension()
            let fullName = p.fileNameWithExtension()
  //          self.listAllImages.append(UploadFileParameter(fileName : fullName, key: "image_url", data: pdfData, mimeType: filee))
    //        self.lbl_Attachment.text = fullName
     //       self.collection.reloadData()
        } catch {
            print(error)
        }
    }
    
    public func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentsAt urls: [URL]) {
        guard let cico = urls.first else {
            return
        }
        print("import result : \(cico)")
        let alerty = UIAlertController(title: "Loading file...", message: nil, preferredStyle: .alert)
        self.present(alerty, animated: true) {
            do {
                let p : String = "\(cico)"
                let pdfData = try Data(contentsOf: cico, options: Data.ReadingOptions())
                alerty.dismiss(animated: true) {
                }
                let filee = "." + p.fileExtension()
                let fullName = p.fileNameWithExtension()
        //        self.listAllImages.append(UploadFileParameter(fileName : fullName, key: "image_url", data: pdfData, mimeType: filee))
           //     self.lbl_Attachment.text = fullName
//                self.collection.reloadData()
            } catch {
                alerty.dismiss(animated: true) {
                }
                print(error)
            }
        }
    }
    
    public func documentMenu(_ documentMenu:UIDocumentMenuViewController, didPickDocumentPicker documentPicker: UIDocumentPickerViewController) {
        documentPicker.delegate = self
        present(documentPicker, animated: true, completion: nil)
    }
    
    
    func documentPickerWasCancelled(_ controller: UIDocumentPickerViewController) {
        print("view was cancelled")
        dismiss(animated: true, completion: nil)
    }
    @IBAction func getImageFrom1(_ sender:UIButton) {
        let alert = UIAlertController(title: "Select Image", message: nil, preferredStyle: UIAlertController.Style.actionSheet)
        alert.addAction(UIAlertAction(title: "file", style: UIAlertAction.Style.default, handler: { (res) in
            self.ChooseFile()
        }))
        alert.addAction(UIAlertAction(title: "Device Photo", style: UIAlertAction.Style.default, handler: { (res) in
     //       self.callimage()
        }))
        
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { (res) in
        }))
        if let popoverPresentationController = alert.popoverPresentationController {
            popoverPresentationController.sourceView = sender
            popoverPresentationController.sourceRect = sender.bounds
        }
        self.present(alert, animated: true, completion: nil)
    }
    
}
